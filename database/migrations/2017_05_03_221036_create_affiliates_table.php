<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAffiliatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('affiliates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('child_id')->unsigned();
            $table->bigInteger('real_deposit',false,true)->default(0);
            $table->bigInteger('commission',false,true)->default(0);
            $table->string('member_btc_address');
            $table->timestamp('member_registered')->nullable();
            $table->timestamp('member_last_login')->nullable();
            $table->boolean('reinvested')->default(false);
            $table->timestamps();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('affiliates');
    }
}
