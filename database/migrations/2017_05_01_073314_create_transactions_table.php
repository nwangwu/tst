<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            //trans_type = deposit,withdrawal,reinvest,withdraw_deposit
            $table->string('trans_type',10);
            $table->bigInteger('amount',false,true);
            $table->integer('account_id',false,true);
            //if withdraw = status(pending or completed or cancelled)
            $table->string('status',20)->nullable();
            $table->string('bit_user_address')->nullable();
            $table->string('bit_address')->nullable();
            $table->string('with_trans_id',255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
