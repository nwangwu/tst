<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('facebook_id')->unique()->nullable();
            $table->string('name')->unique()->nullable();
            $table->integer('role_id')->unsigned()->default(3);//->unsigned();
            $table->string('bit_owner_address')->nullable();
            $table->string('bit_deposit_address')->nullable();
            $table->string('email')->unique();
            //$table->string('referral')->unique()->nullable();
            $table->string('parent')->default('admin');
            $table->string('password')->nullable();
            $table->boolean('active')->default(false);
            $table->boolean('confirmed')->default(false);
            $table->string('confirmation_code')->nullable();
            $table->rememberToken();
            $table->timestamp('last_login')->nullable();
            $table->timestamps();
        });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');

    }
}
