<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeignKeysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users',function(Blueprint $table){
            $table->foreign('role_id')
                ->references('id')
                ->on('roles')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });

        Schema::table('earns', function(Blueprint $table){
            $table->foreign('account_id')
                ->references('id')
                ->on('accounts')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });

        Schema::table('accounts', function(Blueprint $table){
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('transactions', function(Blueprint $table){
            $table->foreign('account_id')
                ->references('id')
                ->on('accounts')
                ->onDelete('restrict')
                ->onUpdate('cascade');
        });

        Schema::table('affiliates',function(Blueprint $table){
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('restrict')
                ->onUpdate('restrict');


            $table->foreign('child_id')
                ->references('id')
                ->on('users')
                ->onDelete('restrict')
                ->onUpdate('restrict');


        });

        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function(Blueprint $table){
            $table->dropForeign('users_role_id_foreign');
        });
        Schema::table('accounts', function(Blueprint $table) {
            $table->dropForeign('accounts_user_id_foreign');
        });
        Schema::table('transactions',function(Blueprint $table) {
            $table->dropForeign('transactions_account_id_foreign');
        });
        Schema::table('earns',function(Blueprint $table){
            $table->dropForeign('earns_account_id_foreign');
        });
        Schema::table('affiliates',function(Blueprint $table){
            $table->dropForeign('affiliates_user_id_foreign');
            $table->dropForeign('affiliates_child_id_foreign');
        });

        //
    }
}
