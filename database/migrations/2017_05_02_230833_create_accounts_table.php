<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id',false,true);
            $table->boolean('can_with_deposit')->default(true);
            $table->boolean('auto_withdraw')->default(false);
            $table->boolean('auto_reinvest')->default(false);
            $table->BigInteger('total_earned',false,true)->default(0);
            $table->BigInteger('active_deposit',false,true)->default(0);
            $table->BigInteger('total_deposit',false,true)->default(0);
            $table->BigInteger('gateway_deposit',false,true)->default(0);
            $table->BigInteger('balance',false,true)->default(0);
            $table->double('interest',12,8)->default(0.00000000);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('accounts');
    }
}
