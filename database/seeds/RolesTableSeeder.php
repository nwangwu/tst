<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('roles')->insert([
            ['title' => 'Super Administrator', 'slug' => 'super_admin'],
            ['title' => 'Administrator', 'slug' => 'admin'],
            ['title' => 'User', 'slug' => 'user'],
            ['title' =>'false users','slug'=>'false_users']
        ]);
    }
}
