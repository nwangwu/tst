<?php

use Illuminate\Database\Seeder;
//use Faker\Factory as Faker;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$faker = Faker::create();

        for($i = 0; $i < 2; $i++) {
            DB::table('users')->insert([
                ['name' => "admin".$i , 'email' => 'admin' . $i . '@gmail.com',
                    'password' => bcrypt('password'),
                    'confirmed' => true, 'confirmation_code' => null, 'role_id' => 1]
            ]);
            /*
                        DB::table('accounts')->insert([
                            ['user_id' => $i+1]
                        ]);*/
        }
        DB::table('users')->insert([
            ['name'=> 'user1',
                'email'=>'user1@gmail.com',
                'password'=>bcrypt('password'),
                'confirmed' => true,
                'confirmation_code'=>null,
                'role_id' => 3
                ]
        ]);
    }
}
