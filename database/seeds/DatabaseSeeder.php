<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        //$this->call(RolesTableSeeder::class);
        $this->call(UserTableSeeder::class);
/*
        DB::table('users')->insert([
            ['name' => 'ifunanya',
                'email' => 'fransmakes',
                'role_id' => 1,
                'password' => bcrypt('upjesus'),
                'confirmed' => true,
                'confirmation_code' => null]
        ]);
*/
    }
}
