<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@homepage')->name('homepage');

/*
Route::get('/usercontroller/path',[
    'middleware' => 'first',
    'uses' => 'UserController@showPath'
]);
*/
/*
Route::get('/deposit/{amount}','TestController@deposit');
Route::get('/withdraw/{amount}','TestController@withdraw');
Route::get('/blockchain','TestController@index');
Route::get('/reinvest','TestController@reinvest');
Route::get('auto_withdraw','TestController@auto_withdraw');
*/

/**
 * Facebook provider url
 */
Route::get('facebook/login','Auth\LoginController@redirectToProvider')->name('facebook');
Route::get('facebook/login/callback','Auth\LoginController@handleProviderCallback');

/**
 * Auth and email confirmation route with homepage
 */

Route::get('test_home',  function (){
    return view('pages.home');
})->name('test_home');
Route::get('test_about', function (){
    return view('pages.about');
});
Route::get('get_started', function (){
    return view('pages.get_started');
});
Route::get('terms', function (){
    return view('pages.terms');
});
Route::get('contact_us', function (){
    return view('pages.contact');
});


Auth::routes();
Route::get('confirm/{token}','Auth\RegisterController@confirm');
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/ref/{name}','Auth\RegisterController@referral')->where('name','[0-9a-zA-Z_-]+');
/**
 * User dashboard routes ::Frontend
*/
Route::get('/member/createusername','Auth\LoginController@create_username')->name('create_username');
Route::post('/member/createusername','Auth\LoginController@create_username_post')->name('create_username_post');

Route::get('/member/deposit','TransactionController@deposit')->name('deposit');
Route::post('/member/deposit','TransactionController@deposit_post')->name('deposit_post');
Route::get('/member/withdraw','TransactionController@withdraw')->name('withdraw');
Route::post('/member/withdraw','TransactionController@withdraw_post')->name('withdraw_post');
Route::get('/member/reinvest','TransactionController@reinvest')->name('reinvest');
Route::post('/member/reinvest','TransactionController@reinvest_post')->name('reinvest_post');
Route::get('/member/withdrawdeposit','TransactionController@withdraw_deposit')->name('withdrawdeposit');
Route::post('/member/canceldeposit','TransactionController@withdraw_deposit_post')->name('cancel_deposit');
Route::get('/member/history','UserController@finance_history')->name('history');
Route::get('/member/automate','UserController@automation')->name('automation');
Route::post('/member/automate','UserController@automation_post')->name('automation_post');
Route::get('/member/affiliate','AffiliateController@index')->name('affiliate');
Route::post('/member/affiliate_post','AffiliateController@affiliate_post')->name('affiliate_post');
Route::resource('/member/setting','SettingsController');;//->name('ph.create');
Route::get('/member/leaderboard','HomeController@leaderboard')->name('leaderboard');
/**
 * Guest routes
 */
Route::post('/gateway','BitcoinController@ajax_check');

Route::get('/about','HomeController@faq')->name('about');
Route::get('/privacy_policy','HomeController@privacy_policy')->name('privacy_policy');
//Route::get('/terms_of_service','HomeController@terms_of_service')->name('tos');
Route::get('/member/contact','HomeController@contact_us')->name('contactus');

Route::post('/member/contact','HomeController@contact_process')->name('contact_post');

Route::get('/member/resend','Auth\RegisterController@resend')->name('resend');

Route::group(['namespace'=>'Admin','middleware'=>'admin','prefix' =>'admin'],function(){
    Route::get('index','AdminController@index')->name('admin_index');
    Route::get('viewwithdraw','AdminController@withdraw_request')->name('withdraw_request');
    Route::get('makepayment','AdminController@make_payment')->name('run_withdraw');
    Route::post('makepayment','AdminController@make_payment_post')->name('make_payment');
    Route::post('makesinglepayment','AdminController@make_single_payment_post')->name('make_single_payment');
    Route::post('cancelsinglepayment','AdminController@cancel_payment_post')->name('cancel_payment');
    Route::get('leaderboard','AdminController@view_leaderboard')->name('view_board');
    Route::get('view_users','AdminController@view_users')->name('view_user');

    Route::get('wallet_info','AdminController@wallet_info')->name('wallet_info');
    Route::get('addressbalance','AdminController@address_balance')->name('address_balance');
    Route::post('addressbalance','AdminController@address_balance_post')->name('address_balance_post');
    Route::get('wallet_setting','SuperAdminController@wallet_setting')->name('wallet_setting');
    Route::post('wallet_setting','SuperAdminController@wallet_setting_post')->name('wallet_setting_post');
    Route::get('add','SuperAdminController@add_admin')->name('add_admin');
    Route::post('add','SuperAdminController@add_admin_post')->name('add_admin_post');
    Route::get('view','SuperAdminController@view_admin')->name('view_admin');
    Route::post('removeadmin','SuperAdminController@remove_admin')->name('remove_admin');
    Route::get('deletedadmins','SuperAdminController@deleted_admin')->name('deleted_admin');
    Route::post('restoreadmin','SuperAdminController@restore_admin')->name('restore_admin');
    Route::post('removeuser','SuperAdminController@remove_user')->name('remove_user');
    Route::get('deletedusers','SuperAdminController@deleted_user')->name('deleted_user');
    Route::post('restoreuser','SuperAdminController@restore_user')->name('restore_user');
    Route::post('changesetting','AdminController@change_admin_passcode_post')->name('changepasscode');
    Route::get('changesettings','AdminController@change_admin_passcode')->name('changepasscodeview');

    Route::get('add_user','AdminController@create_user')->name('create_user');
    Route::post('add_user_post','AdminController@create_user_post')->name('create_user_post');
    Route::get('viewfalseusers','AdminController@get_users')->name('view_false_users');

    Route::post('view_trans','AdminController@view_trans')->name('view_trans');
    Route::post('update_trans','AdminController@update_trans')->name('update_trans');
    Route::post('update_trans_post','AdminController@update_trans_post')->name('update_trans_post');

    Route::post('add_trans','AdminController@add_trans')->name('add_trans');
    Route::post('add_trans_posts','AdminController@add_trans_post')->name('add_trans_post');

    Route::post('update_user','AdminController@update_user')->name('user_update');
    Route::post('update_user_post','AdminController@update_user_post')->name('user_update_post');

    Route::get('sendmail','AdminController@send_one_mail')->name('sendone');
    Route::post('sendmailpost','AdminController@send_one_mail_post')->name('sendonepost');

    Route::get('sendmany','AdminController@send_many_mail')->name('sendmany');
    Route::post('sendmanypost','AdminController@send_many_post')->name('sendmanypost');
});