<!DOCTYPE html PUBLIC " -//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
    </title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">
    <style type="text/css">    body, html {
            width: 100% !important;
            margin: 0;
            padding: 0;
            -webkit-font-smoothing: antialiased;
            -webkit-text-size-adjust: none;
            -ms-text-size-adjust: 100%;
        }
        table td, table {
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }
        #outlook a {
            padding: 0;
        }
        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
            line-height: 100%;
        }
        .ExternalClass {
            width: 100%;
        }
        @media only screen and (max-width: 480px) {
            table, table tr td, table td {
                width: 100% !important;
            }
            img {
                width: inherit;
            }
            .layer_2 {
                max-width: 100% !important;
            }
        }
    </style>
</head>
<body style="padding: 0px; margin: 0px;">
<table style="height: 100%; width: 100%; background-color: #efefef;" align="center">
    <tbody>
    <tr>
        <td id="dbody" data-version="2.23" style="width: 100%; height: 100%; padding-top: 30px; padding-bottom: 30px; background-color: #efefef;" valign="top">
            <!--[if (gte mso 9)|(IE)]><table align="center" style="max-width:600px" width="600" cellpadding="0" cellspacing="0" border="0"><tr><td><![endif]-->
            <table class="layer_1" style="max-width: 600px; box-sizing: border-box; width: 100%;" align="center" cellspacing="0" cellpadding="0" border="0">
                <tbody>
                <tr>
                    <td class="drow" style="background-color: #ffffff; box-sizing: border-box; font-size: 0px; text-align: center;" valign="top" align="center">
                        <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td><![endif]-->
                        <div class="layer_2" style="max-width: 600px; display: inline-block; vertical-align: top; width: 100%;">
                            <table class="edcontent" style="border-collapse: collapse;width:100%" cellspacing="0" cellpadding="10" border="0">
                                <tbody>
                                <tr>
                                    <td class="edimg" style="box-sizing: border-box; text-align: center;" valign="top">
                                        <img style="border-width: 0px; border-style: none; max-width: 568px; width: 100%;" src="https://btcroyals.com/public/color/assets/img/home-bg.jpg" alt="Image" width="568">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="edbutton" valign="top">
                                        <table style="text-align: center" align="center" cellspacing="0" cellpadding="0">
                                            <tbody>
                                            <tr>
                                                <td style="border-radius: 4px; padding: 12px; background: #ffbd03 none repeat scroll 0% 0%;" align="center">
                                                    <a href="https://btcroyals.com/login" target="_blank" style="font-weight: bold; color: #ffffff; font-size: 16px; font-family: Helvetica,Arial,sans-serif; text-decoration: none; display: inline-block;"><span style="color: #ffffff;">Login &amp; Invest</span></a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                    </td>
                </tr>
                <tr>
                    <td class="drow" style="background-color: #3498db; box-sizing: border-box; font-size: 0px; text-align: center;" valign="top" align="center">
                        <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td><![endif]-->
                        <div class="layer_2" style="max-width: 600px; display: inline-block; vertical-align: top; width: 100%;">
                            <table class="edcontent" style="border-collapse: collapse;width:100%;" cellspacing="0" cellpadding="20" border="0">
                                <tbody>
                                <tr>
                                    <td class="edtext" style="text-align: left; color: #5f5f5f; font-size: 12px; font-family: Helvetica,Arial,sans-serif; direction: ltr; box-sizing: border-box;" valign="top">
                                        <p class="style2 text-center" style="color: #ffffff; font-size: 32px; font-family: Helvetica,Arial,sans-serif; margin: 0px; padding: 0px; text-align: center;">&nbsp;BTCRoyals Bitcoin Investment Platform
                                            <br>
                                        </p>
                                        <br>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                    </td>
                </tr>
                <tr>
                    <td class="drow" style="background-color: #ffffff; box-sizing: border-box; font-size: 0px; text-align: center;" valign="top" align="center">
                        <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td><![endif]-->
                        <div class="layer_2" style="max-width: 600px; display: inline-block; vertical-align: top; width: 100%;">
                            <table class="edcontent" style="border-collapse: collapse; width: 100%;" cellspacing="0" cellpadding="20" border="0">
                                <tbody>
                                <tr>
                                    <td class="edtext" style="text-align: left; color: #5f5f5f; font-size: 12px; font-family: Helvetica,Arial,sans-serif; direction: ltr; box-sizing: border-box;" valign="top">
                                        <p style="margin: 0px; padding: 0px;">
                                            <strong>Dear Valued customer,
                                            </strong>
                                            <br>
                                        </p>
                                        <p style="margin: 0px; padding: 0px;">
                                <span style="font-size: 14px;">We noticed to you have not invested on our platform since you register. Do not be left out, we are helping people get richer each day. 0.01 bitcoin to change your financial status. Click
                                  <a href="https://btcroyals.com/login" style="color: #828282; font-size: 12px; font-family: Helvetica,Arial,sans-serif; text-decoration: none;"><span style="color: #1cb52e;">here</span></a> to login now and invest in your future.
                                </span>
                                            <br>
                                        </p>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                    </td>
                </tr>
                <tr>
                    <td class="drow" style="background-color: #ffffff; box-sizing: border-box; font-size: 0px; text-align: center;" valign="top" align="center">
                        <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td><![endif]-->
                        <div class="layer_2" style="max-width: 600px; display: inline-block; vertical-align: top; width: 100%;">
                            <table class="edcontent" style="border-collapse: collapse;width:100%" cellspacing="0" cellpadding="20" border="0">
                                <tbody>
                                <tr>
                                    <td class="edtext" style="text-align: left; color: #5f5f5f; font-size: 12px; font-family: Helvetica,Arial,sans-serif; direction: ltr; box-sizing: border-box;" valign="top">
                                        <p class="style1 text-center" style="color: #000000; font-size: 28px; font-family: Helvetica,Arial,sans-serif; margin: 0px; padding: 0px; text-align: center;">
                                <span style="color: #3498db;">Invest Bitcoin
                                </span>
                                            <br>
                                        </p>
                                        <br>
                                        <p class="text-center" style="margin: 0px; padding: 0px; text-align: center;">Invest any amount Bitcoins from 0.01 BTC.&nbsp;&nbsp; Our platform helps you grow your Bitcoins at a constant rate of 3.33 percent daily.
                                        </p>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                    </td>
                </tr>
                <tr>
                    <td class="drow" style="background-color: #ffffff; box-sizing: border-box; font-size: 0px; text-align: center;" valign="top" align="center">
                        <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td><![endif]-->
                        <div class="layer_2" style="max-width: 600px; display: inline-block; vertical-align: top; width: 100%;">
                            <table class="edcontent" style="border-collapse: collapse; width: 100%;" cellspacing="0" cellpadding="20" border="0">
                                <tbody>
                                <tr>
                                    <td class="edtext" style="text-align: left; color: #5f5f5f; font-size: 12px; font-family: Helvetica,Arial,sans-serif; direction: ltr; box-sizing: border-box;" valign="top">
                                        <p class="style1 text-center" style="color: #000000; font-size: 28px; font-family: Helvetica,Arial,sans-serif; margin: 0px; padding: 0px; text-align: center;">
                                <span style="color: #3498db;">Make Profit
                                </span>
                                            <br>
                                        </p>
                                        <br>
                                        <p class="text-center" style="margin: 0px; padding: 0px; text-align: center;">Relax and watch our platform grow your bitcoins exponentially. Expect a daily return of 3.33% interest forever based on your deposit (23.31% weekly and approx. 100% monthly).
                                        </p>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                    </td>
                </tr>
                <tr>
                    <td class="drow" style="background-color: #ffffff; box-sizing: border-box; font-size: 0px; text-align: center;" valign="top" align="center">
                        <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td><![endif]-->
                        <div class="layer_2" style="max-width: 600px; display: inline-block; vertical-align: top; width: 100%;">
                            <table class="edcontent" style="border-collapse: collapse; width: 100%;" cellspacing="0" cellpadding="20" border="0">
                                <tbody>
                                <tr>
                                    <td class="edtext" style="text-align: left; color: #5f5f5f; font-size: 12px; font-family: Helvetica,Arial,sans-serif; direction: ltr; box-sizing: border-box;" valign="top">
                                        <p class="style1 text-center" style="color: #000000; font-size: 28px; font-family: Helvetica,Arial,sans-serif; margin: 0px; padding: 0px; text-align: center;">
                                <span style="color: #3498db;">Withdraw Profit
                                </span>
                                            <br>
                                        </p>
                                        <br>
                                        <p class="text-center" style="margin: 0px; padding: 0px; text-align: center;">Our payout system is fast, instant and linked with Blockchain.info. Withdrawal of profits can be made anytime and your wallet would be credited immediately. Note Blockchain network may delay payments.
                                        </p>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                    </td>
                </tr>
                <tr>
                    <td class="drow" style="background-color: #ffffff; box-sizing: border-box; font-size: 0px; text-align: center;" valign="top" align="center">
                        <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td><![endif]-->
                        <div class="layer_2" style="max-width: 600px; display: inline-block; vertical-align: top; width: 100%;">
                            <table class="edcontent" style="border-collapse: collapse; width: 100%;" cellspacing="0" cellpadding="20" border="0">
                                <tbody>
                                <tr>
                                    <td class="edtext" style="text-align: left; color: #5f5f5f; font-size: 12px; font-family: Helvetica,Arial,sans-serif; direction: ltr; box-sizing: border-box;" valign="top">
                                        <p class="style1 text-center" style="color: #000000; font-size: 28px; font-family: Helvetica,Arial,sans-serif; margin: 0px; padding: 0px; text-align: center;">
                                <span style="color: #3498db;">Tell a Friend
                                </span>
                                            <br>
                                        </p>
                                        <br>
                                        <p class="text-center" style="margin: 0px; padding: 0px; text-align: center;">Our affiliate program is designed to encourage people to refer users to our platform.
                                        </p>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                    </td>
                </tr>
                <tr>
                    <td class="drow" style="background-color: #ffffff; box-sizing: border-box; font-size: 0px; text-align: center;" valign="top" align="center">
                        <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" cellpadding="0" cellspacing="0" border="0"><tr><td><![endif]-->
                        <div class="layer_2" style="max-width: 600px; display: inline-block; vertical-align: top; width: 100%;">
                            <table class="edcontent" style="border-collapse: collapse;width:100%" cellspacing="0" cellpadding="10" border="0">
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                        <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                    </td>
                </tr>
                </tbody>
            </table>
            <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>
