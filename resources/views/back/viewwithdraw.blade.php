@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                WITHDRAW REQUEST VIEW
                <small>USERS WITHDRAW REQUEST IN THE SYSTEM</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Withdraw view</li>
            </ol>
        </section>
        @include('partials.errorbag')
        @if(session()->has('info'))
            @include('partials.error',['type'=>session('info')['type'],'message'=> session('info')['message']])
        @endif
        <section class="content">
            <div class="box">
                <div class="box-header with-border">
                </div>
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Username</th>
                                <th>Email</th>
                                <th>BTC address</th>
                                <th>BTC deposit address</th>
                                <th>Request Date </th>
                                <th>Total Deposit</th>
                                <th>Gateway Deposit</th>
                                <th>To Withdraw</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $dd)
                                <span id="user{{$dd->account_id}}" data-content="{{$dd->account_id}}|{{$dd->bit_user_address}}|{{$dd->amount}}"></span>
                                <tr>
                                    <td>{{ ucfirst($dd->account->user->name) }}</td>
                                    <td>{{ ucfirst($dd->account->user->email) }}</td>
                                    <td><span class="btn btn-default">{{ $dd->bit_user_address }}</span></td>
                                    <td><a class="btn btn-default">{{ $dd->bit_address }}</a></td>
                                    <td>{{ $dd->created_at }}</td>
                                    <td>{{ sprintf('%.8f',$dd->account->total_deposit) }}</td>
                                    <td>{{ sprintf('%.8f',$dd->account->gateway_deposit) }}</td>
                                    <td>{{ sprintf('%.8f',$dd->amount) }}</td>
                                    <td><button type="submit" onclick="event.preventDefault();
                                                var ele = document.getElementById('user{{$dd->account_id}}').getAttribute('data-content');
                                                document.getElementById('userdata').setAttribute('value',ele);
                                                var ok = confirm('Do you still want to proceed?');if(ok == true){
                                                document.getElementById('form1').submit();}" class="btn btn-info btn-block">Pay User</button> </td>
                                   <td><button type="submit" onclick="event.preventDefault();
                                                var ele = document.getElementById('user{{$dd->account_id}}').getAttribute('data-content');
                                                document.getElementById('userdatacancel').setAttribute('value',ele);
                                                var ok = confirm('Do you still want to cancel the withdrawal?');if(ok == true){
                                                document.getElementById('form2').submit();}" class="btn btn-info btn-block">Cancel</button> </td>             
                                </tr>
                            @endforeach
                        </table>
                        <form method="post" id="form1" action="{{ route('make_single_payment') }}">
                            {{csrf_field()}}
                            <input type="hidden" value="" id="userdata" name="user"/>
                        </form>
                       <form method="post" id="form2" action="{{ route('cancel_payment') }}">
                            {{csrf_field()}}
                            <input type="hidden" value="" id="userdatacancel" name="user"/>
                        </form>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    {{ $users->links() }}
                </div>
                <!-- /.box-footer-->
            </div>
        </section>
    </div>
@endsection