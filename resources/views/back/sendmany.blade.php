@extends('layouts.app')


@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Send Mail
                <small>SEND TO MANY</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Mail</li>
            </ol>
        </section>
        @include('partials.errorbag')
        @if(session()->has('info'))
            @include('partials.error',['type'=>session('info')['type'],'message'=> session('info')['message']])
        @endif
        <section class="content">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Send email to all</h3>
                </div>
                <div class="box-body">
                    <div class="col-md-offset-4 col-md-4">
                        <form role="form" method="POST" action="{{ route('sendmanypost') }}">
                            {{ csrf_field() }}
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Message</label>
                                    <textarea class="form-control" name="message" cols="3" rows="8">{{old('message')}}</textarea>
                                </div>
                            </div>
                            <!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary btn-block">Send</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">

                </div>
                <!-- /.box-footer-->
            </div>
        </section>

    </div>
@endsection