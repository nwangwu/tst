@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                USERS VIEW
                <small>USERS IN THE SYSTEM</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">User view</li>
            </ol>
        </section>
        @include('partials.errorbag')
        @if(session()->has('info'))
            @include('partials.error',['type'=>session('info')['type'],'message'=> session('info')['message']])
        @endif
        <section class="content">
            <div class="box">
                <div class="box-header with-border">
                    <a href="{{ route('deleted_user') }}" class="btn btn-warning" >See Deleted Users</a>
                </div>
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Username</th>
                                <th>Parent</th>
                                <th>BTC address</th>
                                <th>BTC deposit address</th>
                                <th>Last login </th>
                                <th>Total Deposit</th>
                                <th>Gateway Deposit</th>
                                <th>Total Earned</th>
                                <th>Balance</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $dd)
                                <tr>
                                    <td  id="user{{ $dd->id }}" data-content="{{$dd->id}}">{{ ucfirst($dd->name) }}</td>
                                    <td>{{ $dd->parent }}</td>
                                    <td><span class="btn btn-default">{{ $dd->bit_owner_address }}</span></td>
                                    <td><a class="btn btn-default">{{ $dd->bit_deposit_address}}</a></td>
                                    <td>{{ $dd->last_login }}</td>
                                    <td>{{ $dd->account->total_deposit }}</td>
                                    <td>{{ $dd->account->gateway_deposit }}</td>
                                    <td>{{ $dd->account->total_earned }}</td>
                                    <td>{{ $dd->account->balance }}</td>
                                    <td><a href="#" class="btn btn-danger" onclick="event.preventDefault();
                                                var x =  document.getElementById('user{{ $dd->id }}').getAttribute('data-content');
                                                document.getElementById('input1').setAttribute('value',x);
                                                document.getElementById('form1').submit();">Delete</a> </td>
                                </tr>
                            @endforeach
                            <form id="form1" action="{{ route('remove_user') }}" method="post" style="display: none">
                                {{csrf_field()}}
                                <input type="hidden" id="input1" name="userform" value=""/>
                            </form>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    {{ $users->links() }}
                </div>
                <!-- /.box-footer-->
            </div>
        </section>
    </div>
@endsection