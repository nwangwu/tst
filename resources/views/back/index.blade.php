@extends('layouts.app')

@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Dashboard
                <small>CONTROL YOUR PROFIT</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ol>
        </section>
        @if(session()->has('info'))
            @include('partials.error',['type'=>session('info')['type'],'message'=> session('info')['message']])
        @endif
        <section class="content">
            <!-- Info boxes -->
            <div class="row">
                <div class="col-md-12 bg-light-blue-gradient">
                    <h2>Admin Data</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-aqua"><i class="fa fa-btc"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">TOTAL DEPOSIT</span>
                            <span class="info-box-number">{{ sprintf('%.8f',$total_site_deposit)  }}<i class="fa fa-btc"></i></span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-red"><i class="fa fa-cog"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">ACTIVE USERS</span>
                            <span class="info-box-number">{{ $active_users }}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->

                <!-- fix for small devices only -->
                <div class="clearfix visible-sm-block"></div>

                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-green"><i class="fa fa-money"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">TOTAL USERS</span>
                            <span class="info-box-number">{{ $registered_users }}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-yellow"><i class="fa fa-hourglass-half"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">TOTAL WITHDRAW</span>
                            <span class="info-box-number">{{ sprintf('%.8f',$total_site_withdraw) }}<i class="fa fa-btc"></i> <div id="countdown_valor"></div></span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <div class="row">
                <div class="col-md-12 bg-gray-light">
                    <h2>Site Data</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-aqua"><i class="fa fa-btc"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">TOTAL DEPOSIT</span>
                            <span class="info-box-number">{{ sprintf('%.8f',$total_site_deposit + env('FALSE_DEPOSIT'))  }}<i class="fa fa-btc"></i></span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-red"><i class="fa fa-cog"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">ACTIVE USERS</span>
                            <span class="info-box-number">{{ $active_users + env('FALSE_ACTIVE_USER') }}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->

                <!-- fix for small devices only -->
                <div class="clearfix visible-sm-block"></div>

                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-green"><i class="fa fa-money"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">TOTAL USERS</span>
                            <span class="info-box-number">{{ $registered_users + env('FALSE_REGISTERED_USER')}}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-yellow"><i class="fa fa-hourglass-half"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">TOTAL WITHDRAW</span>
                            <span class="info-box-number">{{ sprintf('%.8f',$total_site_withdraw + env('FALSE_WITHDRAW')) }}<i class="fa fa-btc"></i> <div id="countdown_valor"></div></span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
            </div>


        </section>

    </div>

@endsection()