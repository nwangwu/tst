@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                SETTINGS
                <small>CHANGE YOUR PROFILE SETTINGS</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Setting</li>
            </ol>
        </section>
        @include('partials.errorbag')
        @if(session()->has('info'))
            @include('partials.error',['type'=>session('info')['type'],'message'=> session('info')['message']])
        @endif
        <section class="content">
            <div class="box">
                <div class="box-header with-border">
                </div>
                <div class="box-body">

                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="col-md-4">
                        <div class="box box-primary">
                            <div class="box-body box-profile">
                                <h3 class="profile-username text-center">{{ucfirst(auth()->user()->name)}}</h3>
                                <ul class="list-group list-group-unbordered">

                                    <li class="list-group-item">
                                        <b>Email</b> <a class="pull-right">{{auth()->user()->email}}</a>
                                    </li>
                                    <li class="list-group-item">
                                        <b>Parent</b><a class="pull-right">{{ auth()->user()->parent }}</a>
                                    </li>
                                </ul>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="box box-info">
                            <!-- /.box-header -->
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Quick Example</h3>
                                </div>
                                <!-- /.box-header -->
                                <!-- form start -->
                                <form role="form" method="post" action="{{route('changepasscode')}}">
                                    {{ csrf_field() }}
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="password">New Password</label>
                                            <input class="form-control" name="password" id="password" placeholder="Password" type="password">
                                        </div>
                                        <div class="form-group">
                                            <label for="confirm_password">Re-enter New Password</label>
                                            <input class="form-control" name="password_confirmation" placeholder="Password" type="password">
                                        </div>
                                    </div>
                                    <!-- /.box-body -->

                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-primary btn-block">Save Settings</button>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- /.box-footer-->
            </div>
        </section>
    </div>
@endsection