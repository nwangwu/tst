@extends('layouts.app')

@section('content')

    <div class="content-wrapper">

        <section class="content-header">
            <h1>
                False User
                <small>UPDATE A FALSE USER</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">update user</li>
            </ol>
        </section>
        @include('partials.errorbag')
        @if(session()->has('info'))
            @include('partials.error',['type'=>session('info')['type'],'message'=> session('info')['message']])
        @endif
        <section class="content">
            <div class="col-md-offset-4 col-md-4 col-sm-offset-3 col-sm-6 col-xs-12">
                <div class="register-box-body">
                    <p class="login-box-msg"></p>

                    <form action="{{ route('user_update_post') }}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }} has-feedback">
                            <input type="text" name="name" class="form-control" placeholder="Username">
                            <span class="glyphicon glyphicon-user form-control-feedback"></span>

                            @if ($errors->has('name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <input type="hidden" name="user_id" value="{{ $user_id }}"/>
                        <div class="row">
                            <!-- /.col -->
                            <div class="col-xs-12">
                                <button type="submit" class="btn btn-primary btn-block btn-flat">Add User</button>
                            </div>
                            <!-- /.col -->
                        </div>
                    </form>
                </div>

            </div>
        </section>
    </div>
@endsection