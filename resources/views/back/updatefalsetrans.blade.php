@extends('layouts.app')

@section('content')

    <div class="content-wrapper">

        <section class="content-header">
            <h1>
                False Transaction
                <small>UPDATE A FALSE trans</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">update trans</li>
            </ol>
        </section>
        @include('partials.errorbag')
        @if(session()->has('info'))
            @include('partials.error',['type'=>session('info')['type'],'message'=> session('info')['message']])
        @endif
        <section class="content">
            <div class="col-md-offset-4 col-md-4 col-sm-offset-3 col-sm-6 col-xs-12">
                <div class="register-box-body">
                    <p class="login-box-msg"></p>

                    <form action="{{ route('update_trans_post') }}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group {{ $errors->has('trans_type') ? ' has-error' : '' }} has-feedback">
                            <select class="form-control" name="trans_type">
                                <option value="">Select Trans type</option>
                                <option value="withdraw">Withdraw</option>
                                <option value="deposit">Deposit</option>
                            </select>
                            <small>previous type: {{ $result->trans_type }}</small>
                            @if ($errors->has('trans_type'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('trans_type') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('amount') ? ' has-error' : '' }} has-feedback">
                            <input type="text" name="amount" class="form-control" placeholder="{{ $result->amount }}">
                            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                            @if ($errors->has('amount'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('amount') }}</strong>
                                    </span>
                            @endif
                        </div>

                        <div class="form-group {{ $errors->has('updated_at') ? ' has-error' : '' }} has-feedback">
                            <input type="datetime" name="updated_at" class="form-control" placeholder="{{ $result->updated_at }}">
                            <small>Format: mm/dd/yyyy hh:mm:ss</small>
                            <small>Eg: 08/27/2017 20:19:45</small>
                            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                            @if ($errors->has('updated_at'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('updated_at') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <input type="hidden" name="trans_id" value="{{$result->id}}">
                        <div class="row">
                            <!-- /.col -->
                            <div class="col-xs-12">
                                <button type="submit" class="btn btn-primary btn-block btn-flat">Update Trans</button>
                            </div>
                            <!-- /.col -->
                        </div>
                    </form>
                </div>

            </div>
        </section>
    </div>
@endsection