@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                DELETED ADMINS
                <small>ADMINS IN THE SYSTEM</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Admin view</li>
            </ol>
        </section>
        @include('partials.errorbag')
        @if(session()->has('info'))
            @include('partials.error',['type'=>session('info')['type'],'message'=> session('info')['message']])
        @endif
        <section class="content">
            <div class="box">
                <div class="box-header with-border">
                    <a href="{{route('view_admin')}}" class="btn btn-primary" >See Admins</a>
                </div>
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Username</th>
                                <th>Parent</th>
                                <th>BTC address</th>
                                <th>BTC deposit address</th>
                                <th>Deleted at</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($admins as $dd)
                                <tr class="{{ ($dd->role_id == 1)?"bg-gray":"" }}">
                                    <td id="user{{ $dd->id }}" data-content="{{$dd->id}}">{{ ucfirst($dd->name) }}</td>
                                    <td>{{ $dd->parent }}</td>

                                    <td><span class="btn btn-default">{{ $dd->bit_owner_address }}</span></td>
                                    <td><a class="btn btn-default">{{ $dd->bit_deposit_address }}</a></td>
                                    <td>{{ $dd->deleted_at }}</td>
                                    <td><a href="#" class="btn btn-warning" onclick="event.preventDefault();
                                                var x =  document.getElementById('user{{ $dd->id }}').getAttribute('data-content');
                                                document.getElementById('input1').setAttribute('value',x);
                                                document.getElementById('form1').submit();">Restore</a> </td>
                                </tr>
                            @endforeach
                            <form id="form1" action="{{ route('restore_admin') }}" method="post" style="display: none">
                                {{csrf_field()}}
                                <input type="hidden" id="input1" name="adminform" value=""/>
                            </form>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    {{ $admins->links() }}
                </div>
                <!-- /.box-footer-->
            </div>
        </section>
    </div>
@endsection