@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                USERS VIEW
                <small>FALSE USERS IN THE SYSTEM</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">USERS view</li>
            </ol>
        </section>
        @include('partials.errorbag')
        @if(session()->has('info'))
            @include('partials.error',['type'=>session('info')['type'],'message'=> session('info')['message']])
        @endif
        <section class="content">
            <div class="box">
                <div class="box-header with-border">
                </div>
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Username</th>
                                <th>Parent</th>
                                <th>BTC address</th>
                                <th>BTC deposit address</th>
                                <th>Action 1 </th>
                                <th>Action 2</th>
                                <th>Action 3</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $dd)
                                <tr class="{{($dd->role_id == 3)?"bg-gray":"" }}">
                                    <td id="user{{ $dd->id }}" data-content="{{$dd->id}}">{{ ucfirst($dd->name) }}</td>
                                    <td>{{ $dd->parent }}</td>

                                    <td><span class="btn btn-default">{{ $dd->bit_owner_address }}</span></td>
                                    <td><a class="btn btn-default">{{ $dd->bit_deposit_address }}</a></td>
                                    <td><a href="#" class="btn btn-info" onclick="event.preventDefault();
                                                var x =  document.getElementById('user{{ $dd->id }}').getAttribute('data-content');
                                                document.getElementById('input2').setAttribute('value',x);
                                                document.getElementById('form2').submit();">Add Trans</a> </td></td>
                                    <td><a href="#" class="btn btn-warning" onclick="event.preventDefault();
                                                var x =  document.getElementById('user{{ $dd->id }}').getAttribute('data-content');
                                                document.getElementById('input1').setAttribute('value',x);
                                                document.getElementById('form1').submit();">View Trans</a> </td>
                                    <td>
                                        <a href="#" class="btn btn-success" onclick="event.preventDefault();
                                                var x =  document.getElementById('user{{ $dd->id }}').getAttribute('data-content');
                                                document.getElementById('input3').setAttribute('value',x);
                                                document.getElementById('form3').submit();">Edit Uname</a>
                                    </td>
                                </tr>
                            @endforeach
                            <form id="form1" action="{{ route('view_trans') }}" method="post" style="display: none">
                                {{csrf_field()}}
                                <input type="hidden" id="input1" name="user_id" value=""/>
                            </form>
                            <form id="form2" action="{{ route('add_trans') }}" method="post" style="display: none">
                                {{csrf_field()}}
                                <input type="hidden" id="input2" name="user_id" value=""/>
                            </form>
                            <form id="form3" action="{{ route('user_update') }}" method="post" style="display: none">
                                {{csrf_field()}}
                                <input type="hidden" id="input3" name="user_id" value=""/>
                            </form>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    {{ $users->links() }}
                </div>
                <!-- /.box-footer-->
            </div>
        </section>
    </div>
@endsection