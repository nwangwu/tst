@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                WALLET INFORMATION
                <small>WALLET DATA</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Wallet</li>
            </ol>
        </section>
        @include('partials.errorbag')
        @if(session()->has('info'))
            @include('partials.error',['type'=>session('info')['type'],'message'=> session('info')['message']])
        @endif
        <section class="content">
            <div class="box">
                <div class="box-header with-border">
                </div>
                <div class="box-body">
                    <div class="row">
                    <div class="col-md-12">
                        <h1>Wallet Balance: @if($balance > 0){{$balance/env('SATOSHI',100000000)}} BTC @else {{$balance}} SATOSHI @endif</h1>
                    </div>
                    </div>

                    <div class="col-md-12">
                        <div class="box box-info">
                            <!-- /.box-header -->
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title"></h3>
                                </div>
                                <!-- /.box-header -->
                                <!-- form start -->
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>Username</th>
                                            <th>Address</th>
                                            <th>Total received</th>
                                            <th>Balance</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($address_list as $dd)
                                            <tr>
                                                <td>{{ $dd['label'] }}</td>
                                                <td><span class="btn btn-default">{{ $dd['address'] }}</span></td>
                                                <td>@if($dd['total_received'] > 0){{$dd['total_received']/env('SATOSHI',100000000)}} BTC @else{{$dd['total_received']}} @endif</td>
                                                <td>@if($dd['balance'] > 0){{$dd['balance']/env('SATOSHI',100000000)}} BTC @else {{ $dd['balance'] }} @endif</td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection