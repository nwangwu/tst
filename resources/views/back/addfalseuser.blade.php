@extends('layouts.app')

@section('content')

<div class="content-wrapper">

    <section class="content-header">
        <h1>
            False User
            <small>ADD A FALSE USER</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">New user</li>
        </ol>
    </section>
    @include('partials.errorbag')
    @if(session()->has('info'))
    @include('partials.error',['type'=>session('info')['type'],'message'=> session('info')['message']])
    @endif
    <section class="content">
        <div class="col-md-offset-4 col-md-4 col-sm-offset-3 col-sm-6 col-xs-12">
            <div class="register-box-body">
                <p class="login-box-msg"></p>

                <form action="{{ route('create_user_post') }}" method="post">
                    {{ csrf_field() }}
                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }} has-feedback">
                        <input type="text" name="name" class="form-control" placeholder="Username">
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>

                        @if ($errors->has('name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }} has-feedback">
                        <input type="email" name="email" class="form-control" placeholder="Email">
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>

                        @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }} has-feedback">
                        <input type="password" name="password" class="form-control" placeholder="Password">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>

                        @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" name="password_confirmation" class="form-control" placeholder="Retype password">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="row">
                        <!-- /.col -->
                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Add User</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>
            </div>

        </div>
    </section>
</div>
@endsection