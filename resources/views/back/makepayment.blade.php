@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                Mass Payment VIEW
                <small>BTC Payment</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Payment view</li>
            </ol>
        </section>
        @include('partials.errorbag')
        @if(session()->has('info'))
            @include('partials.error',['type'=>session('info')['type'],'message'=> session('info')['message']])
        @endif
        <section class="content">
            <div class="box">
                <div class="box-header with-border">
                </div>
                <div class="box-body">
                    <form method="post" action="{{ route('make_payment') }}" id="form1">
                        {{ csrf_field() }}
                        <button type="submit" onclick="event.preventDefault();
                                var ok = confirm('Do you still want to proceed?');if(ok == true){document.getElementById('form1').submit(); }" class="btn btn-primary">Make Payment</button>

                        <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Email</th>
                                <th>BTC address</th>
                                <th>BTC deposit address</th>
                                <th>Total Deposit</th>
                                <th>Gateway Deposit</th>
                                <th>To withdraw</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $dd)
                                <tr>
                                    <td>{{ ucfirst($dd->account->user->email) }}</td>
                                    <td><span class="btn btn-default">{{ $dd->bit_user_address }}</span></td>
                                    <td><a class="btn btn-default">{{ $dd->bit_deposit_address }}</a></td>
                                    <td>{{ sprintf('%.8f',$dd->account->total_deposit) }}</td>
                                    <td>{{ sprintf('%.8f',$dd->account->gateway_deposit) }}</td>
                                    <td>{{ sprintf('%.8f',$dd->amount) }}</td>
                                    <td><input type="checkbox" class="form-control" name="btcaddress[]" value="{{ $dd->account_id }}|{{ $dd->bit_user_address }}|{{ $dd->amount }}"/> </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    </form>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    {{ $users->links() }}
                </div>
                <!-- /.box-footer-->
            </div>
        </section>
    </div>
@endsection