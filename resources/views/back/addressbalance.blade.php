@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                ADDRESS
                <small>QUERY ADDRESS BALANCE</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Address balance</li>
            </ol>
        </section>
        @include('partials.errorbag')
        @if(session()->has('info'))
            @include('partials.error',['type'=>session('info')['type'],'message'=> session('info')['message']])
        @endif
        <section class="content">
            <div class="box">
                <div class="box-header with-border">
                </div>
                <div class="box-body">
                    <div class="col-md-4">
                        @if(isset($balance))
                            <h3>Address: <span class="btn btn-default">{{ $address }}</span></h3>
                            <h2>Balance: {{ $balance }} SATOSHI</h2>
                        @endif
                    </div>
                    <div class="col-md-8">
                        <div class="box box-info">
                            <!-- /.box-header -->
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title"></h3>
                                </div>
                                <!-- /.box-header -->
                                <!-- form start -->
                                <form role="form" method="post" action="{{route('address_balance_post')}}">
                                    {{ csrf_field() }}
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">BTC Address</label>
                                            <input class="form-control" name="btcaddress"  placeholder="Address" type="text">
                                            <small>Address must exist in the wallet for it to return the balance</small>
                                        </div>
                                    </div>
                                    <!-- /.box-body -->
                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-primary btn-block">Check balance</button>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection