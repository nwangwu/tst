@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                FALSE SETTINGS VIEW
                <small>FALSE WITHDRAW & DEPOSIT IN THE SYSTEM</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">DEPOSIT</li>
            </ol>
        </section>
        @include('partials.errorbag')
        @if(session()->has('info'))
            @include('partials.error',['type'=>session('info')['type'],'message'=> session('info')['message']])
        @endif
        <section class="content">
            <div class="box">
                <div class="box-header with-border">
                </div>
                <div class="box-body">
                    <div class="col-md-8">
                        <div class="box box-info">
                            <!-- /.box-header -->
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title"></h3>
                                </div>
                                <!-- /.box-header -->
                                <!-- form start -->
                                <form role="form" method="post" action="{{route('wallet_setting_post')}}">
                                    {{ csrf_field() }}

                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Investments</label>
                                            <input class="form-control" name="invest" id="key" value="" placeholder="Investments" type="text">
                                            <small>Old value is:  {{ $envv->bit_owner_address }}</small>
                                        </div>
                                        <div class="form-group">
                                            <label for="password">Payouts</label>
                                            <input class="form-control" name="payouts" id="keyvalue" placeholder="Payouts" type="text">
                                            <small>Old value is: {{  $envv->bit_deposit_address  }}</small>
                                        </div>
                                        <div class="form-group">
                                            <label for="password">Registered Users</label>
                                            <input class="form-control" name="registerusers" id="keyvalue" placeholder="Registered users" type="text">
                                            <small>Old value is: {{ $envv->password }}</small>
                                        </div>
                                        <div class="form-group">
                                            <label for="password">Active Users</label>
                                            <input class="form-control" name="activeusers" id="keyvalue" placeholder="Active Users" type="text">
                                            <small>Old value is: {{  $envv->confirmation_code }}</small>
                                        </div>
                                    </div>
                                    <!-- /.box-body -->

                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-primary btn-block">Save Settings</button>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection