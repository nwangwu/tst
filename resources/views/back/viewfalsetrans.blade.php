@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                FALSE TRANSACTION VIEW
                <small>TRANSACTIONS IN THE SYSTEM</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Transaction view</li>
            </ol>
        </section>
        @include('partials.errorbag')
        @if(session()->has('info'))
            @include('partials.error',['type'=>session('info')['type'],'message'=> session('info')['message']])
        @endif
        <section class="content">
            <div class="box">
                <div class="box-header with-border">

                </div>
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Type</th>
                                <th>amount</th>
                                <th>create_at</th>
                                <th>updated_at</th>
                                <th>Action </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($trans as $dd)
                                <tr>
                                    <td  id="user{{ $dd->id }}" data-content="{{$dd->id}}">{{ ucfirst($dd->trans_type) }}</td>
                                    <td>{{ sprintf('%.8f',$dd->amount) }}</td>
                                    <td>{{ $dd->created_at }}</td>
                                    <td>{{ $dd->updated_at }}</td>
                                    <td><a href="#" class="btn btn-danger" onclick="event.preventDefault();
                                                var x =  document.getElementById('user{{ $dd->id }}').getAttribute('data-content');
                                                document.getElementById('input1').setAttribute('value',x);
                                                document.getElementById('form1').submit();">Update</a> </td>
                                </tr>
                            @endforeach
                            <form id="form1" action="{{ route('update_trans') }}" method="post" style="display: none">
                                {{csrf_field()}}
                                <input type="hidden" id="input1" name="transform" value=""/>
                            </form>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                </div>
                <!-- /.box-footer-->
            </div>
        </section>
    </div>
@endsection