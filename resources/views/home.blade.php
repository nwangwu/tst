@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    You are logged in!

                </div>
            </div>
        </div>
    </div>
</div>

<div class="row  vert-offset-top-1 text-center">
    <div class="col-md-6">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Latest Deposits</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table no-margin">
                        <tbody>
                        @foreach($deposit as $depo)
                            <tr>
                                <td><span class="label label-success">Deposit</span></td>
                                <td>{{ $depo->account->user->name }}</td>
                                <td class="text-info">{{ sprintf('%.8f',$depo->amount) }}<i class="fa fa-btc"></i></td>
                                <td class="text-green">{{ $depo->status == 'reinvest' ? 'Reinvest' : 'Bitcoin'}}</td>
                                <td>{{ $depo->updated_at }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.box-footer -->
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Latest Withdrawals</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table no-margin">
                        <tbody>
                        @foreach($withdraw as $withd)
                            <tr>
                                <td><span class="label label-danger">Withdraw</span></td>
                                <td>{{ $withd->account->user->name }}</td>
                                <td class="text-green">{{ sprintf('%.8f',$withd->amount) }}<i class="fa fa-btc"></i></td>
                                <td>{{ $withd->updated_at }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>

        </div>
    </div>
</div>
@endsection
