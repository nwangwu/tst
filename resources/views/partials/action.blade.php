<a class="btn btn-primary"
   onclick="event.preventDefault();
           var x = document.getElementById('commission{{ $slot }}').getAttribute('data-content');
           document.getElementById('referral_commission{{ $slot }}').setAttribute('value',x);
           document.getElementById('affiliate_process{{$slot}}').submit();">
    Invest
</a>
<form id="affiliate_process{{ $slot }}" method="post" action="{{route('affiliate_post')}}" style="display: none">
    {{csrf_field()}}
    <input type="hidden" name="commission" id="referral_commission{{ $slot }}" value="" />
    <input type="hidden" name="child_id" value="{{ $slot }}" />
</form>