@extends('layouts.template')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                ACCOUNT
                <small>CREATE YOUR USERNAME TO COMPLETE SIGNIN</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Account</li>
            </ol>
        </section>
        @include('partials.errorbag')
        @if(session()->has('info'))
            @include('partials.error',['type'=>session('info')['type'],'message'=> session('info')['message']])
        @endif

        <section class="content">
            <div class="col-md-offset-4 col-md-4 col-sm-offset-3 col-sm-6 col-xs-12">
                <div class="login-box-body">
                    <p class="login-box-msg"></p>

                    <form action="{{ route('create_username_post') }}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group {{$errors->has('name') ? ' has-error': ''}} has-feedback ">
                            <input type="text" name="name" class="form-control" placeholder="Create Username" value="{{ old('name') }}" required autofocus>
                            <span class="glyphicon glyphicon-user form-control-feedback"></span>
                            @if ($errors->has('name'))
                                <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="row">
                            <!-- /.col -->
                            <div class="col-xs-4">
                                <button type="submit" class="btn btn-primary btn-block btn-flat">Create</button>
                            </div>
                            <!-- /.col -->
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
@endsection