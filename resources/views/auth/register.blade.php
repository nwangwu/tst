@extends('layouts.template')


@section('content')

    <div class="content-wrapper">

        <section class="content-header">
            <h1>
                Register
                <small>Register YOUR ACCOUNT</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Login</li>
            </ol>
        </section>
        <section class="content">
            <div class="col-md-offset-4 col-md-4 col-sm-offset-3 col-sm-6">
                <div class="register-box-body">
                    <p class="register-box-msg"><small>Your referral is: {{ !is_null(session('referral'))?session('referral'):'Admin' }}</small></p>

                    <form action="{{ route('register') }}" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="referral" value="{{session('referral')}}"/>
                        <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }} has-feedback">
                            <input type="text" name="name" value="{{ old('name') }}" class="form-control" placeholder="Username">
                            <span class="glyphicon glyphicon-user form-control-feedback"></span>

                            @if ($errors->has('name'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }} has-feedback">
                            <input type="email" name="email" value="{{old('email')}}" class="form-control" placeholder="Email">
                            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }} has-feedback">
                            <input type="password" name="password" class="form-control" placeholder="Password">
                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>

                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="form-group has-feedback">
                            <input type="password" class="form-control" name="password_confirmation" placeholder="Retype password">
                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        </div>
                        <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }} has-feedback">
                            <input type="checkbox" name="agree">
                            by signing up, I agree to the <a href="{{ route('tos') }}">terms of service</a> and the
                            <a href="{{ route('privacy_policy') }}"> privacy <polic></polic>y</a> of Cryptoflare

                            @if ($errors->has('agree'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('agree') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="form-group has-feedback">
                            {!! app('captcha')->display() !!}
                        </div>
                        <div class="row">
                            <!-- /.col -->
                            <div class="col-xs-12">
                                <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
                            </div>
                            <!-- /.col -->
                        </div>
                    </form>

                    <div class="social-auth-links text-center">
                        <p>- OR -</p>
                        <a href="{{ route('facebook') }}" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign up using
                            Facebook</a>
                    </div>

                    <a href="{{ route('login') }}" class="text-center">I already have a membership</a>
                </div>

            </div>
        </section>
    </div>
@endsection