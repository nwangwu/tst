@extends('layouts.template')


@section('content')

    <div class="content-wrapper">

        <section class="content-header">
            <h1>
                LOGIN
                <small>LOGIN INTO YOUR ACCOUNT</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Login</li>
            </ol>
        </section>

        @include('partials.errorbag')
        @if(session()->has('info'))
            @include('partials.error',['type'=>session('info')['type'],'message'=> session('info')['message']])
        @endif


        <section class="content">
            <div class="col-md-offset-4 col-md-4 col-sm-offset-3 col-sm-6">
            <div class="login-box-body">
                <p class="login-box-msg"></p>

                <form action="{{ route('login') }}" method="post">
                    {{ csrf_field() }}
                    <div class="form-group {{($errors->has('name') === null || $errors->has('email') === null) ? ' has-error': ''}} has-feedback ">
                        <input type="text" name="log" class="form-control" placeholder="Username or Email" value="" required autofocus>
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group {{$errors->has('password') ? ' has-error': ''}} has-feedback">
                        <input type="password" name="password" class="form-control" placeholder="Password" required />
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-xs-8">
                            <div class="checkbox icheck">
                                <label>
                                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                </label>
                            </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-xs-4">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>

                <div class="social-auth-links text-center">
                    <p>- OR -</p>
                    <a href="{{ route('facebook') }}" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using
                        Facebook</a>
                </div>
                <!-- /.social-auth-links -->

                <a href="{{ route('password.request') }}">I forgot my password</a><br>
                <a href="{{ route('register') }}" class="text-center">Register a new membership</a>

            </div>
            </div>
        </section>

    </div>