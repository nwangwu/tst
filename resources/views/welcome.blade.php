<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<head>
	<meta charset="utf-8" />
	<title>btcroyals.com | The top Bitcoin investment platform</title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	
	<!-- ================== BEGIN BASE CSS STYLE ================== -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <link rel="shortcut icon" href="{{ asset('color/assets/img/favicon.ico') }}" type="image/x-icon">
    <link rel="icon" href="{{ asset('color/assets/img/favicon.ico') }}" type="image/x-icon">
    <!-- Bootstrap 3.3.6 -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Font Awesome -->
    <!--<link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" integrity="sha256-j+P6EZJVrbXgwSR5Mx+eCS6FvP9Wq27MBRC/ogVriY0=" crossorigin="anonymous" />
	<link href="{{ asset('color/assets/css/style.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('color/assets/css/style-responsive.min.css')}}" rel="stylesheet" />
	<link href="{{ asset('color/assets/css/theme/default.css')}}"  rel="stylesheet" />
	<!-- ================== END BASE CSS STYLE ================== -->
	
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="{{ asset('color/assets/plugins/pace/pace.min.js')}}"></script>
	<!-- ================== END BASE JS ================== -->
</head>
<body data-spy="scroll" data-target="#header-navbar" data-offset="51">
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-104499147-1', 'auto');
    ga('send', 'pageview');

</script>
    <!-- begin #page-container -->
    <div id="page-container" class="fade">
        <!-- begin #header -->
        <div id="header" class="header navbar navbar-transparent navbar-fixed-top">
            <!-- begin container -->
            <div class="container">
                <!-- begin navbar-header -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#header-navbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="/" class="navbar-brand">
                        <span class="brand-logo"></span>
                        <span class="brand-text">
                            <span class="text-theme"><i class="fa fa-btc"></i>tc</span>Royals
                        </span>
                    </a>
                </div>
                <!-- end navbar-header -->
                <!-- begin navbar-collapse -->
                <div class="collapse navbar-collapse" id="header-navbar">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="active dropdown">
                            <a href="#home" data-click="scroll-to-target">HOME</a>
                        </li>
                        <li><a href="#about" data-click="scroll-to-target">ABOUT</a></li>
                        <li><a href="#service" data-click="scroll-to-target">HOW IT WORKS</a></li>
                        <li><a href="#client" data-click="scroll-to-target">TESTIMONIALS</a></li>
                        <li class="active dropdown">
                            <a href="#" data-toggle="dropdown">ACCOUNT <b class="caret"></b></a>
                            <ul class="dropdown-menu dropdown-menu-left animated fadeInDown">
                                <li><a href="/login">Sign In</a></li>
                                <li><a href="/register">Create Account</a></li>
                            </ul>
                        </li>
                        <li><a href="#contact" data-click="scroll-to-target">CONTACT</a></li>
                        <li><a href="{{ route("leaderboard") }}">TRANSACTIONS</a></li>
                    </ul>
                </div>
                <!-- end navbar-collapse -->
            </div>
            <!-- end container -->
        </div>
        <!-- end #header -->
        
        <!-- begin #home -->
        <div id="home" class="content has-bg home">
            <!-- begin content-bg -->
            <div class="content-bg">
                <img src="{{ asset('color/assets/img/home-bg.jpg') }}" alt="Home" />
            </div>
            <!-- end content-bg -->
            <!-- begin container -->
            <div class="container home-content">
                <h1>Welcome to <span class="fa fa-btc">tcRoyals</span></h1>
                <h3>The top Bitcoin investment platform</h3>
                <p>
                    We have created a platform where people can invest and earn Bitcoins from the comfort of their homes<br />
                    You can start earning now.
                </p>
                <a href="{{ route("register") }}" class="btn btn-theme">Register Now</a> <a href="{{ route("login") }}" class="btn btn-outline">Login</a><br />
            </div>
            <!-- end container -->
        </div>
        <!-- end #home -->
        <div id="service" class="content" data-scrollview="true">
            <!-- begin container -->
            <div class="container">
                <h2 class="content-title">How it Works</h2>

                <!-- begin row -->
                <div class="row">
                    <!-- begin col-4 -->
                    <div class="col-md-4 col-sm-4">
                        <div class="service">
                            <div class="icon bg-theme" data-animation="true" data-animation-type="bounceIn"><i class="fa fa-cog"></i></div>
                            <div class="info">
                                <h4 class="title">Sign Up</h4>
                                <p class="desc">Go through our one time registration process by filling out the form on the registration page to begin.</p>
                            </div>
                        </div>
                    </div>
                    <!-- end col-4 -->
                    <!-- begin col-4 -->
                    <div class="col-md-4 col-sm-4">
                        <div class="service">
                            <div class="icon bg-theme" data-animation="true" data-animation-type="bounceIn"><i class="fa fa-paint-brush"></i></div>
                            <div class="info">
                                <h4 class="title">Invest <span class="fa fa-btc">TC</span></h4>
                                <p class="desc">Invest any amount of Bitcoins above or equals 0.01 BTC by signing in and navigating to the deposit page.</p>
                            </div>
                        </div>
                    </div>
                    <!-- end col-4 -->
                    <!-- begin col-4 -->
                    <div class="col-md-4 col-sm-4">
                        <div class="service">
                            <div class="icon bg-theme" data-animation="true" data-animation-type="bounceIn"><i class="fa fa-file"></i></div>
                            <div class="info">
                                <h4 class="title">Make Profit</h4>
                                <p class="desc">Relax and watch our platform grow your bitcoins exponentially.
                                    Expect a daily return of {{ env('DAILY_GROWTH_RATE')*100  }}% interest forever based on your deposit
                                    ({{ env('WEEKLY_GROWTH_RATE')*100 }}% weekly and approx. {{ env('MONTHLY_GROWTH_RATE')*100 }}% monthly).</p>
                            </div>
                        </div>
                    </div>
                    <!-- end col-4 -->
                </div>
                <!-- end row -->
                <!-- begin row -->
                <div class="row">
                    <!-- begin col-4 -->
                    <div class="col-md-4 col-sm-4">
                        <div class="service">
                            <div class="icon bg-theme" data-animation="true" data-animation-type="bounceIn"><i class="fa fa-code"></i></div>
                            <div class="info">
                                <h4 class="title">Withdraw Profits</h4>
                                <p class="desc">
                                    Our payout system is fast, instant and linked with Blockchain.
                                    Withdrawal of profits can be made anytime and your wallet would
                                    be credited immediately. Note Blockchain network may delay payments.
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- end col-4 -->
                    <!-- begin col-4 -->
                    <div class="col-md-4 col-sm-4">
                        <div class="service">
                            <div class="icon bg-theme" data-animation="true" data-animation-type="bounceIn"><i class="fa fa-shopping-cart"></i></div>
                            <div class="info">
                                <h4 class="title">Reinvest Coins</h4>
                                <p class="desc">After trying out our platform, we encourage you to reinvest and earn more..</p>
                            </div>
                        </div>
                    </div>
                    <!-- end col-4 -->
                    <!-- begin col-4 -->
                    <div class="col-md-4 col-sm-4">
                        <div class="service">
                            <div class="icon bg-theme" data-animation="true" data-animation-type="bounceIn"><i class="fa fa-heart"></i></div>
                            <div class="info">
                                <h4 class="title">Tell a Friend</h4>
                                <p class="desc">We have a very nice affiliate program to encourage our users to spread the BTCRoyals good news. So, tell a friend to tell a friend</p>
                            </div>
                        </div>
                    </div>
                    <!-- end col-4 -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <div class="row content text-center" data-scrollview="true">
                <div class="col-md-6">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Latest Deposits</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="table-responsive">
                                <table class="table no-margin">
                                    <tbody>
                                    @foreach($deposit as $depo)
                                        <tr>
                                            <td><span class="label label-success">Deposit</span></td>
                                            <td>{{ $depo->account->user->name }}</td>
                                            <td class="text-info">{{ sprintf('%.8f',$depo->amount) }}<i class="fa fa-btc"></i></td>
                                            <td class="text-green">{{ $depo->status == 'reinvest' ? 'Reinvest' : 'Bitcoin'}}</td>
                                            <td>{{ $depo->updated_at }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.box-footer -->
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Latest Withdrawals</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="table-responsive">
                                <table class="table no-margin">
                                    <tbody>
                                    @foreach($withdraw as $withd)
                                        <tr>
                                            <td><span class="label label-danger">Withdraw</span></td>
                                            <td>{{ $withd->account->user->name }}</td>
                                            <td class="text-green">{{ sprintf('%.8f',$withd->amount) }}<i class="fa fa-btc"></i></td>
                                            <td>{{ $withd->updated_at }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>

                    </div>
                </div>
        </div>
        <div id="milestone" class="content bg-black-darker has-bg" data-scrollview="true">
            <!-- begin content-bg -->
            <div class="content-bg">
                <img src="{{ asset('color/assets/img/milestone-bg.jpg') }}" alt="Milestone" />
            </div>
            <!-- end content-bg -->
            <!-- begin container -->
            <div class="container">
                <!-- begin row -->
                <div class="row">
                    <!-- begin col-3 -->
                    <div class="col-md-3 col-sm-3 milestone-col">
                        <div class="milestone">
                            <div class="number" data-animation="true" data-animation-type="number" data-final-number="{{ $registered_users }}">{{ $registered_users }}</div>
                            <div class="title">Registered Users</div>
                        </div>
                    </div>
                    <!-- end col-3 -->
                    <!-- begin col-3 -->
                    <div class="col-md-3 col-sm-3 milestone-col">
                        <div class="milestone">
                            <div class="number" data-animation="true" data-animation-type="number" data-final-number="{{ $active_users }}">{{ $active_users }}</div>
                            <div class="title">Active Users</div>
                        </div>
                    </div>
                    <!-- end col-3 -->
                    <!-- begin col-3 -->
                    <div class="col-md-3 col-sm-3 milestone-col">
                        <div class="milestone">
                            <div class="number" data-animation="true" data-animation-type="numbr" data-final-number="34"><span class="fa fa-btc">{{ sprintf('%.5f',$total_site_deposit) }}</span></div>
                            <div class="title">Investments</div>
                        </div>
                    </div>
                    <!-- end col-3 -->
                    <!-- begin col-3 -->
                    <div class="col-md-3 col-sm-3 milestone-col">
                        <div class="milestone">
                            <div class="number" data-animation="true" data-animation-type="numbr" data-final-number="129"><span class="fa fa-btc">{{ sprintf('%.5f',$total_site_withdraw ) }}</span> </div>
                            <div class="title">Payouts</div>
                        </div>
                    </div>
                    <!-- end col-3 -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <!-- begin #about -->
        <div id="about" class="content" data-scrollview="true">
            <!-- begin container -->
            <div class="container" data-animation="true" data-animation-type="fadeInDown">
                <h2 class="content-title">About Us</h2>
                <p class="content-desc">
                <p>
                    The company was created by a group of qualified experts, professional bankers,
                    traders and analysts who specialized in the stock, bond, futures, currencies,
                    gold, silver and oil trading with having more than ten years of extensive practical
                    experiences of combined personal skills, knowledge, talents and collective ambitions
                    for success.
                    <br>
                    BTCRoyals is a fully automated bitcoin platform operating with minimal human intervention,
                    aside from regular server maintenance. Take full advantage of our powerful and high frequency
                    investment platform. Our automated system gathers information from the blockchain transfers and
                    cryptocurrency exchanges to study and predict the bitcoin price, our servers open and close thousands
                    of transactions per minute,
                    analyzing the price difference and transaction fees, and use that information to generate profit.
                </p>
                <p>
                    We believe that superior investment performance is achieved through a
                    skillful balance of three core attributes: knowledge, experience and adaptability.
                    There is only one way to be on the cutting edge – commitment to innovation.
                    We do our best to achieve a consistent increase in investment performance for our clients,
                    and superior value-add. We appreciate our clients loyalty and value the relationships we
                    build with each customer.
                </p>

                </p>
                <!-- begin row -->
                <div class="row">
                    <!-- begin col-4 -->
                    <div class="col-md-4 col-sm-6">
                        <!-- begin about -->
                        <div class="about">
                            <h3>Why Invest With Us</h3>
                            Btcroyals.com  offer you the best chance to make Bitcoins online without leaving your home.
                            We are focusing on providing <u>stable</u>, <u>secure</u> and <u>long-time</u> investments.<br>
                            We do not ask you to send documents or share your personal informations. Our cooperation is
                            fully anonymous. <br>
                            All your deposits will run on a regular basis and bring profit of {{ env('DAILY_GROWTH_RATE')*100 }}% each calendar
                            day ({{ env('HOURLY_GROWTH_RATE')*100 }}% hourly), with the ability to instantly withdraw your funds whenever you want.<br>
                            You will get 10% of all received Bitcoin from your referral members. It's a fast
                            and easy way to make Bitcoin without your own investment.<br>
                            btcroyals.com has experienced technical staffs who are cautious and do its utmost
                            for the safety of your investment and stable hourly profits.<br><br>
                            <strong>You can start earning Bitcoins in few seconds - it's simple!</strong>
                        </div>
                        <!-- end about -->
                    </div>
                    <!-- end col-4 -->
                    <!-- begin col-4 -->
                    <div class="col-md-4 col-sm-6">
                        <h3>Our Philosophy</h3>
                        <!-- begin about-author -->
                        <div class="about-author">
                            <div class="quote bg-silver">
                                <i class="fa fa-quote-left"></i>
                                <h3>We work hard,<br /><span>so that our clients can make easy money</span></h3>
                                <i class="fa fa-quote-right"></i>
                            </div>
                            <div class="author">
                                <div class="image">
                                    <img src="{{ asset('color/assets/img/user-1.jpg') }}" alt="Sean Ngu" />
                                </div>
                                <div class="info">
                                    Williams Saltman
                                    <small>Founder, Btcroyals</small>
                                </div>
                            </div>
                        </div>
                        <!-- end about-author -->
                    </div>
                    <!-- end col-4 -->
                    <!-- begin col-4 -->
                    <div class="col-md-4 col-sm-12">
                        <h3>Our Ratings</h3>
                        <!-- begin skills -->
                        <div class="skills">
                            <div class="skills-name">Trade Analysis</div>
                            <div class="progress progress-striped">
                                <div class="progress-bar progress-bar-success" style="width: 95%">
                                    <span class="progress-number">95%</span>
                                </div>
                            </div>
                            <div class="skills-name">Payout Speed</div>
                            <div class="progress progress-striped">
                                <div class="progress-bar progress-bar-success" style="width: 90%">
                                    <span class="progress-number">90%</span>
                                </div>
                            </div>
                            <div class="skills-name">Client Interfacing</div>
                            <div class="progress progress-striped">
                                <div class="progress-bar progress-bar-success" style="width: 85%">
                                    <span class="progress-number">85%</span>
                                </div>
                            </div>
                            <div class="skills-name">ROI Guarantee</div>
                            <div class="progress progress-striped">
                                <div class="progress-bar progress-bar-success" style="width: 80%">
                                    <span class="progress-number">80%</span>
                                </div>
                            </div>
                        </div>
                        <!-- end skills -->
                    </div>
                    <!-- end col-4 -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <!-- end #about -->
    
        <!-- begin #milestone -->

        <!-- begin #quote -->
        <div id="quote" class="content bg-black-darker has-bg" data-scrollview="true">
            <!-- begin content-bg -->
            <div class="content-bg">
                <img src="{{ asset('color/assets/img/quote-bg.jpg') }}" alt="Quote" />
            </div>
            <!-- end content-bg -->
            <!-- begin container -->
            <div class="container" data-animation="true" data-animation-type="fadeInLeft">
                <!-- begin row -->
                <div class="row">
                    <!-- begin col-12 -->
                    <div class="col-md-12 quote">
                        <i class="fa fa-quote-left"></i> Passion leads to insight, insight leads to performance, <br />
                        performance leads to <span class="text-theme">success</span>!  
                        <i class="fa fa-quote-right"></i>
                        <small>Btcroyals, Marketing Team</small>
                    </div>
                    <!-- end col-12 -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <!-- end #quote -->
        
        <!-- beign #service -->

        <!-- end #about -->
        
        <!-- beign #action-box -->
        <div id="action-box" class="content has-bg" data-scrollview="true">
            <!-- begin content-bg -->
            <div class="content-bg">
                <img src="{{ asset('color/assets/img/action-bg.jpg') }}" alt="Action" />
            </div>
            <!-- end content-bg -->
            <!-- begin container -->
            <div class="container" data-animation="true" data-animation-type="fadeInRight">
                <!-- begin row -->
                <div class="row action-box">
                    <!-- begin col-9 -->
                    <div class="col-md-9 col-sm-9">
                        <div class="icon-large text-theme">
                            <i class="fa fa-binoculars"></i>
                        </div> 
                        <h3>START EARNING BITCOINS RIGHTAWAY!</h3>
                        <p>
                           You can earn in the world's top cryptocurrency now.
                        </p>
                    </div>
                    <!-- end col-9 -->
                    <!-- begin col-3 -->
                    <div class="col-md-3 col-sm-3">
                        <a href="{{ route("register") }}" class="btn btn-outline btn-block">Sign Up</a>
                    </div>
                    <!-- end col-3 -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>

        <!-- begin #client -->
        <div id="client" class="content has-bg bg-green" data-scrollview="true">
            <!-- begin content-bg -->
            <div class="content-bg">
                <img src="{{ asset('color/assets/img/client-bg.jpg') }}" alt="Client" />
            </div>
            <!-- end content-bg -->
            <!-- begin container -->
            <div class="container" data-animation="true" data-animation-type="fadeInUp">
                <h2 class="content-title">Our Client Testimonials</h2>
                <!-- begin carousel -->
                <div class="carousel testimonials slide" data-ride="carousel" id="testimonials">
                    <!-- begin carousel-inner -->
                    <div class="carousel-inner text-center">
                        <!-- begin item -->
                        <div class="item active">
                            <blockquote>
                                <i class="fa fa-quote-left"></i>
                                Bitcoin doubled in 7 days. You should not expect anything more. Excellent customer service!

                                <i class="fa fa-quote-right"></i>
                            </blockquote>
                            <div class="name"> — <span class="text-theme">Guill ManteMakati</span>, Philippines</div>
                        </div>
                        <!-- end item -->
                        <!-- begin item -->
                        <div class="item">
                            <blockquote>
                                <i class="fa fa-quote-left"></i>
                                Good and quick profit. I used this website many times and I can recommend it.

                                <i class="fa fa-quote-right"></i>
                            </blockquote>
                            <div class="name"> — <span class="text-theme">Anas SulaimanKuala Lumpur
</span>, Malaysia</div>
                        </div>
                        <!-- end item -->
                        <!-- begin item -->
                        <div class="item">
                            <blockquote>
                                <i class="fa fa-quote-left"></i>
                                I invested 0.5 at Start Plan. It wasn’t easy to wait 30 days to have my bitcoins doubled, but most important thing is this site is legit and paying. Plus for good and patient support. I will invest again.

                                <i class="fa fa-quote-right"></i>
                            </blockquote>
                            <div class="name"> — <span class="text-theme">Deepali Gandhi
</span>, Kolkata, India</div>
                        </div>
                        <!-- end item -->
                    </div>
                    <!-- end carousel-inner -->
                    <!-- begin carousel-indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#testimonials" data-slide-to="0" class="active"></li>
                        <li data-target="#testimonials" data-slide-to="1" class=""></li>
                        <li data-target="#testimonials" data-slide-to="2" class=""></li>
                    </ol>
                    <!-- end carousel-indicators -->
                </div>
                <!-- end carousel -->
            </div>
            <!-- end containter -->
        </div>
        <!-- end #client -->
        
        <!-- begin #pricing -->

        <!-- begin #contact -->
        <div id="contact" class="content bg-silver-lighter" data-scrollview="true">
            <!-- begin container -->
            <div class="container">
                <h2 class="content-title">Contact Us</h2>
                <!-- begin row -->
                <div class="row">
                    <!-- begin col-6 -->
                    <div class="col-md-6" data-animation="true" data-animation-type="fadeInLeft">
                        <h3>If you would like to talk about investing, get in touch with us.</h3>

                        <p>Btcroyals, Inc</strong><br />
                            795 Simpson Ave, Suite 600<br />
                            San Francisco, CA 94107<br />
                            P: +1 415-509-6995<br />
                        </p>
                        <p>

                            <a href="mailto:info@Btcroyals.com">info@Btcroyals.com</a>
                        </p>
                    </div>
                    <!-- end col-6 -->
                    <!-- begin col-6 -->
                    <div class="col-md-6 form-col" data-animation="true" data-animation-type="fadeInRight">
                        @if(session()->has('info'))
                            @include('partials.error',['type'=>session('info')['type'],'message'=> session('info')['message']])
                        @endif
                        <form class="form-horizontal" method="post" action="{{ route("contact_post") }}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label class="control-label col-md-3">Name <span class="text-theme">*</span></label>
                                <div class="col-md-9">
                                    <input type="text" name="username" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Email <span class="text-theme">*</span></label>
                                <div class="col-md-9">
                                    <input type="text" name="email" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Message <span class="text-theme">*</span></label>
                                <div class="col-md-9">
                                    <textarea class="form-control" name="message" rows="10"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3"></label>
                                <div class="col-md-9 text-left">
                                    <button type="submit" class="btn btn-theme btn-block">Send Message</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- end col-6 -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <!-- end #contact -->
        
        <!-- begin #footer -->
        <div id="footer" class="footer">
            <div class="container">
                <div class="footer-brand">
                    <div class="footer-brand-logo"></div>
                    <i class="fa fa-btc"></i>tcroyals
                </div>
                <p>
                    &copy; Copyright Btcroyals 2017
                </p>
                <p class="social-list">
                    <a href="#"><i class="fa fa-facebook fa-fw"></i></a>
                    <a href="#"><i class="fa fa-instagram fa-fw"></i></a>
                    <a href="#"><i class="fa fa-twitter fa-fw"></i></a>
                </p>
            </div>
        </div>
        <!-- end #footer -->
    </div>
    <!-- end #page-container -->
	
	<!-- ================== BEGIN BASE JS ================== -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/3.0.0/jquery-migrate.min.js" integrity="sha256-JklDYODbg0X+8sPiKkcFURb5z7RvlNMIaE3RA2z97vw=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<script src="{{ asset('color/assets/crossbrowserjs/excanvas.min.js') }}"></script>
	<![endif]-->
	<script src="{{ asset('color/assets/plugins/jquery-cookie/jquery.cookie.js') }}"></script>
	<script src="{{ asset('color/assets/plugins/scrollMonitor/scrollMonitor.js') }}"></script>
	<script src="{{ asset('color/assets/js/apps.min.js') }}"></script>

	<script>
		$(document).ready(function(){
			App.init();
		});
	</script>
    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/5978bd995dfc8255d623f134/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
        })();
    </script>
    <!--End of Tawk.to Script-->

</body>
</html>
