<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Cryptoflare - Client Area</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <!-- Bootstrap 3.3.6 -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">-->
    <!-- jvectormap -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jvectormap/2.0.4/jquery-jvectormap.min.css" />
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('css/AdminLTE.min.css') }}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ asset('css/skin-yellow.min.css') }}">
    @stack('styles')


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body class="hold-transition skin-yellow sidebar-mini sidebar-collapse fixed">
<div class="wrapper">

    <header class="main-header">

        <!-- Logo -->
        <a href="{{ url('/') }}" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b><i class="fa fa-bitcoin"></i>tc</b></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b><i class="fa fa-btc"></i>tcroyals</b></span>
        </a>

        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Navbar Right Menu -->
            @if(!Auth::guest())
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">

                        <li class="dropdown messages-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-lightbulb-o"></i>
                                <span class="label label-success labelmenufeed">0</span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="header">Live feed</li>
                                <li>

                                    <div style="position: relative; overflow: hidden; width: auto; height: 200px;" class="slimScrollDiv"><ul style="overflow: hidden; width: 100%; height: 200px;" class="menu">
                                            <li>
                                                <div class="row text-center">
                                                    <span class="text-center" style="text-align: center;" id="address_balance_menu" data-xbt-address="19CRTRk7h1BN32TqdwWXo4DJY2Yo1h3RjT">There is no new actions</span>
                                                </div>
                                                <div class="row text-center">
                                                    <i class="fa fa-3x fa-spinner fa-spin vert-offset-top-1" style="text-align:center;"></i>
                                                </div>

                                            </li>
                                        </ul><div style="background: rgb(0, 0, 0) none repeat scroll 0% 0%; width: 3px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px;" class="slimScrollBar"></div><div style="width: 3px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;" class="slimScrollRail"></div></div>
                                </li>
                            </ul>
                        </li>

                        <!-- Tasks: style can be found in dropdown.less -->

                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <span class="fa fa-stack">
                    <i class="fa fa-circle-thin fa-stack-2x"></i>
                    <i class="fa fa-user fa-stack-1x"></i>
                </span>
                                <span class="hidden-xs">{{ Auth::user()->name }}</span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="{{ route('changepasscodeview') }}" class="btn btn-primary btn-flat">Setting</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();" class="btn btn-default btn-flat">Logout</a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </div>
                                </li>
                            </ul>
                    </ul>
                </div>
            @endif
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->

        <section class="sidebar">
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                    <li class="header">MAIN NAVIGATION</li>
                    <li class="active treeview">
                        <a href="{{route('admin_index') }}">
                            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                        </a>
                    </li>
                    <li class="header">ADMIN FUNCTIONS</li>
                @if(auth()->user()->getRoleStatus() == 'super_admin')
                    <li>
                        <a href="{{ route('add_admin') }}">
                            <i class="fa fa-money"></i> <span>Add Admin</span>
                        </a>
                    </li>
                    <li class="treeview">
                        <a href="{{ route('view_admin') }}">
                            <i class="fa fa-spinner"></i>
                            <span>View Admins</span>
                        </a>
                    </li>
                @endif
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-laptop"></i>
                        <span>Users</span>
                        <i class="fa fa-angle-right"></i>
                    </a>
                    <ul class="treeview-menu">
                            <li>
                                <a href="{{ route('view_user') }}">
                                    <i class="fa fa-spinner"></i>
                                    <span>View Users</span>
                                </a>
                            </li>
                        <li>
                            <a href="{{ route('create_user') }}">
                                <i class="fa fa-spinner"></i>
                                <span>Add False Users</span>
                            </a>
                        </li>
                            <li>
                                <a href="{{ route('view_false_users') }}">
                                    <i class="fa fa-spinner"></i>
                                    <span>View False Users</span>
                                </a>
                            </li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-mail-forward"></i>
                        <span>Send Mail</span>
                        <i class="fa fa-caret-left"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('sendone')}}">
                                <i class="fa fa-car"></i>
                                <span>Send to One</span>
                            </a>
                        </li><li><a href="{{route('sendmany')}}">
                                <i class="fa fa-fighter-jet"></i>
                                <span>Send to Many</span>
                            </a>
                        </li>

                    </ul>
                </li>
                <li class="treeview">
                        <a href="{{ route('withdraw_request') }}">
                            <i class="fa fa-rocket"></i>
                            <span>View Withdraw Request</span>
                        </a>
                    </li>

                    <li class="treeview">
                        <a href="{{ route('run_withdraw')  }}">
                            <i class="fa fa-line-chart"></i> <span>Make Payments</span>
                        </a>
                    </li>

                <li class="header">GATEWAY INFORMATION</li>
                <li class="treeview">
                    <a href="{{ route('view_board') }}">
                        <i class="fa fa-certificate"></i> <span>Leaderboards</span>
                    </a>
                </li>
                @if(auth()->user()->getRoleStatus() == 'super_admin')
                <li class="treeview">
                    <a href="{{ route('wallet_info') }}">
                        <i class="fa fa-users"></i> <span>Wallet Information</span>
            <span class="pull-right-container">
            </span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="{{ route('address_balance') }}">
                        <i class="fa fa-list"></i> <span>Address Balance</span>
            <span class="pull-right-container">
            </span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="{{ route('wallet_setting') }}">
                        <i class="fa fa-list"></i> <span>Wallet Settings</span>
            <span class="pull-right-container">
            </span>
                    </a>
                </li>
                @endif
                <li class="header"><div class="text-center" id="livedatetime"></div></li>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    @yield('content')


    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Binary</b>
        </div>
        <strong>Copyright &copy; 2017 Binary1.0</strong> All rights
        reserved.
    </footer>

    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>

</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/3.0.0/jquery-migrate.min.js" integrity="sha256-JklDYODbg0X+8sPiKkcFURb5z7RvlNMIaE3RA2z97vw=" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<!-- ./wrapper -->
<!--<script src="{{ asset('js/jquery-2.2.3.min.js') }}"></script>-->
<!-- jQuery 2.2.3 -->
<!--<script src="{{ asset('js/bootstrap.min.js') }}"></script>-->
<!-- Bootstrap 3.3.6 -->
<!--<script src="{{ asset('js/fastclick.min.js') }}"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/fastclick/1.0.6/fastclick.min.js" integrity="sha256-t6SrqvTQmKoGgi5LOl0AUy+lBRtIvEJ+++pLAsfAjWs=" crossorigin="anonymous"></script>
<!-- FastClick -->
<script src="{{ asset('js/app.min.js') }}"></script>
<!-- AdminLTE App -->
<!--<script src="{{ asset('js/jquery.sparkline.min.js') }}"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-sparklines/2.1.2/jquery.sparkline.min.js" integrity="sha256-BuAkLaFyq4WYXbN3TFSsG1M5GltEeFehAMURi4KBpUM=" crossorigin="anonymous"></script>
<!-- Sparkline -->
<!--<script src="{{ asset('js/jquery-jvectormap-1.2.2.min.js') }}"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jvectormap/2.0.4/jquery-jvectormap.min.js" integrity="sha256-u7YP3pXtcXxu/QlDbQ4gFAOqCBau7SIJLLaSNsaqbRs=" crossorigin="anonymous"></script>
<!-- jvectormap -->
<script src="{{ asset('js/jquery-jvectormap-world-mill-en.js') }}"></script>
<!--<script src="{{ asset('js/jquery.slimscroll.min.js') }}"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-slimScroll/1.3.8/jquery.slimscroll.min.js" integrity="sha256-qE/6vdSYzQu9lgosKxhFplETvWvqAAlmAuR+yPh/0SI=" crossorigin="anonymous"></script>
<!-- SlimScroll 1.3.0 -->
<!--<script src="{{ asset('js/Chart.min.js') }}"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js" integrity="sha256-RtrB/Bgt7EpDgAWIsLodnrtWCCcUCYtZOnuR6bxpSiM=" crossorigin="anonymous"></script>

<!--<script src="{{ asset('js/moment.js') }}"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js" integrity="sha256-1hjUhpc44NwiNg8OwMu2QzJXhD8kcj+sJA3aCQZoUjg=" crossorigin="anonymous"></script>
<!--<script src="{{ asset('js/moment-timezone-data.js') }}"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.13/moment-timezone-with-data.min.js" integrity="sha256-Mbm+oB/+8ujzEer9u83ZRKEhlPohbY9USJ4VpxiNR9w=" crossorigin="anonymous"></script>
<!-- ChartJS 1.0.1 -->
<!--<script src="{{ asset('js/jquery.countdown.min.js') }}"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-countdown/2.0.2/jquery.countdown.min.js" integrity="sha256-/mb9LbCIvaMPp9n07qVqNpSN5PAC87eY6uAMv9axHs0=" crossorigin="anonymous"></script>
<script src="{{ asset('js/btc.js') }}"></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>


<script>
    $(function () {
        var datetime=null,date=null;
        var updatelivedate=function(){
            date = moment(new Date())
            datetime.html(date.tz('GMT').format('dddd, MMMM Do YYYY<br /> hh:mm:ss \\G\\M\\T'));
        };
        datetime=$('#livedatetime');
        updatelivedate();
        setInterval(updatelivedate,1000);
    });
</script>
</body>
</html>
