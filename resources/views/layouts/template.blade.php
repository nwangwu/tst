<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Cryptoflare</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <link rel="shortcut icon" href="{{ asset('color/assets/img/favicon.ico') }}" type="image/x-icon">
    <link rel="icon" href="{{ asset('color/assets/img/favicon.ico') }}" type="image/x-icon">
    <!-- Bootstrap 3.3.6 -->
    <!--<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">-->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Font Awesome -->
    <!--<link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">-->
    <!-- jvectormap -->
    <!--<link rel="stylesheet" href="{{ asset('css/jquery-jvectormap-1.2.2.css') }}">-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jvectormap/2.0.4/jquery-jvectormap.min.css" integrity="sha256-sQoGnt24NPHEjR6SdRxBEeM8vq+ARYdTJCxm/XuQUvA=" crossorigin="anonymous" />
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('css/AdminLTE.min.css') }}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ asset('css/skin-purple.min.css') }}">
    @stack('styles')


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body class="hold-transition skin-purple sidebar-mini sidebar-collapse fixed">
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-104499147-1', 'auto');
    ga('send', 'pageview');

</script>
<div class="wrapper">

    <header class="main-header">

        <!-- Logo -->
        <a href="{{ url('/') }}" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>CFL</b></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>CRYPTOFLARE</b></span>
        </a>

        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Navbar Right Menu -->
            @if(!Auth::guest())
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">

                  <li class="dropdown messages-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-lightbulb-o"></i>
                        <span class="label label-success labelmenufeed">0</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">Deposit feed</li>
                        <li>

                            <div style="position: relative; overflow: hidden; width: auto; height: 200px;" class="slimScrollDiv"><ul style="overflow: hidden; width: 100%; height: 200px;" class="menu">
                                    <li>
                                        <div class="row text-center">
                                            <div id="gateway_deposit" data-gateway="{{ auth()->user()->account->gateway_deposit }}" style="display: none;"></div>
                                            <span class="text-center" style="text-align: center;" id="address_balance_menu" data-xbt-address="{{ auth()->user()->bit_deposit_address }}">There are no new actions</span>
                                        </div>
                                        <div class="row text-center">
                                            <i class="fa fa-3x fa-spinner fa-spin vert-offset-top-1" style="text-align:center;"></i>
                                        </div>

                                    </li>
                                </ul><div style="background: rgb(0, 0, 0) none repeat scroll 0% 0%; width: 3px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px;" class="slimScrollBar"></div><div style="width: 3px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51) none repeat scroll 0% 0%; opacity: 0.2; z-index: 90; right: 1px;" class="slimScrollRail"></div></div>
                        </li>
                    </ul>
                    </li>

                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <span class="fa fa-stack">
                    <i class="fa fa-circle-thin fa-stack-2x"></i>
                    <i class="fa fa-user fa-stack-1x"></i>
                </span>
                            <span class="hidden-xs">{{ Auth::user()->name }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="{{ route('setting.index') }}" class="btn btn-primary" style="background-color: #8dacfa;">Setting</a>
                                </div>
                                <div class="pull-right">
                                    <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();" class="btn btn-default btn-flat">Logout</a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </div>
                            </li>
                        </ul>
                </ul>
            </div>
            @endif
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->

        <section class="sidebar">
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                @if(Auth::guest())
                <li class="header">ACCOUNT</li>
                <li class="active treeview">
                    <a href="{{ route('login') }}">
                        <i class="fa fa-plus-square"></i> <span>Login</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="{{ route('register') }}">
                        <i class="fa fa-user-plus"></i>
                        <span>Register</span>
                    </a>
                </li>
                @else
                <li class="header">MAIN NAVIGATION</li>
                <li class="active treeview">
                    <a href="{{route('home') }}">
                        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                    </a>
                </li>
                <li class="header">FINACES</li>
                <li class="treeview">
                    <a href="{{ route('deposit') }}">
                        <i class="fa fa-bitcoin"></i>
                        <span>Deposit</span>
            <span class="pull-right-container">
            <span class="pull-right-container">
                @if (isset($active_deposit))
                @if ($active_deposit <= 0.0)
                <small class="label pull-right bg-red" data-toggle="tooltip"
                       data-placement="right" title=""
                       data-original-title="You don't have any deposit">!</small>
            </span>
                @endif
                @endif
            </span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('withdraw') }}">
                        <i class="fa fa-money"></i> <span>Withdraw</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="{{ route('reinvest') }}">
                        <i class="fa fa-spinner"></i>
                        <span>Reinvest</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="{{ route('withdrawdeposit') }}">
                        <i class="fa fa-rocket"></i>
                        <span>Withdraw Deposit</span>
                    </a>
                </li>

                <li class="treeview">
                    <a href="{{ route('history')  }}">
                        <i class="fa fa-line-chart"></i> <span>Finance History</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="{{ route('automation') }}">
                        <i class="fa fa-gears"></i> <span>Automation</span>
                    </a>
                </li>

                @endif
                <li class="header">INFORMATIONS</li>
                <li class="treeview">
                    <a href="{{ route('leaderboard') }}">
                        <i class="fa fa-certificate"></i> <span>Latest Transactions</span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="{{ route('affiliate') }}">
                        <i class="fa fa-users"></i> <span>Affiliate Program</span>
            <span class="pull-right-container">
            </span>
                    </a>
                </li>
                {{--<li class="treeview">--}}
                    {{--<a href="{{ route('faq') }}">--}}
                        {{--<i class="fa fa-list"></i> <span>About Us</span>--}}
            {{--<span class="pull-right-container">--}}
            {{--</span>--}}
                    {{--</a>--}}
                {{--</li>--}}
                <li class="treeview">
                    <a href="{{ route('contactus') }}">
                        <i class="fa fa-envelope-o"></i> <span>Contact Us</span>
            <span class="pull-right-container">
            </span>
                    </a>
                </li>
                <li class="header"><div class="text-center" id="livedatetime"></div></li>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    @yield('content')


    <footer class="main-footer">
        <strong>Copyright &copy; 2017 Binary1.0</strong> All rights
        reserved.
    </footer>
    <!--<div class="control-sidebar-bg"></div>-->

</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/3.0.0/jquery-migrate.min.js" integrity="sha256-JklDYODbg0X+8sPiKkcFURb5z7RvlNMIaE3RA2z97vw=" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<!-- ./wrapper -->
<!--<script src="{{ asset('js/jquery-2.2.3.min.js') }}"></script>-->
<!-- jQuery 2.2.3 -->
<!--<script src="{{ asset('js/bootstrap.min.js') }}"></script>-->
<!-- Bootstrap 3.3.6 -->
<!--<script src="{{ asset('js/fastclick.min.js') }}"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/fastclick/1.0.6/fastclick.min.js" integrity="sha256-t6SrqvTQmKoGgi5LOl0AUy+lBRtIvEJ+++pLAsfAjWs=" crossorigin="anonymous"></script>
<!-- FastClick -->
<script src="{{ asset('js/app.min.js') }}"></script>
<!-- AdminLTE App -->
<!--<script src="{{ asset('js/jquery.sparkline.min.js') }}"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-sparklines/2.1.2/jquery.sparkline.min.js" integrity="sha256-BuAkLaFyq4WYXbN3TFSsG1M5GltEeFehAMURi4KBpUM=" crossorigin="anonymous"></script>
<!-- Sparkline -->
<!--<script src="{{ asset('js/jquery-jvectormap-1.2.2.min.js') }}"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jvectormap/2.0.4/jquery-jvectormap.min.js" integrity="sha256-u7YP3pXtcXxu/QlDbQ4gFAOqCBau7SIJLLaSNsaqbRs=" crossorigin="anonymous"></script>
<!-- jvectormap -->
<script src="{{ asset('js/jquery-jvectormap-world-mill-en.js') }}"></script>
<!--<script src="{{ asset('js/jquery.slimscroll.min.js') }}"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-slimScroll/1.3.8/jquery.slimscroll.min.js" integrity="sha256-qE/6vdSYzQu9lgosKxhFplETvWvqAAlmAuR+yPh/0SI=" crossorigin="anonymous"></script>
<!-- SlimScroll 1.3.0 -->
<!--<script src="{{ asset('js/Chart.min.js') }}"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js" integrity="sha256-RtrB/Bgt7EpDgAWIsLodnrtWCCcUCYtZOnuR6bxpSiM=" crossorigin="anonymous"></script>

<!--<script src="{{ asset('js/moment.js') }}"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js" integrity="sha256-1hjUhpc44NwiNg8OwMu2QzJXhD8kcj+sJA3aCQZoUjg=" crossorigin="anonymous"></script>
<!--<script src="{{ asset('js/moment-timezone-data.js') }}"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.13/moment-timezone-with-data.min.js" integrity="sha256-Mbm+oB/+8ujzEer9u83ZRKEhlPohbY9USJ4VpxiNR9w=" crossorigin="anonymous"></script>
<!-- ChartJS 1.0.1 -->
<script src="{{ asset('js/jquery.countdown.min.js') }}"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-countdown/2.0.2/jquery.countdown.min.js" integrity="sha256-/mb9LbCIvaMPp9n07qVqNpSN5PAC87eY6uAMv9axHs0=" crossorigin="anonymous"></script>-->
<script src="{{ asset('js/btc.js') }}"></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5978bd995dfc8255d623f134/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>
<!--End of Tawk.to Script-->


<script type="text/javascript">

    Date.prototype.addDays = function (days) {
        this.setDate(this.getDate()+ parseInt(days));
        return this;
    };
    var time_array = [];

    for(i = 0;i<7;i++){
        var currentDate = new Date();
        var bang = currentDate.addDays(i);
        time_array.push(bang.getDate()+"/"+bang.getMonth()+"/"+bang.getFullYear()   );
    }
    var dd = currentDate.addDays(2);
    var aa = currentDate.addDays(1);
    // Get context with jQuery - using jQuery's .get() method.
    var salesChartCanvas = $("#salesChart").get(0).getContext("2d");
    // This will get the first returned node in the jQuery collection.
    var salesChart = new Chart(salesChartCanvas);

    var salesChartData = {
        labels: time_array,//[""+ aa +"", ""+ dd +"", "March", "April", "May", "June", "July"],
        datasets: [

            {
                label: "Bitcoin Invest",
                fillColor: "rgba(60,141,188,0.9)",
                strokeColor: "rgba(60,141,188,0.8)",
                pointColor: "#3b8bba",
                pointStrokeColor: "rgba(60,141,188,1)",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(60,141,188,1)",
                data: [1, 1.3, 1.6, 1.7, 2.0, 2.1, 2.9]
            }
        ]
    };

    var salesChartOptions = {
//Boolean - If we should show the scale at all
        showScale: true,
//Boolean - Whether grid lines are shown across the chart
        scaleShowGridLines: false,
//String - Colour of the grid lines
        scaleGridLineColor: "rgba(0,0,0,.05)",
//Number - Width of the grid lines
        scaleGridLineWidth: 1,
//Boolean - Whether to show horizontal lines (except X axis)
        scaleShowHorizontalLines: true,
//Boolean - Whether to show vertical lines (except Y axis)
        scaleShowVerticalLines: true,
//Boolean - Whether the line is curved between points
        bezierCurve: true,
//Number - Tension of the bezier curve between points
        bezierCurveTension: 0.3,
//Boolean - Whether to show a dot for each point
        pointDot: true,
//Number - Radius of each point dot in pixels
        pointDotRadius: 4,
//Number - Pixel width of point dot stroke
        pointDotStrokeWidth: 1,
//Number - amount extra to add to the radius to cater for hit detection outside the drawn point
        pointHitDetectionRadius: 20,
//Boolean - Whether to show a stroke for datasets
        datasetStroke: true,
//Number - Pixel width of dataset stroke
        datasetStrokeWidth: 2,
//Boolean - Whether to fill the dataset with a color
        datasetFill: false,
//String - A legend template
        legendTemplate: "<ul class=\"<%= name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){ %><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%=datasets[i].label%></li><%}%></ul>",
//Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
        maintainAspectRatio: true,
//Boolean - whether to make the chart responsive to window resizing
        responsive: true
    };

    //Create the line chart
    salesChart.Line(salesChartData, salesChartOptions);
</script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!--<script src="dist/js/pages/dashboard2.js"></script>-->
<!-- AdminLTE for demo purposes -->
<!--<script src="dist/js/demo.js"></script>-->


    <script type="text/javascript">
    $(function () {


        var date = new Date();
        var hour = date.getHours();
        var min = date.getMinutes();
        date.setHours(hour+1);
        date.setMinutes(0);
        date.setSeconds(0);



        $('#countdown_valor').countdown(date)
                .on('update.countdown', function(event) {
                    var $this = $(this);
                    $this.html(event.strftime('%M min : %S sec'));
                }).on('finish.countdown', function(event){
                    var $this = $(this);
                    var date = new Date();
                    var hour = date.getHours();
                    var min = date.getMinutes();
                    date.setHours(hour+1);
                    date.setMinutes(0);
                    date.setSeconds(0);

                    $(this).countdown(date, function(event){
                        $this.html(event.strftime('%M min : %S sec'));
                    });
                });

    });
</script>

<script>
    $(function () {
        var datetime=null,date=null;
        var updatelivedate=function(){
            date = moment(new Date())
            datetime.html(date.tz('GMT').format('dddd, MMMM Do YYYY<br /> hh:mm:ss \\G\\M\\T'));
        };
            datetime=$('#livedatetime');
            updatelivedate();
            setInterval(updatelivedate,1000);
    });
</script>
@if(auth()->check())
<script>
    $(function() {
        $('#address_balance_menu').on('bitstrapped', function(event, data) {
            if(data) {
                var address_balance = data.addresses[0].total_received;
                if(address_balance > 0){
                    address_balance = address_balance/100000000
                }
                var hash = data.txs[0].hash;
                if(address_balance >= {{env("MIN_DEPOSIT")}}) {
                    var conf_num = (data.info.latest_block.height - data.txs[0].block_height + 1);
                    var list = (conf_num > 3) ? ' (' + conf_num + ' Confirmation)' : '( still waiting for 3 confirmations)';
                    $(this).text('Total received: ' + address_balance + ' BTC ' + list);
                    $('.labelmenufeed').text('1');
                }

                if(conf_num > 3){

                    gateway_deposit = $('#gateway_deposit').attr('data-gateway');
                    if((address_balance - gateway_deposit) >= {{env("MIN_DEPOSIT")}}){
                        console.log(address_balance - gateway_deposit);
                        $.ajaxSetup({
                            headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
                        });

                        $.ajax({
                            type:'POST',
                            dataType:'json',
                            data:{'deposit_address': '{{ auth()->user()->bit_deposit_address }}','gateway_deposit':gateway_deposit,'txs_hash': hash},
                            url:'{{ route('deposit_post') }}',
                            success: function (data) {
                                console.log(data);
                            }
                        });
                    }
                }
            }
        });
        $().bitstrap({interval:100000});
    });
</script>
    @endif
</body>
</html>
