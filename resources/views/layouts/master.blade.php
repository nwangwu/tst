<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
<!--<![endif]-->

<head>
    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title>Cryptoflare - Your preferred crypto trading and investment advisory company</title>

    <meta name="author" content="emergoinvest.net">

    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Bootstrap  -->
    <link rel="stylesheet" type="text/css" href="/frontend/stylesheets/bootstrap.css">

    <!-- Theme Style -->
    <link rel="stylesheet" type="text/css" href="/frontend/stylesheets/style.css">

    <!-- Responsive -->
    <link rel="stylesheet" type="text/css" href="/frontend/stylesheets/responsive.css">

    <!-- Colors -->
    <link rel="stylesheet" type="text/css" href="/frontend/stylesheets/colors/color2.css" id="color2">

    <!-- Animation Style -->
    <link rel="stylesheet" type="text/css" href="/frontend/stylesheets/animate.css">

    <!-- Animation headline Style -->
    <link rel="stylesheet" type="text/css" href="/frontend/stylesheets/headline.css">

    <!-- REVOLUTION LAYERS STYLES -->
    <link rel="stylesheet" type="text/css" href="/frontend/revolution/css/layers.css">
    <link rel="stylesheet" type="text/css" href="/frontend/revolution/css/settings.css">

    <!-- Favicon and touch icons  -->
    <link href="/frontend/icon/apple-touch-icon-48-precomposed.png" rel="icon" sizes="48x48">
    <link href="/frontend/icon/apple-touch-icon-32-precomposed.png" rel="icon">
    <link href="/frontend/icon/favicon.png" rel="icon">

    <!--[if lt IE 9]>
    <script src="/frontend/javascript/html5shiv.js"></script>
    <script src="/frontend/javascript/respond.min.js"></script>
    <![endif]-->

    <script src="http://code.tidio.co/yjntlzojdvbxyuf3hzi8p93l4oo4bwv2.js"></script>
</head>

<body class="header_sticky">
<!-- Preloader -->
{{--<div id="loading-overlay">--}}
    {{--<div class="loader"></div>--}}
{{--</div>--}}

<!-- Boxed -->
<div class="boxed">
    <div class="flat-header-wrap style2">
        <div class="top style3 background-trans padding-none">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7">
                        <div class="wrap-top margin-top9 clearfix">
                            <ul class="flat-information style2">
                                <li><i class="fa fa-envelope"></i><a href="#">info@cryptoflare.io</a></li>
                                <li><i class="fa fa-phone"></i><a href="#">+1 631-637-7717</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- /.col-lg-7 -->
                    <div class="col-lg-5">
                        <div class="wrap-top reponsive-none clearfix">
                            <ul class="social-links color-white float-right margin-left25 margin-top14">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li class="google"><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            </ul>
                            <div class="float-right flat-language margin-top12 color-white">
                                <ul class="unstyled">
                                    <li class="current"><a href="#">English</a>
                                        <ul class="unstyled">
                                            <li class="en"><a href="#">French</a></li>
                                            <li class="ge"><a href="#">German</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /.col-lg-5 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /.top -->
        <!-- Header -->
        <header id="header" class="header header-style3 header-classic">
            <div class="container">
                <div class="background-white">
                    <div class="row">
                        <div class="col-lg-3">
                            <div id="logo" class="logo">
                                <a href="/" rel="home">
                                    <img src="/frontend/images/logo.png" alt="image">
                                </a>
                            </div>
                            <!-- /.logo -->
                        </div>
                        <div class="col-lg-9">
                            <div class="flat-wrap-header">
                                <div class="nav-wrap clearfix">
                                    <nav id="mainnav" class="mainnav style2 color-93a float-left">
                                        <ul class="menu">
                                            <li><a href="/test_about">About Us</a>
                                                <ul class="submenu">
                                                    <li><a href="">Trading Methodologies</a></li>
                                                    <li><a href="">Global Leading Brokers</a></li>
                                                    <li><a href="">Funds Management</a></li>
                                                    <li><a href="">Safety of funds</a></li>
                                                </ul>
                                                <!-- /.submenu -->
                                            </li>
                                            <li><a href="/get_started">Getting Started</a></li>
                                            <li><a href="/get_started">Terms & Conditions</a></li>

                                            <li><a href="#">Support</a>
                                                <ul class="submenu right">
                                                    <li><a href="/contact_us">Contact Us</a></li>
                                                    <li><a href="/faq">FAQs</a></li>
                                                </ul>
                                                <!-- /.submenu -->
                                            </li>
                                        </ul>
                                        <!-- /.menu -->
                                    </nav>
                                    <!-- /.mainnav -->
                                    <aside class="widget float-right">
                                        <div class="btn-click style2">
                                            <a href="/login" class="flat-button">ACCOUNT AREA</a>
                                        </div>
                                    </aside>
                                </div>
                                <!-- /.nav-wrap -->
                                <div class="btn-menu">
                                    <span></span>
                                </div>
                                <!-- //mobile menu button -->
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </header>
    </div>

@yield('content')

<!-- Footer -->
    <footer class="footer widget-footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-sm-6 reponsive-mb30">
                    <div class="widget-logo">
                        <div id="logo-footer" class="logo">
                            <a href="/" rel="home">
                                <img src="/frontend/images/logo.png" alt="image">
                            </a>
                        </div>
                        <p>Cryptoflare Ltd. is a fully-registered crypto trading and investment advisory company.</p>
                        <ul class="flat-information">
                            <li><i class="fa fa-map-marker"></i><a href="#">450 Lexington Avenue, New York, NY 10017</a></li>
                            <li><i class="fa fa-phone"></i><a href="#">+1 631-637-7717</a></li>
                            <li><i class="fa fa-envelope"></i><a href="#">info@cryptoflare.io</a></li>
                        </ul>
                    </div>
                </div>
                <!-- /.col-md-3 -->

                <div class="col-lg-3 col-sm-6 reponsive-mb30">
                    <div class="widget widget-out-link clearfix">
                        <h5 class="widget-title">Our Links</h5>
                        <ul class="one-half">
                            <li><a href="#">Forex &amp; CFD</a>
                            </li>
                            <li><a href="#">Cryptocurrency</a>
                            </li>
                            <li><a href="#">Risk &amp; Rewards</a>
                            </li>
                        </ul>
                        <ul class="one-half">
                            <li><a href="#">Trading Methodologies</a></li>
                            <li><a href="#">Safety of funds</a></li>
                        </ul>
                    </div>
                </div>
                <!-- /.col-md-3 -->

                <div class="col-lg-3 col-sm-6 reponsive-mb30">
                    <div class="widget widget-recent-new">
                        <h5 class="widget-title">Recent News</h5>
                        <ul class="popular-new">
                            <li>
                                <div class="text">
                                    <h6><a href="http://bit.do/ecTNx">US Dollar Catches a Bid Ahead of Non-Farm Payrolls (NFP)</a></h6>
                                    <span>5 April 2018</span>
                                </div>
                            </li>
                            <li>
                                <div class="text">
                                    <h6><a href="http://bit.do/ecTN8">Crude Oil, Gold Prices Positioned for Weakness Into Holiday Lull</a></h6>
                                    <span>30 March 2018</span>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- /.col-md-3 -->

                <div class="col-lg-3 col-sm-6 reponsive-mb30">
                    <div class="widget widget-letter">
                        <h5 class="widget-title">Newsletter</h5>
                        <p class="info-text">Subscribe our newsletter to get noti-fication about new offers, etc.</p>
                        <form id="subscribe-form" class="flat-mailchimp" method="post" action="#" data-mailchimp="true">
                            <div class="field clearfix" id="subscribe-content">
                                <p class="wrap-input-email">
                                    <input type="text" tabindex="2" id="subscribe-email" name="subscribe-email" placeholder="Enter Your Email">
                                </p>
                                <p class="wrap-btn">
                                    <button type="button" id="subscribe-button" class="flat-button subscribe-submit" title="Subscribe now">SUBSCRIBE</button>
                                </p>
                            </div>
                            <div id="subscribe-msg"></div>
                        </form>
                    </div>
                </div>
                <!-- /.col-md-3 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </footer>

    <!-- Bottom -->
    <div class="bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="copyright">
                        <p>Copyright &copy; 2017. All rights reserved. Cryptoflare Ltd<a href="#"> Legal</a>
                        </p>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <ul class="social-links style2 text-right">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <!-- <li><a href="#"><i class="fa fa-vimeo"></i></a></li> -->
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fa fa-skype"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- /.container -->
    </div>
    <!-- bottom -->

    <!-- Go Top -->
    <a class="go-top">
        <i class="fa fa-angle-up"></i>
    </a>

</div>

<!-- Javascript -->
<script src="/frontend/javascript/jquery.min.js"></script>
<script src="/frontend/javascript/tether.min.js"></script>
<script src="/frontend/javascript/bootstrap.min.js"></script>
<script src="/frontend/javascript/jquery.easing.js"></script>
<script src="/frontend/javascript/jquery-waypoints.js"></script>
<script src="/frontend/javascript/jquery-validate.js"></script>
<script src="/frontend/javascript/jquery.cookie.js"></script>
<script src="/frontend/javascript/owl.carousel.js"></script>
<script src="/frontend/javascript/jquery-countTo.js"></script>
<script src="/frontend/javascript/jquery-validate.js"></script>
<script src="/frontend/javascript/jquery.flexslider-min.js"></script>
<script src="/frontend/javascript/modern.custom.js"></script>
<script src="/frontend/javascript/jquery.hoverdir.js"></script>

<script src="/frontend/javascript/headline.js"></script>
<script src="/frontend/javascript/parallax.js"></script>
<script src="/frontend/javascript/main.js"></script>

<!-- Revolution Slider -->
<script src="/frontend/revolution/js/jquery.themepunch.tools.min.js"></script>
<script src="/frontend/revolution/js/jquery.themepunch.revolution.min.js"></script>
<script src="/frontend/revolution/js/slider3.js"></script>

<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
<script src="/frontend/revolution/js/extensions/revolution.extension.actions.min.js"></script>
<script src="/frontend/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
<script src="/frontend/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
<script src="/frontend/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script src="/frontend/revolution/js/extensions/revolution.extension.migration.min.js"></script>
<script src="/frontend/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
<script src="/frontend/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
<script src="/frontend/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
</body>

</html>