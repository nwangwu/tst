<!DOCTYPE html>
<html>
<head>
    <style type="text/css">
        .page-error{display:-ms-flexbox;display:flex;-ms-flex-align:center;align-items:center;-ms-flex-pack:center;justify-content:center;-ms-flex-direction:column;flex-direction:column;min-height:100vh}.page-error h1{margin:10px;color:#F44336;font-size:42px}
        .fa {
            display: inline-block;
            font: normal normal normal 14px/1 FontAwesome;
            font-size: inherit;
            text-rendering: auto;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
        }
        * {
            box-sizing: border-box;
        }
        h1 {
            display: block;
            font-size: 2em;
            -webkit-margin-before: 0.67em;
            -webkit-margin-after: 0.67em;
            -webkit-margin-start: 0px;
            -webkit-margin-end: 0px;
            font-weight: bold;
        }

        body {
            font-family: "Lato","Segoe UI",sans-serif;
            font-size: 14px;
            line-height: 1.428571429;
            color: #333;
            background-color: #e5e5e5;
        }
    </style>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSS-->
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <title>bTCRoyals</title>

</head>
<body>
<div class="page-error">
    <h1> Error 404</h1>
    <p>The page you have requested is not found.</p>
    <p><a href="javascript:window.history.back();">Go back to previous page</a></p>
</div>
</body>
</html>