@extends('layouts.master')


@section('content')
    <div id="rev_slider_1078_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container slide-overlay" data-alias="classic4export" data-source="gallery" style="margin:0px auto;background-color:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">

        <!-- START REVOLUTION SLIDER 5.3.0.2 auto mode -->
        <div id="rev_slider_1078_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.3.0.2">
            <div class="slotholder"></div>

            <ul>
                <!-- SLIDE  -->

                <!-- SLIDE 1 -->
                <li data-index="rs-3050" data-transition="fade" data-slotamount="7" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000" data-rotate="0" data-saveperformance="off" data-title="Ken Burns"
                    data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- <div class="overlay">
                    </div> -->
                    <!-- MAIN IMAGE -->
                    <img src="/frontend/images/slides/3.jpg" alt="" data-bgposition="center center" data-kenburns="off" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0"
                         data-bgparallax="10" class="rev-slidebg" data-no-retina>

                    <div class="section-overlay style2"></div>
                    <!-- LAYERS -->
                    <!-- LAYER NR. 12 -->
                    <div class="tp-caption title-slide color-white" id="slide-3050-layer-1" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['-45','-45','-45','-15']" data-fontsize="['80','80','50','33']"
                         data-lineheight="['95','95','55','35']" data-fontweight="['700','700','700','700']" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"delay":100,"speed":3000,"frame":"0","from":"x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:1;","mask":"x:[100%];y:0;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['center','center','center','center']" data-paddingtop="[10,10,10,10]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0" data-paddingleft="[0,0,0,0]" style="z-index: 16; white-space: nowrap;">Cryptoflare
                    </div>

                    <!-- LAYER NR. 13 -->
                    <div class="tp-caption sub-title color-white" id="slide-3050-layer-4" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['29','29','29,'-15']" data-fontsize="['18',18','18','16']"
                         data-lineheight="['30','30','22','16']" data-fontweight="['400','400','400','400']" data-width="['1000',1000','800','450']" data-height="none" data-whitespace="['nowrap',normal','normal','normal']" data-type="text" data-responsive_offset="on"
                         data-frames='[{"delay":1100,"speed":3000,"frame":"0","from":"x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:1;","mask":"x:[100%];y:0;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                         data-textAlign="['center','center','center','center']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 17; white-space: normal;">We are pleased to welcome all moneymakers of cryptocurrency on the website of Cryptoflare.
                    </div>

                    <a href="/register" target="_self" class="tp-caption flat-button color-white text-center" data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","speed":2000,"to":"o:1;","delay":2000,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                       data-x="['center','center','center','center']" data-hoffset="['-90','-90','-90','0']" data-y="['middle','middle','middle','middle']" data-voffset="['119','119','100','100']" data-fontweight="['700','700','700','700']" data-width="['auto']"
                       data-height="['auto']" style="z-index: 3;">Sign Up
                    </a>
                    <!-- END LAYER LINK -->

                    <a href="/login" target="_self" class="tp-caption flat-button style3 text-center" data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","speed":2000,"to":"o:1;","delay":2000,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                       data-x="['center','center','center','center']" data-hoffset="['93','93','93','0']" data-y="['middle','middle','middle','middle']" data-voffset="['119','119','100','170']" data-fontweight="['700','700','700','700']" data-width="['auto']"
                       data-height="['auto']" style="z-index: 3;">Log In
                    </a>
                    <!-- END LAYER LINK -->
                </li>
            </ul>
        </div>
    </div>
    <!-- END REVOLUTION SLIDER -->

    <section>
        <div class="container">
            <ul id="data-effect" class="data-effect wrap-iconbox margin-top_81 clearfix">
                <li>
                    <div class="iconbox effect bg-image text-center">
                        <div class="box-header">
                            <div class="box-icon">
                                <i class="icon_globe"></i>
                            </div>
                        </div>
                        <div class="box-content">
                            <h5 class="box-title">Smart Trading</h5>
                            <p>We use a dynamic portfolio of top performing strategies to seamlessly trade your capital</p>
                        </div>
                        <div class="effecthover">
                            <img src="/frontend/images/imagebox/1.jpg" alt="image">
                        </div>
                    </div>
                </li>
                <li>
                    <div class="iconbox effect bg-image text-center">
                        <div class="box-header">
                            <div class="box-icon">
                                <i class="icon_search-2"></i>
                            </div>
                        </div>
                        <div class="box-content">
                            <h5 class="box-title">Expert Advice</h5>
                            <p>We guide you so that you understand and are prepared for the decisions that lie ahead</p>
                        </div>
                        <div class="effecthover">
                            <img src="/frontend/images/imagebox/1.jpg" alt="image">
                        </div>
                    </div>
                </li>
                <li>
                    <div class="iconbox effect bg-image text-center">
                        <div class="box-header">
                            <div class="box-icon">
                                <i class="icon_cogs"></i>
                            </div>
                        </div>
                        <div class="box-content">
                            <h5 class="box-title">Confidentiality</h5>
                            <p>We keep all client information and data confidential and secure through a robust infrastructure</p>
                        </div>
                        <div class="effecthover">
                            <img src="/frontend/images/imagebox/1.jpg" alt="image">
                        </div>
                    </div>
                </li>
            </ul>
            <div class="divider sh26"></div>
            <!-- TradingView Widget BEGIN -->
            <div class="tradingview-widget-container">
                <div class="tradingview-widget-container__widget"></div>
                {{--<script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-tickers.js" async>--}}
                    {{--{--}}
                        {{--"symbols": [{--}}
                        {{--"title": "S&P 500",--}}
                        {{--"proName": "INDEX:SPX"--}}
                    {{--}, {--}}
                        {{--"title": "Nasdaq 100",--}}
                        {{--"proName": "INDEX:IUXX"--}}
                    {{--}, {--}}
                        {{--"title": "EUR/USD",--}}
                        {{--"proName": "FX_IDC:EURUSD"--}}
                    {{--}, {--}}
                        {{--"title": "Crude Oil",--}}
                        {{--"proName": "NYMEX:CL1!"--}}
                    {{--}, {--}}
                        {{--"title": "Gold",--}}
                        {{--"proName": "FX_IDC:XAUUSD"--}}
                    {{--}],--}}
                        {{--"locale": "en"--}}
                    {{--}--}}
                {{--</script>--}}
            </div>
            <!-- TradingView Widget END -->
            <div class="row">
                <div class="col-lg-4 flat-icon-left">
                    <div class="divider sh35"></div>
                    <div class="iconbox iconleft">
                        <div class="box-header">
                            <div class="box-icon">
                                <i class="icon_currency"></i>
                            </div>
                        </div>
                        <div class="box-content">
                            <h5 class="box-title">Fast & Secure Withdrawals</h5>
                            <p>Withdrawal requests process instantly without any fee. You can make as many requests as you want everyday and without a minimum limits.</p>
                        </div>
                    </div>
                    <div class="divider sh5"></div>
                    <div class="iconbox iconleft">
                        <div class="box-header">
                            <div class="box-icon">
                                <i class="icon_building"></i>
                            </div>
                        </div>
                        <div class="box-content">
                            <h5 class="box-title">Hourly Sales Payment</h5>
                            <p>Your hourly profit will be added to your account balance automatically every 1 hours for lifetime and can be withdrawn at any time.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="featured-iconbox">
                        <img src="/frontend/images/imagebox/f1.jpg" alt="image">
                    </div>
                </div>
                <div class="col-lg-4 flat-icon-right">
                    <div class="divider sh35"></div>
                    <div class="iconbox iconleft">
                        <div class="box-header">
                            <div class="box-icon">
                                <i class="icon_piechart"></i>
                            </div>
                        </div>
                        <div class="box-content">
                            <h5 class="box-title">User Friendly Interface</h5>
                            <p>Whether you are a beginner or a professional in the online investment field, we are sure that you will find our platform easy to use.</p>
                        </div>
                    </div>
                    <div class="divider sh5"></div>
                    <div class="iconbox iconleft">
                        <div class="box-header">
                            <div class="box-icon">
                                <i class="icon_creditcard"></i>
                            </div>
                        </div>
                        <div class="box-content">
                            <h5 class="box-title">Seamless Service</h5>
                            <p>All deposits and withdrawals are made seamless by our secure Crpto API</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-maps-form style2 parallax parallax4">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <div class="padding-lr65_5">
                        <article class="pricing-item text-center wrap-iconbox">
                            <div class="header-pricing">
                                <h6>Elixir Premium</h6>
                                <div class="flat-value">

                                    <span class="price-number">120%</span>
                                </div>
                            </div>
                            <ul class="pricing-content">
                                <li style="width: 100%">Investor type: All</li>
                                <li style="width: 100%">Contract (TF) : 1 day</li>
                                <li style="width: 100%">Short Term</li>
                                <li style="width: 100%">EST ROI: 120%</li>
                            </ul>
                            <div class="btn-pricing">
                                <a href="/register" class="flat-button style2">GET STARTED</a>
                            </div>
                        </article>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="padding-lr35">
                        <article class="pricing-item active text-center wrap-iconbox">
                            <div class="header-pricing">
                                <h6>Elixir Super</h6>
                                <div class="flat-value">
                                    <span class="price-number">140%</span>
                                </div>
                            </div>
                            <ul class="pricing-content">
                                <li style="width: 100%">Investor type: All</li>
                                <li style="width: 100%">Contract (TF) : 3 Days</li>
                                <li style="width: 100%">Short Term</li>
                                <li style="width: 100%">EST ROI: 140%%</li>
                            </ul>
                            <div class="btn-pricing">
                                <a href="/register" class="flat-button style2">GET STARTED</a>
                            </div>
                        </article>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="padding-lr5_65">
                        <article class="pricing-item text-center wrap-iconbox">
                            <div class="header-pricing">
                                <h6>Elixir Ultimate</h6>
                                <div class="flat-value">
                                    <span class="price-number">200%</span>
                                </div>
                            </div>
                            <ul class="pricing-content">
                                <li style="width: 100%">Investor type: All</li>
                                <li style="width: 100%">Contract (TF) : 1 Week</li>
                                <li style="width: 100%">Short Term</li>
                                <li style="width: 100%">EST ROI: 200% %</li>
                            </ul>
                            <div class="btn-pricing">
                                <a href="/register" class="flat-button style2">GET STARTED</a>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-maps-form style2 parallax parallax4">
        <div class="section-overlay style2"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="formrequest2 style2 padding-right30">
                        <div class="divider sh94"></div>
                        <div class="title-section style2 titlesize48 color-white">
                            <h1 class="title"><span>Request</span> a call back.</h1>
                            <div class="sub-title">
                                Whatever specific financial, personal or investment opportunities you’re looking for, be sure that the free consultation with one of our experts will sway you to signup!
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="wrap-formrequest padding-lr100">
                        <form id="contactform" class="contactform wrap-form style3 clearfix" method="post" action="./contact/contact-process2.php" novalidate="novalidate">
                            <span class="title-form">Select a time to talk</span>
                            <span class="flat-input flat-select">
                                <select>
                                    <option value="">Weekday Morning</option>
                                    <option value="">Weekday Evening</option>
                                    <option value="">Weekend Morning</option>
                                    <option value="">Weekend Evening</option>
                                </select>
                            </span>
                            <span class="flat-input"><input name="name" type="text" value="" placeholder="Your Name:*" required="required"></span>
                            <span class="flat-input"><input name="phone" type="text" value="" placeholder="Phone Number:*" required="required"></span>
                            <span class="flat-input"><button name="submit" type="submit" class="flat-button" id="submit" title="Submit now">SUBMIT<i class="fa fa-angle-double-right"></i></button></span>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="flat-row section-testimonials2 padding3">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title-section text-center">
                        <div class="symbol">
                            ​‌“
                        </div>
                        <h1 class="cd-headline clip is-full-width">
                            <span>2000+</span>
                            <span class="cd-words-wrapper">
                                <b class="is-visible"> Happy Clients</b>
                                <b>Investors</b>
                                <b>Managed Trades</b>
                            </span>
                        </h1>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="wrap-testimonial padding-lr79">
                        <div id="testimonial-slider">
                            <ul class="slides">
                                <li>
                                    <div class="testimonials style3 text-center">
                                        <div class="message">
                                            <blockquote class="whisper">
                                                "This is what trade advisors should do! I have never had this kind of experience in the past with asset investment advisers and this is the kind of service I have been looking for. I feel like you know the whole picture and if anything happened to me
                                                now, I know my family will be looked after."
                                            </blockquote>
                                        </div>
                                        <div class="avatar">
                                            <span class="name">Sandeep Kulkarni</span><br>
                                            <div class="start">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                            <span class="position">Physiotherapist</span>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="testimonials style3 text-center">
                                        <div class="message">
                                            <blockquote class="whisper">
                                                "I am very satisfied with the advice provided by Mike Wagner. He listened sensitively and quickly assessed the types of risk I was prepared to take. Mike demonstrated that he understood my situation and my needs and advised accordingly."
                                            </blockquote>
                                        </div>
                                        <div class="avatar">
                                            <span class="name">Ashley Jones</span><br>
                                            <div class="start">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <!-- <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i> -->
                                            </div>
                                            <span class="position">Events Planner</span>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="testimonials style3 text-center">
                                        <div class="message">
                                            <blockquote class="whisper">
                                                "Cryptoflare have professionally handled my investments over a long period. Day trading can be a daunting world, but their service has always been friendly, targeted, and clear – somehow they always find a way of demystifying the process.
                                                Highly recommended."
                                            </blockquote>
                                        </div>
                                        <div class="avatar">
                                            <span class="name">Scott Poole</span><br>
                                            <div class="start">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                            <span class="position">CEO DeerCreative</span>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>

                        <div id="testimonial-carousel">
                            <ul class="slides clearfix">
                                <li>
                                    <img alt="image" src="/frontend/images/testimonial/1.jpg">
                                </li>
                                <li>
                                    <img alt="image" src="/frontend/images/testimonial/2.jpg">
                                </li>
                                <li>
                                    <img alt="image" src="/frontend/images/testimonial/3.jpg">
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- /.wrap-testimonial -->
                </div>
            </div>
        </div>
    </section>

    <section class="flat-row background-nopara background-image2 section-counter2">
        <div class="section-overlay style2"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title-section style2 color-white titlesize48">
                        <h1 class="title">Take the right <span>steps</span></h1>
                        <div class="sub-title style2 linehight30">
                             Do the big things
                        </div>
                    </div>
                </div>
                <!-- /.col-md-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-2 col-sm-6">
                    <div class="flat-counter style2 padding-another">
                        <div class="content-counter">
                            <div class="numb-count" data-to="10" data-speed="2000" data-waypoint-active="yes">10</div>
                            <div class="name-count">Days Active</div>
                        </div>
                    </div>
                    <!-- /.flat-counter -->
                </div>
                <div class="col-lg-2 col-sm-6">
                    <div class="flat-counter style2 before">
                        <div class="content-counter">
                            <div class="numb-count" data-to="{{ $registered_users }}" data-speed="2000" data-waypoint-active="yes">{{ $registered_users }}</div>
                            <div class="name-count">Registered Accounts</div>
                        </div>
                    </div>
                    <!-- /.flat-counter -->
                </div>
                <div class="col-lg-2 col-sm-6">
                    <div class="flat-counter style2">
                        <div class="content-counter">
                            <div class="numb-count" data-to="{{ sprintf('%.5f',$total_site_deposit) }}" data-speed="2000" data-waypoint-active="yes">{{ sprintf('%.5f',$total_site_deposit) }}</div>
                            <div class="name-count">Deposits</div>
                        </div>
                    </div>
                    <!-- /.flat-counter -->
                </div>
                <div class="col-lg-2 col-sm-6">
                    <div class="flat-counter style2 before percent">
                        <div class="content-counter">
                            <div class="numb-count" data-to="{{ sprintf('%.5f',$total_site_withdraw ) }}" data-speed="2000" data-waypoint-active="yes">{{ sprintf('%.5f',$total_site_withdraw ) }}</div>
                            <div class="name-count">Withdrawals</div>
                        </div>
                    </div>
                    <!-- /.flat-counter -->
                </div>
            </div>
        </div>
        <!-- /.container -->
    </section>

    <section class="flat-row v11">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title-section text-center">
                        <h1 class="title">Latest News</h1>
                    </div>
                </div>
            </div>
            <div class="blog-carosuel-wrap2">
                <div class="blog-shortcode post-list">
                    <article class="entry clearfix">
                        <div class="entry-border clearfix">
                            <div class="featured-post">
                                <a href="services-details.html"> <img src="/frontend/images/blog/ls1.jpg" alt="image"></a>
                            </div>
                            <!-- /.feature-post -->
                            <div class="content-post">
                                <span class="category">News &amp; Analysis</span>
                                <h2 class="title-post"><a href="https://www.dailyfx.com/forex/fundamental/daily_briefing/session_briefing/daily_fundamentals/2018/04/05/us-dollar-catches-a-bid-ahead-of-non-farm-payrolls-nfp-srepstans.html">US Dollar Catches a Bid Ahead of Non-Farm Payrolls (NFP)</a></h2>
                                <div class="meta-data style2 clearfix">
                                    <ul class="meta-post clearfix">
                                        <li class="day-time">
                                            <span>5 April 2018</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!-- /.contetn-post -->
                        </div>
                        <!-- /.entry-border -->
                    </article>
                    <article class="entry clearfix">
                        <div class="entry-border clearfix">
                            <div class="featured-post">
                                <a href="services-details.html"> <img src="/frontend/images/blog/ls2.jpg" alt="image"></a>
                            </div>
                            <!-- /.feature-post -->
                            <div class="content-post">
                                <span class="category">Commodities</span>
                                <h2 class="title-post"><a href="https://www.dailyfx.com/forex/fundamental/daily_briefing/daily_pieces/commodities/2018/03/30/Crude-Oil-Gold-Prices-Positioned-for-Weakness-Into-Holiday-Lull.html">Crude Oil, Gold Prices Positioned for Weakness</a></h2>
                                <div class="meta-data style2 clearfix">
                                    <ul class="meta-post clearfix">
                                        <li class="day-time">
                                            <span>30 March 2018</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!-- /.contetn-post -->
                        </div>
                        <!-- /.entry-border -->
                    </article>
                </div>
            </div>
        </div>
    </section>

    <section class="flat-row section-client bg-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="flat-client" data-item="5" data-nav="false" data-dots="false" data-auto="false">
                        <div class="client"><img src="/frontend/images/clients/1.png" alt="image"></div>
                        <div class="client"><img src="/frontend/images/clients/2.png" alt="image"></div>
                        <div class="client"><img src="/frontend/images/clients/3.png" alt="image"></div>
                        <div class="client"><img src="/frontend/images/clients/4.png" alt="image"></div>
                        <div class="client"><img src="/frontend/images/clients/5.png" alt="image"></div>
                    </div>
                    <!-- /.flat-client -->
                </div>
            </div>
        </div>
    </section>
@endsection