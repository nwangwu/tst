@extends('layouts.master')

@section('content')
    <section>
        <div>
            <div class="background-holder overlay" style="background-image:url(assets/images/background-2.jpg);background-position: center bottom;"></div>
            <!--/.background-holder-->
            <div class="container">
                <div class="row pt-6" data-inertia='{"weight":1.5}'>
                    <div class="col-md-8 px-md-0 color-white" data-zanim-timeline="{}" data-zanim-trigger="scroll">
                        <div class="overflow-hidden">
                            <h1 class="color-white fs-4 fs-md-5 mb-0 zopacity" data-zanim='{"delay":0}'>Terms & Conditions</h1>
                            <div class="nav zopacity" aria-label="breadcrumb" role="navigation" data-zanim='{"delay":0.1}'>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/.row-->
        </div>
        <!--/.container-->
    </section>
    <section class="background-11">
        <div class="container">

            <div class="row mt-6">
                <div class="col">
                    <h3 class="text-center fs-2 fs-md-3">Please Read Carefully</h3>
                    <hr class="short" data-zanim='{"from":{"opacity":0,"width":0},"to":{"opacity":1,"width":"4.20873rem"},"duration":0.8}' data-zanim-trigger="scroll"/>
                </div>
                <div class="col-12">
                    <div class="background-white px-3 mt-6 px-0 py-5 px-lg-5 radius-secondary">
                        <h5>General</h5>
                        <p class="mt-3">These rules are official and the public offer of bitline.club - Your Profitable Operator, acting in accordance with the legislation of Great Britain, on the one hand, and the individual investor. This is equivalent to the conclusion of the Agreement in accordance with international law.
                            <br><br>
                            These rules shall enter into force on the date of registration of the Investor on the website of the program bitline.club - Your Profitable Operator and his acceptance of the terms and conditions. If you disagree with these terms and conditions or any part of these terms and conditions, you must not use this website.
                            <br><br>
                            Any individual or company from any country may open an account with us. You must be at least 18 years of age to use this website.
                            <br><br>
                            You agree that all information, interactions, materials coming from bitline.club - Your Profitable Operator are unsolicited and must be kept private, confidential and protected from any disclosure.
                            <br><br>
                            Besides, the information, interactions and materials present herein are not to be regarded as an offer, nor a solicitation for investments in any jurisdiction which deems non-public offers or solicitations unlawful, nor to any person to whom it will be unlawful to make such offer or solicitation.
                            <br><br>
                            Each Investor can register only one personal account, re-registration is not allowed. In case of multiple registrations, the Company reserves the right to block all accounts to ascertain the circumstances.
                        </p>
                        <h5>Member Registration</h5>
                        <p class="mt-3">
                            You must register as a Member to access certain functions of the Website. You are obliged to provide only complete and accurate information about yourself (the "Registration Data") when registering as a Member or updating your Registration Data.
                            <br><br>
                            You agree to maintain and keep your Registration Data current and to update the Registration Data as soon as it changes. You are responsible for maintaining the security of your password. Our Company and its service providers are not liable for any loss that you may suffer through the use of your password by others.
                        </p>
                        <h5>Investment Conditions</h5>
                        <p class="mt-3">
                            Each deposit is considered to be a private transaction between bitline.club - Your Profitable Operator and its Member. Members perform all financial transactions solely at their own discretion and their own risk. The Investor personally decides whether or not to invest and how much to invest. All accruals in the Personal Account are made according to the chosen investment package. The Investor has the right to freely dispose of the funds that are on his personal account. The Investor can make a deposit with only help of electronic payment systems used by the Company.
                        </p>
                        <h5>Refund Policy</h5>
                        <p class="mt-3">
                            All operations related to the withdrawal of funds carried out only in accordance with the Company’s Refund Policy.
                            <br><br>
                            The Client can only receive a refund of his investment upon expiration of the investment package.
                            <br><br>
                            You must contact Us to make the right decision before addressing Your complaint to Your payment processor's customer support.
                        </p>
                        <h5>Confidentiality</h5>
                        <p class="mt-3">
                            Company undertakes not disclose any personal information of Investor to third parties.
                            <br><br>
                            We act in accordance with policy of complete confidentiality. No information about you, including personal data, information on operations and revenue to anyone except you are available. Your information may only be used by authorized employees of the Company in case of necessity. The only information that is displayed publicly is current deposit statistics, which includes deposit's date and time, amount, payment method and username of the investor. Real name of the Investor is never shown publicly and is never displayed. Investor is allowed to take any username except for forbidden ones. We guarantee confidentiality of all transactions arising from Your membership with the Company at all times.
                        </p>
                        <h5>Change or Termination</h5>
                        <p class="mt-3">
                            We reserve the right, at our sole discretion and without prior notice, temporarily terminate or suspend the delivery of information through this Website. It can happen when We need to supplement, update, improve or correct the content of this Website. The Website can be shut down temporarily in case of urgent system updates, equipment failures, power cutoffs or natural disasters. The Company does not bear responsibility for direct or indirect losses resulted by such circumstances.
                            <br><br>
                            We may permanently or temporarily terminate or suspend your access to the Site without notice or liability, if in our sole determination you violate any provision of these Terms of Conditions.
                        </p>

                    </div>
                </div>
            </div>
            <!--/.row-->
        </div>
        <!--/.container-->
    </section>
@endsection