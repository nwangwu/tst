@extends('layouts.master')

@section('content')
    <!-- Page title -->
    <div class="page-title parallax parallax1">
        <div class="section-overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-title-heading">
                        <h1 class="title">Contact Us</h1>
                    </div><!-- /.page-title-captions -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.page-title -->
    <section class="flat-row page-contact">
        <div class="container">
            <div class="wrap-infobox">


                <div class="row">
                    <div class="col-lg-4">
                        <div class="info-box text-center">
                            <h3>France</h3>
                            <ul>
                                <li>John Doe, 123 Main St Chicago, IL 60626</li>
                                <li>Email: info@cryptoflare.io</li>
                                <li>Phone: 258-556-189</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="info-box text-center">
                            <h3>United States</h3>
                            <ul>
                                <li>John Doe, 123 Main St Chicago, IL 60626</li>
                                <li>Email: info@cryptoflare.io</li>
                                <li>Phone: 258-556-189</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="info-box text-center">
                            <h3>Viet Nam</h3>
                            <ul>
                                <li>John Doe, 123 Main St Chicago, IL 60626</li>
                                <li>Email: info@cryptoflare.io</li>
                                <li>Phone: 258-556-189</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
@endsection