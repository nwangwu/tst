@extends('layouts.master')

@section('content')
    <!-- Page title -->
    <div class="page-title parallax parallax1">
        <div class="section-overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-title-heading">
                        <h1 class="title"></h1>
                    </div>
                    <!-- /.page-title-captions -->
                    <!-- <div class="breadcrumbs">
                        <ul>
                            <li class="home"><i class="fa fa-home"></i><a href="index.html">Home</a></li>
                            <li>Services Detail</li>
                        </ul>
                    </div> -->
                    <!-- /.breadcrumbs -->
                </div>
                <!-- /.col-md-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </div>
    <!-- /.page-title -->
    <!-- Blog posts -->
    <section class="flat-row project-single">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="single-post">
                        <div class="single-text1">
                            <h2>Open An Account</h2>
                            <p>To become a member of bitline.club - Your Profitable Operator you need first to open a free account. It's quite easy and doesn't take much time, but you will get an opportunity to become an investor and earn profit. For that you must complete the registration process. In order to register yourself as a member of bitline.club - Your Profitable Operator, click on the "Create Account" button, fill in the registration form and press "Register". Your account is ready to use! You are obliged to provide only complete and accurate information about yourself (the "Registration Data") when registering as a Member.</p>

                            <h2>Deposit Funds</h2>

                            <p>To make a deposit, log into your bitline.club - Your Profitable Operator's account using your login and password. Choose "Make Deposit" section, select an investment plan most appropriate for you, decide what amount to invest and what term to choose. Select the required electronic payment system (so-called EPS). Pay amount of minimal or maximal deposit that is stated in it in accordance with chosen investment plan. Follow the instructions of EPS and approve the payment finally. Wait for adding funds on your balance. It can take from a few seconds up to few hours.</p>

                            <h2>Earning of Profit</h2>
                            <p>All accruals of profit are done automatically and in accordance with chosen investment plan. Depending on the amount of your deposit and the term of chosen investment period, you will receive guaranteed income after a certain period of time. Your earnings is depending from your investment plan and can be in Daily basis or at the end of investment term. If you choose Daily plans you will get your initial capital back at the end of investment term. For "After" and "VIP" plans which includes both your principal amount and profits, which means that your principal amount will not be returned separately. To calculate your income please use our Profit Calculator.</p>

                            <h2>Withdraw Funds</h2>
                            <p>When profit is accrued on your account balance, you can withdraw it via electronic payment system. Log into the account, then select the "Withdraw" section. Type an amount, which you want to withdraw, choose payment system and click "Withdraw" button. Now just wait for the money that was requested during the withdrawal at your electronic wallet. Your withdrawal request will be processed in the fastest timeframe possible, as per the online availability of our support staff (up to 30 hours). Please take note that you can withdraw funds only in the same currency as you invested, for example if you invest via PerfectMoney, you will be able to withdraw only via PerfectMoney.</p>

                        </div>
                    </div>
                </div>
                <!-- /.col-md-6 -->
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>
@endsection