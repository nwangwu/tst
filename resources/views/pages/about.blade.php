@extends('layouts.master')

@section('content')
    <!-- Page title -->
    <div class="page-title parallax parallax1">
        <div class="section-overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-title-heading">
                        <h1 class="title"></h1>
                    </div>
                    <!-- /.page-title-captions -->
                    <!-- <div class="breadcrumbs">
                        <ul>
                            <li class="home"><i class="fa fa-home"></i><a href="index.html">Home</a></li>
                            <li>Services Detail</li>
                        </ul>
                    </div> -->
                    <!-- /.breadcrumbs -->
                </div>
                <!-- /.col-md-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </div>
    <!-- /.page-title -->

    <!-- Services Details -->
    <section class="flat-row services-details">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="sidebar">
                        <div class="widget widget-help">
                            <div class="featured-widget">
                                <img src="/frontend/images/services/w1.jpg" alt="image">
                            </div>
                            <h3>Talk to an Expert</h3>
                            <p>Contact us at the Cryptoflare office nearest to you or submit a business inquiry online.</p>
                            <a href="#" class="flat-button style2">CONTACT US</a>
                        </div>

                        <div class="widget widget-download">
                            <div class="download">
                                <a href="#" class="flat-button style2">OPEN AN ACCOUNT</a>
                            </div>
                        </div>


                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="item-wrap">
                        <div class="item item-details">
                            <div class="featured-item">
                                <img src="/frontend/images/services/details.jpg" alt="image">
                            </div>
                            <!-- /.feature-post -->
                            <div class="content-item">
                                <h2 class="title-item">Our History</h2>
                                <p>Emerging from a rich history of investment excellence, Cryptoflare boasts 50 years of combined trading expertise across 100 financial instruments provided across our state-of-the-art platforms.</p>

                                <p>We draw on the 5-year old successful history of Cryptoflare, a multicultural transnational investment group. Over the years we created a vast network of high-level relationships and built the infrastructure to maintain
                                    and expand those relationships. Our international network of experienced professionals enables us to execute almost any investment strategy.</p>

                                <p>Cryptoflare today is an independent broker built on the strong foundations of its predecessor and continuing in the underlying mission of protecting and enhancing client’s wealth. Having gained our experience in the
                                    fields of trading and portfolio management, we always take the conservative approach and ensure that we adhere to even stricter rules and regulations than what our regulator imposes on us.</p>


                            </div>
                            <!-- /.content-item -->
                        </div>
                        <!-- /.item -->
                        <div class="video-services clearfix">


                        </div>
                        <!-- /.video-services -->
                        <div class="flat-tabs" data-effect="fadeIn">
                            <ul class="menu-tab clearfix">
                                <li class="active"><a href="#">Trading Methodologies</a></li>
                                <li><a href="#">Global Leading Brokers</a></li>
                                <li><a href="#">Funds Management</a></li>
                                <li><a href="#">Safety of Funds</a></li>
                            </ul>
                            <!-- /.menu-tab -->
                            <div class="content-tab">
                                <div class="content-inner">
                                    <p>One of the most important factors that every investor should pay attention to without fail while choosing their fund management team is the trading methodology used by the fund managers. Experience of the fund managers
                                        will yield results only when the right trading methodology is employed.</p>
                                    <h3>Risk Management and Strategy</h3>
                                    <p>Every investor knows that Forex trading involves market risks and that there are chances to lose money if the funds are not managed effectively. When you have an experienced fund management service provider such
                                        as Cryptoflare you will be able to minimize the market risks. At Cryptoflare our trading methodology incorporates risk management. It will be imprudent to enter into Forex trading without taking into account
                                        the market risks and without having a dependable risk management strategy in place.</p>
                                    <p>Risk management strategies cannot be developed off hand, number of factors have to be taken into account before the risk management strategies can be implemented. The fund managers should have adequate market experience
                                        and should have acute market analysis capabilities. When you approach Cryptoflare for your private funds management needs, you will be entrusting your funds in the most capable hands and your funds are absolutely
                                        safe.
                                    </p>
                                    <p>Our fund managers have vast experience in market analysis which adds on to your advantage. They understand the market movements better than anyone does in the industry. Cryptoflare uses proven trading system which
                                        has been designed carefully based on the latest trading scenarios. Our trading system will work effectively in all types of trading conditions. Added to that, the expertise of our fund managers will supplement
                                        the trading system in day-to-day trading activities.</p>
                                    <p>One of the most important reasons for the success of our trading system besides the expertise of our fund managers is our highly proven risk management strategies. Cryptoflare minimizes the risks and maximizes
                                        the profit and our customers are ultimately happy knowing that their funds are in the safest hands.</p>
                                </div>
                                <div class="content-inner">
                                    <p>Selecting one of the best brokers is essential to maximize all kind of advantages and services available from forex brokers worldwide. Cryptoflare preferred brokers are selected based on many criteria to suit
                                        today's advance retail clients and traders. The following are some of important criteria of our preferred brokers.</p>
                                    <h3>Global Leaders</h3>
                                    <p>Our preferred brokers have been maintaining global branding and servicing easily more than 100,000 accounts worldwide. Global footprint includes offices in strategic locations worldwide.</p>
                                    <h3>Strong Regulatory</h3>
                                    <p>Our preferred brokers are duly registered and licensed by the required legal framework of its jurisdiction. The following are combined list of some of regulatory bodies of our brokers.</p>
                                    <ul>
                                        <li>- Member of the National Futures Association (NFA) (United States)</li>
                                        <li>- A Futures Commission Merchant (FCM) with the Commodity Futures Trading Commission (CFTC) (United States)</li>
                                        <li>- Financial Services Authority (FSA) (United Kingdom)</li>
                                        <li>- Malta Financial Services Authority ( M FSA) ( Malta )</li>
                                        <li>- Monetary Authority of Singapore (MAS)</li>
                                        <li>- Cyprus Securities and Exchange Commission</li>
                                        <li>- New Zealand Financial Services Provider Register (FSPR)</li>
                                        <li>- Member of the New Zealand Financial Dispute Resolution (FDR)</li>
                                        <li>- Financial Services Agency (FSA) (Japan)</li>
                                        <li>- Australian Securities and Investments Commission (ASIC)</li>
                                        <li>- Dubai Gold &amp; Commodities Exchange (DGCX)</li>
                                        <li>- Dubai Multi Commodities Centre (DMCC)</li>
                                        <li>- National Stock Exchange of India, MCX Stock Exchange Ltd (MCX-SX), India's new stock exchange</li>
                                        <li>- Financial Instruments Directive (MiFID) EU</li>
                                    </ul>
                                    <h3>Best Execution</h3>
                                    <ul>
                                        <li>- Access to deep liquidity from top tier banks and ECNs</li>
                                        <li>- Best bid and ask prices available in the market</li>
                                        <li>- Fast and Accurate Trade executions</li>
                                        <li>- Competitive Spreads</li>
                                        <li>- Ability to place orders inside the spread</li>
                                        <li>- Multi-Language Capabilities</li>
                                    </ul>
                                    <h3>Financial Stability</h3>
                                    <p>Strong capital position includes healthy balance sheet, solid operating margins, high standards regarding financial reporting and disclosure, including the release annual financial statements.</p>
                                </div>
                                <div class="content-inner">
                                    <p>Our investment approach is driven by a combination of fundamental research, advanced market analysis, revolutionary technology platform and a persistent drive to excel on behalf our investors. Our trading program
                                        offers a unique opportunity to clients that understand the potential of the capital markets and expecting steady and exponential returns on their investment.</p>

                                    <p>Our team execute various trading strategies in equities, futures, FX, options, commodities and cryptocurrencies, providing two-sided liquidity on over two hundred market centers around the world. Clients' funds
                                        are segregated and kept in tier 1 banks monitored by strict regulatory body. Therefore, clients can rest assured that their funds are completely safe. Principal of our HFT team is development of revolutionary
                                        technology, designed to automate market making and post-trade activities to accumulate large profits over times. Cryptoflare is committed on generating targeted profits with limited risks exposure and transparency
                                        in trading to help our clients achieving their desired return.</p>
                                </div>
                                <div class="content-inner">
                                    <p>Protecting our clients' funds is one of our core values. The company complies and is governed by the European Markets in Financial Instruments Directive (MiFID) and authorised and regulated by CySEC (Licence no.
                                        334/17).
                                    </p>
                                    <p>Cryptoflare trading platforms enjoy a global reputation and provide multi-award winning services.</p>
                                    <p>We recognize the effect that global instability has on the markets and we are constantly vigilant to assure that our clients' funds are held securely with transparency and integrity.</p>
                                    <h3>Funds Safety within the System</h3>
                                    <p>We offer services through a variety of local and international financial institutions. Our risk management practices ensure that these completely independent banks are at or close to the top of Global Ratings. We
                                        are constantly reviewing our relationships using current best practices as directed by the Central Banks.</p>
                                    <h3>Segregation of Clients' Funds</h3>
                                    <p>Clients' funds are segregated, as stated in the MiFID and CySEC directives.These activities are monitored by internal compliance and external auditors to ensure conformity with regulation.</p>
                                    <p>The clients' funds are coded to ensure the funding of our clients for trading. Our auditors KPMG are one of the "big four" firms and are renowned internationally.</p>
                                    <h3>Investor Compensation Fund Protection</h3>
                                    <p>Cryptoflare, as a CySec regulated entity, is a member of the Investor's Compensation Fund under directive DI144-2007-15 OF 2015 OF THE CYPRUS SECURITIES AND EXCHANGE COMMISSION FOR THE CONTINUANCE OF OPERATION
                                        AND THE OPERATION OF THE CIF INVESTOR COMPENSATION FUND. All clients receive the highest possible degree of protection under this regulation. Clients may be eligible to be compensated in case of failure/inability
                                        of a member of the fund to fulfil its financial obligations.</p>
                                    <h3>Risk Management</h3>
                                    <p>Cryptoflare uses rigorous Risk Management tools to protect its clients and ensure that the losses are limited to their equity.</p>
                                </div>
                            </div>
                            <!-- /.content-tab -->
                        </div>
                        <!-- /.roll-tabs -->
                    </div>
                    <!-- /.item-wrap -->
                </div>
                <!-- /.Col-lg-9 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>

    <section class="flat-row section-client bg-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="flat-client" data-item="5" data-nav="false" data-dots="false" data-auto="true">
                        <div class="client"><img src="/frontend/images/clients/1.png" alt="image"></div>
                        <div class="client"><img src="/frontend/images/clients/2.png" alt="image"></div>
                        <div class="client"><img src="/frontend/images/clients/3.png" alt="image"></div>
                        <div class="client"><img src="/frontend/images/clients/4.png" alt="image"></div>
                        <div class="client"><img src="/frontend/images/clients/5.png" alt="image"></div>
                    </div>
                    <!-- /.flat-client -->
                </div>
            </div>
        </div>
    </section>

@endsection