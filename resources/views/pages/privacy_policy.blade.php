
@extends('layouts.master')

@section('content')
    <section>
        <div>
            <div class="background-holder overlay" style="background-image:url(assets/images/background-2.jpg);background-position: center bottom;"></div>
            <!--/.background-holder-->
            <div class="container">
                <div class="row pt-6" data-inertia='{"weight":1.5}'>
                    <div class="col-md-8 px-md-0 color-white" data-zanim-timeline="{}" data-zanim-trigger="scroll">
                        <div class="overflow-hidden">
                            <h1 class="color-white fs-4 fs-md-5 mb-0 zopacity" data-zanim='{"delay":0}'>Privacy Policy</h1>
                            <div class="nav zopacity" aria-label="breadcrumb" role="navigation" data-zanim='{"delay":0.1}'>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/.row-->
        </div>
        <!--/.container-->
    </section>
    <section class="background-11">
        <div class="container">

            <div class="row mt-6">
                <div class="col">
                    <h3 class="text-center fs-2 fs-md-3">Please Read Carefully</h3>
                    <hr class="short" data-zanim='{"from":{"opacity":0,"width":0},"to":{"opacity":1,"width":"4.20873rem"},"duration":0.8}' data-zanim-trigger="scroll"/>
                </div>
                <div class="col-12">
                    <div class="background-white px-3 mt-6 px-0 py-5 px-lg-5 radius-secondary">
                        <h5>General</h5>
                        <p>
                            You agree that all information, communications, materials coming from Btcroyals
                            are unsolicited and must be kept private, confidential and protected from any
                            disclosure. Moreover, the information, communications, and materials contained
                            herein are not to be regarded as an offer, or a solicitation for investments in
                            any jurisdiction which deems non-public offers or solicitations unlawful, nor to
                            any person to whom it will be unlawful to make such offer or solicitation.
                        </p>
                        <p>
                            Like most commercial website owners, we may also use what is known as "cookie"
                            technology. A "cookie" is an element of data that a website can send to your browser
                            when you link to that website. It is not a computer program and has no ability to
                            read data residing on your computer or instruct it to perform any step or function.
                            By assigning a unique data element to each visitor, the website is able to recognize
                            repeat users, track usage patterns and better serve you when you return to that site.
                            For example, a cookie can be used to store your preferences for certain kinds of
                            information or to store a password so that you do not have to input it every time you
                            visit our Site. The cookie does not extract other personal information about you,
                            such as your name and address. Most browsers provide a simple procedure that will
                            enable you to control whether you want to receive cookies by notifying you when a
                            website is about to deposit a cookie file.
                        </p>
                        <p>
                            All the data giving by a member to
                            Cryptoflare will be only privately used and not disclosed to any third parties.
                            Cryptoflare is not responsible or liable for any loss of data.
                        </p>
                    </div>
                </div>
            </div>
            <!--/.row-->
        </div>
        <!--/.container-->
    </section>
@endsection

