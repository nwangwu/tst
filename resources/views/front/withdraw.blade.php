@extends('layouts.template')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                WITHDRAW
                <small>WITHDRAW  YOUR BALANCE</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Withdraw</li>
            </ol>
        </section>
        @include('partials.errorbag')
        @if(session()->has('info'))
            @include('partials.error',['type'=>session('info')['type'],'message'=> session('info')['message']])
        @endif
        <section class="content">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Title</h3>
                </div>
                <div class="box-body">
                    <div class="panel-body">
                        Once you have collected a certain amount on your account balance, you can make a withdrawal request. <br>
                        Fill in the details below and get your earned Bitcoins! (Withdrawal fee: 0.00000000 <i class="fa fa-btc"></i>, minimum amount: 0.001 <i class="fa fa-btc"></i>)<br>
                        All your withdrawal requests are processed automatically and instantly - you will get your Bitcoins just in few seconds. <br>
                        You can withdraw as many times as you need, but there is a limit of <strong>1 withdrawal per day</strong> to keep program our program stabile.
                        <hr>
                        <span class="text-center bg-red"><strong>Notice:</strong> The speed of withdrawals may be delayed (even &gt;6 hours) depending on the speed of the Bitcoin Service. Please be patient - it doesn't depends on us!</span>
                        <hr>
                        <form action="{{ route('withdraw_post') }}" method="post" accept-charset="utf-8">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="withdraw_amount">Amount</label> <small class="form-text text-muted">Your current balance: {{ sprintf('%.8f',$balance) }} <i class="fa fa-btc"></i></small>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-btc"></i></span>
                                    <input class="form-control" id="withdraw_amount" value="{{ old('withdraw_amount') }}" name="withdraw_amount" placeholder="How much you want withdraw (eg. 0.1524234)" type="text">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="withdraw_address">Enter your BTC address</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-paper-plane-o"></i></span>
                                    <input class="form-control" value="{{old('withdraw_address')}}" id="withdraw_address" name="withdraw_address" placeholder="Where Bitcoins should be sent (eg. 1Hcbac1NaKGwutScm34texZUuqFz59HAq9)" type="text">
                                </div>
                                <!--<small id="bitcoinaddressHelp" class="form-text text-muted">We are completely transparent, and you can track all deposits on our site <a href="https://blockchain.info/address/">here</a></small>-->
                            </div>
                            <div class="col-md-offset-4 col-md-4 col-sm-4">
                                <button type="submit" class="btn btn-primary btn-block">Withdraw your Bitcoins</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection