@extends('layouts.template')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                AFFILIATE
                <small>EARN MORE THROUGH AFFILIATES</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Affiliate</li>
            </ol>
        </section>
        @include('partials.errorbag')
        @if(session()->has('info'))
            @include('partials.error',['type'=>session('info')['type'],'message'=> session('info')['message']])
        @endif
        <section class="content">
            <div class="form-group">
                <label>Your affiliate link</label>
                <input class="form-control" type="text" readonly value="{{ url('ref/'. auth()->user()->name)  }}"/>
            </div>
            <div class="box">
                <div class="box-header with-border">

                </div>
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Username</th>
                                <th>Real Deposit</th>
                                <th>Commission</th>
                                <th>Member BTC address</th>
                                <th>Registered</th>
                                <th>Last login </th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($affiliate as $dd)
                                <tr>
                                    <td>{{ \App\User::find($dd->child_id)->name }}</td>
                                    <td><span class="label label-success">{{ sprintf('%.8f',$dd->real_deposit) }}<i class="fa fa-btc"></i></span></td>
                                    <td><span class="label label-danger" data-content="{{$dd->commission}}" id="commission{{$dd->child_id}}">{{ sprintf('%.8f',$dd->commission) }}<i class="fa fa-btc"></i></span></td>
                                    <td><a class="btn btn-default">{{ $dd->member_btc_address }}</a></td>
                                    <td>{{ $dd->member_registered }}</td>
                                    <td>{{ $dd->member_last_login }}</td>
                                    <td>
                                        @if(!$dd->reinvested)

                                            @component('partials.action')
                                            {{ $dd->child_id }}
                                            @endcomponent

                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    {{ $affiliate->links() }}
                </div>
                <!-- /.box-footer-->
            </div>
        </section>
    </div>
@endsection