@extends('layouts.template')


@section('content')

<div class="content-wrapper">

    <section class="content-header">
        <h1>
            Dashboard
            <small>CONTROL YOUR PROFIT</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>
    @if(session()->has('info'))
        @include('partials.error',['type'=>session('info')['type'],'message'=> session('info')['message']])
    @endif
    <section class="content">
        <!-- Info boxes -->
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-btc"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">ACTIVE DEPOSIT</span>
                        <span class="info-box-number">{{ sprintf('%.8f',$active_deposit)  }}<i class="fa fa-btc"></i></span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="fa fa-cog"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">TOTAL EARNED</span>
                        <span class="info-box-number">{{ sprintf('%.8f',$total_earned) }}<i class="fa fa-btc"></i></span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>

            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-money"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">WITHDRAW BALANCE</span>
                        <span class="info-box-number">{{ sprintf('%.8f',$balance) }}<i class="fa fa-btc"></i></span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="fa fa-hourglass-half"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">NEXT VALORIZATION</span>
                        <span class="info-box-number"><div id="countdown_valor"></div></span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Profit Prediction Simulation</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-8">
                                <p class="text-center">
                                    <strong>Profit for the next 7 days</strong>
                                </p>

                                <div class="chart">
                                    <!-- Sales Chart Canvas -->
                                    <canvas id="salesChart" style="height: 180px;"></canvas>
                                </div>
                                <!-- /.chart-responsive -->
                            </div>
                            <!-- /.col -->
                            <div class="col-md-4">
                                <p class="text-center">
                                    <strong>Your current earnings</strong>
                                </p>
                                <div class="table-responsive">
                                    <table class="table no-margin text-center">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Amount</th>
                                            <th>Date</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($earnings as $ee)
                                            <tr>
                                                <td>{{ $ee->id }}</td>
                                                <td>{{ sprintf('%.8f',$ee->amount) }}</td>
                                                <td>{{ $ee->created_at }}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- ./box-body -->
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-sm-3 col-xs-6">
                                <div class="description-block border-right">
                    <span class="description-block text-green">
                        <h5 class="description-header"><i class="fa fa-caret-left"></i>
                            {{ sprintf('%.8f',$hourly_earning) }}<i class="fa fa-btc"></i></h5>
                    </span>
                                    <span class="description-text">HOURLY EARNINGS</span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-3 col-xs-6">
                                <div class="description-block border-right">
                    <span class="description-block text-green"><h5 class="description-header">
                            <i class="fa fa-caret-left"></i>
                            {{ sprintf('%.8f',$daily_earning) }}<i class="fa fa-btc"></i></h5>
                    </span>
                                    <span class="description-text">CURRENT DAILY EARNINGS</span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-3 col-xs-6">
                                <div class="description-block border-right">
                    <span class="description-block text-green"><h5 class="description-header">
                            <i class="fa fa-caret-left"></i>
                            {{ sprintf('%.8f',$total_deposit) }}<i class="fa fa-btc"></i></h5>
                    </span>
                                    <span class="description-text">TOTAL DEPOSIT</span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-3 col-xs-6">
                                <div class="description-block">
                    <span class="description-block {{(($total_earned - $total_deposit) < 0.0 )?'text-red': 'text-green'}}"><h5 class="description-header">
                            <i class="fa {{(($total_earned - $total_deposit) < 0.0 )?'fa-caret-down': 'fa-caret-up'}}"></i>
                           {{ sprintf('%.8f',(($total_earned - $total_deposit)< 0.0) ? $total_earned : ($total_earned - $total_deposit)) }} <i class="fa fa-btc"></i></h5>
                    </span>
                                    <span class="description-text">TOTAL PROFIT</span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <div class="col-md-8">
                <!-- MAP & BOX PANE -->
                <div class="box box-solid">
                    <div class="box-header with-border">
                        <!--<h3 class="box-title">About btcroyals.com</h3>-->
                    </div>

                    <div class="box-body">
                        <div class="box-group" id="accordion">
                            <div class="panel box box-primary">
                                <div class="box-header with-border">
                                    <h4 class="box-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" class="collapsed">
                                            Why you should make investments in btcroyals.com Company?
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                    <div class="box-body">
                                        Btcroyals.com  offer you the best chance to make Bitcoins online without leaving your home.
                                        We are focusing on providing <u>stable</u>, <u>secure</u> and <u>long-time</u> investments.<br>
                                        We do not ask you to send documents or share your personal informations. Our cooperation is
                                        fully anonymous. <br>
                                        All your deposits will run on a regular basis and bring profit of {{ env('DAILY_GROWTH_RATE')*100 }}% each calendar
                                        day ({{ env('HOURLY_GROWTH_RATE')*100 }}% hourly), with the ability to instantly withdraw your funds whenever you want.<br>
                                        You will get 10% of all received Bitcoin from your referral members. It's a fast
                                        and easy way to make Bitcoin without your own investment.<br>
                                        btcroyals.com has experienced technical staffs who are cautious and do its utmost
                                        for the safety of your investment and stable hourly profits.<br><br>
                                        <strong>You can start earning Bitcoins in few seconds - it's simple!</strong>
                                    </div>
                                </div>
                            </div>
                            <div class="panel box box-danger">
                                <div class="box-header with-border">
                                    <h4 class="box-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="" aria-expanded="true">
                                            Security
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse in" aria-expanded="true" style="">
                                    <div class="box-body">
                                        Security is one of our company main goals.
                                        Secured servers in few different localizations to guarantee that our site will
                                        work smoothy without any problem. <br>
                                        We only use trusted and well-tested software. Every server has professional
                                        operating personnel.<br><br>
                                        Because we only use <strong>Bitcoin</strong> and we don't convert Bitcoin to USD
                                        so you won't face issues related with currency price changes.<br>
                                        One of the major benefits of a digital currency is its ability to circumvent the
                                        need for a central bank. This makes Bitcoin secure and attractive as a currency
                                        since the mining of Cryptocurrency isn't regulated by any outside agents.<br>
                                        Bitcoins solve the problem which most digital currencies fail to solve: how to
                                        make sure that digital signatures weren't being used multiple times.<br><br>
                                        btcroyals.com is constantly monitored and protected against DDoS attacks, it's
                                        scanned for malicious software and viruses. Our databases are hosted on a dedicated
                                        servers and transactions security is protected by SSL.
                                    </div>
                                </div>
                            </div>
                            <div class="panel box box-success">
                                <div class="box-header with-border">
                                    <h4 class="box-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="collapsed" aria-expanded="false">
                                            About btcroyals.com
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse" aria-expanded="false">
                                    <div class="box-body">
                                        The company was created by a group of qualified experts, professional bankers,
                                        traders and analysts who specialized in the stock, bond, futures, currencies,
                                        gold, silver and oil trading with having more than ten years of extensive practical
                                        experiences of combined personal skills, knowledge, talents and collective ambitions
                                        for success.
                                        <br>
                                        BTCRoyals is a fully automated bitcoin platform operating with minimal human intervention,
                                        aside from regular server maintenance. Take full advantage of our powerful and high frequency
                                        investment platform. Our automated system gathers information from the blockchain transfers and
                                        cryptocurrency exchanges to study and predict the bitcoin price, our servers open and close thousands
                                        of transactions per minute,
                                        analyzing the price difference and transaction fees, and use that information to generate profit.
                                        <br>
                                        That's why you'll be <strong>guaranteed</strong> to earn your hourly profits and instantly
                                        withdraw them.<br><br>
                                        If you have any questions, feel free to ask in the
                                        <a href="{{ route("contactus") }}">Contact</a> section, we will answer as soon as possible!
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box -->
            <div class="col-md-4">

                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>{{ ($registered_users) }}</h3>

                        <p>Registered Users</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-users"></i>
                    </div>
                </div>

                <!-- ./col -->
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>{{ ($active_users ) }}</h3>

                        <p>Active Users</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                </div>

                <!-- ./col -->

                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>{{ sprintf('%.8f',($total_site_deposit )) }}</h3>

                        <p>Total site deposit</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-bank"></i>
                    </div>
                </div>

                <!-- ./col -->

                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3>{{ sprintf('%.8f',($total_site_withdraw )) }}</h3>

                        <p>Total site withdraw</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-print"></i>
                    </div>
                </div>
            </div>
            <!-- ./col -->
            <!-- /.box -->
        </div>
        <!-- /.col -->

        <!-- TABLE: LATEST ORDERS -->

        <div class="row  vert-offset-top-1 text-center">
            <div class="col-md-6">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Latest Deposits</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table no-margin">
                                <tbody>
                                @foreach($deposit as $depo)
                                    <tr>
                                        <td><span class="label label-success">Deposit</span></td>
                                        <td>{{ $depo->account->user->name }}</td>
                                        <td class="text-info">{{ sprintf('%.8f',$depo->amount) }}<i class="fa fa-btc"></i></td>
                                        <td class="text-green">{{ $depo->status == 'reinvest' ? 'Reinvest' : 'Bitcoin'}}</td>
                                        <td>{{ $depo->updated_at }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.box-footer -->
                </div>
            </div>
            <div class="col-md-6">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Latest Withdrawals</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table no-margin">
                                <tbody>
                                @foreach($withdraw as $withd)
                                    <tr>
                                        <td><span class="label label-danger">Withdraw</span></td>
                                        <td>{{ $withd->account->user->name }}</td>
                                        <td class="text-green">{{ sprintf('%.8f',$withd->amount) }}<i class="fa fa-btc"></i></td>
                                        <td>{{ $withd->updated_at }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>

                </div>
            </div>
        </div>

        <!-- /.box -->
        <!-- /.col -->
    </section>
    <!-- /.content -->
</div>



<!-- /.content-wrapper -->
@endsection