@extends('layouts.template')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                SETTINGS
                <small>CHANGE YOUR PROFILE SETTINGS</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Setting</li>
            </ol>
        </section>
        @include('partials.errorbag')
        @if(session()->has('info'))
            @include('partials.error',['type'=>session('info')['type'],'message'=> session('info')['message']])
        @endif
        <section class="content">
            <div class="box">
                <div class="box-header with-border">
                </div>
                <div class="box-body">

                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                <div class="col-md-4">
                    <div class="box box-primary">
                        <div class="box-body box-profile">
                            <h3 class="profile-username text-center">{{ucfirst(auth()->user()->name)}}</h3>
                            <ul class="list-group list-group-unbordered">

                                <li class="list-group-item">
                                    <b>Email</b> <a class="pull-right">{{auth()->user()->email}}</a>
                                </li><li class="list-group-item">
                                    <b>Uplink</b> <a class="pull-right alert-warning">{{auth()->user()->parent}}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>BTC address to send Deposit</b>
                                    <input class="form-control" id="btc" value="{{auth()->user()->bit_deposit_address}}" type="text" readonly/>
                                </li>
                                <li class="list-group-item">
                                    <b>BTC address for withdraw payment</b>
                                    <input class="form-control" value="{{auth()->user()->bit_owner_address}}" type="text" readonly />
                                </li>
                            </ul>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
                    <div class="col-md-8">
                        <div class="box box-info">
                            <!-- /.box-header -->
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                    <h3 class="box-title"></h3>
                                </div>
                                <!-- /.box-header -->
                                <!-- form start -->
                                <form role="form" method="post" action="{{route('setting.store')}}">
                                    {{ csrf_field() }}
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">BTC address to send deposit</label>
                                            <input class="form-control" value="{{ auth()->user()->bit_deposit_address }}" id="btc" placeholder="" type="text" readonly/>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Recipient BTC address for payouts</label>
                                            <input class="form-control" name="btc_address" id="btc_address" value="{{auth()->user()->bit_owner_address}}" placeholder="BTC address" type="text">
                                            <small>You may leave this empty and enter it whenever you want to perform withdraw transaction</small>
                                        </div>
                                        @if(!is_null(auth()->user()->facebook_id) && is_null(auth()->user()->password))
                                        <blockquote class="text-red">You can create a password to enable you login later with the local login system</blockquote>
                                        @endif
                                        <div class="form-group">
                                            <label for="password">New Password</label>
                                            <input class="form-control" name="password" id="password" placeholder="Password" type="password">
                                        </div>
                                        <div class="form-group">
                                            <label for="confirm_password">Re-enter New Password</label>
                                            <input class="form-control" name="password_confirmation" placeholder="Password" type="password">
                                        </div>
                                    </div>
                                    <!-- /.box-body -->

                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-primary btn-block">Save Settings</button>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- /.box-footer-->
            </div>
        </section>
    </div>
@endsection