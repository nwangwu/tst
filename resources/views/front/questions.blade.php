@extends('layouts.template')


@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                ABOUT US
                <small>READ ABOUT OUR SERVICE</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">faq</li>
            </ol>
        </section>

        <section class="content">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">About our services</h3>
                </div>
                <div class="box-body">
                    <div class="col-md-offset-2 col-md-8">
                        <p>The company was created by a group of qualified experts, professional bankers,
                            traders and analysts who specialized in the stock, bond, futures, currencies,
                            gold, silver and oil trading with having more than ten years of extensive practical
                            experiences of combined personal skills, knowledge, talents and collective
                            ambitions for success.
                        </p>
                        <p>BTCRoyals is a fully automated bitcoin platform operating with minimal human intervention,
                            aside from regular server maintenance. Take full advantage of our powerful and high frequency
                            investment platform. Our automated system gathers information from the blockchain transfers
                            and cryptocurrency exchanges to study and predict the bitcoin price, our servers open and
                            close thousands of transactions per minute, analyzing the price difference and transaction
                            fees, and use that information to generate profit.
                        </p>

                        <p>We believe that superior investment performance is achieved through a skillful balance
                            of three core attributes: knowledge, experience and adaptability. There is only one way
                            to be on the cutting edge – commitment to innovation. We do our best to achieve a
                            consistent increase in investment performance for our clients, and superior value-add.
                            We appreciate our clients loyalty and value the relationships we build with each customer.
                        </p>

                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection