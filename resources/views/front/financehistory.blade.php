@extends('layouts.template')


@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                HISTORY
                <small>CHECK YOUR ACCOUNT HISTORY</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">History</li>
            </ol>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">History made so far</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                    <li class=""><a href="#tab_1" data-toggle="tab" aria-expanded="false">EARNINGS</a></li>
                                    <li class="active"><a href="#tab_2" data-toggle="tab" aria-expanded="true">DEPOSITS</a></li>
                                    <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">TOP WITHDRAWS</a></li>
                                    <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane" id="tab_1">
                                        <div class="table-responsive">
                                            <table class="table table-hover">
                                                <thead>
                                                <tr>
                                                    <th>Profit Amount</th>
                                                    <th>Profit Date</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($earning as $earnd)
                                                        <tr>
                                                            <td>$earnd->amount<i class="fa fa-btc"></i></td>
                                                            <td>$earnd->updated_at</td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                            <div class="box-footer">
                                                {{ $earning->links() }}
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.tab-pane -->
                                    <div class="tab-pane active" id="tab_2">
                                        <div class="table-responsive">
                                            <table class="table table-hover">
                                                <thead>
                                                <tr>
                                                    <th>Deposit amount</th>
                                                    <th>Deposit source</th>
                                                    <th>Deposit status</th>
                                                    <th>Deposit date</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($deposit as $top_d)
                                                    <tr>
                                                        <td>{{ sprintf('%.8f',$top_d->amount) }}<i class="fa fa-btc"></i></td>
                                                        <td class="{{ $top_d->status =='first' || $top_d->status == 'closed' ? 'text-warning': 'text-green'}}" >
                                                            {{ $top_d->status =='first' || $top_d->status == 'closed'?'Bitcoin': ucfirst($top_d->status)}}</td>
                                                        <td><span class="label label-success">{{ $top_d->status !=='cancelled'?'Accepted':'Cancelled' }}</span></td>
                                                        <td>{{ $top_d->updated_at }}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                            <div class="box-footer">
                                                {{ $deposit->links() }}
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.tab-pane -->
                                    <div class="tab-pane" id="tab_3">
                                        <div class="table-responsive">
                                            <table class="table table-hover">
                                                <thead>
                                                <tr>
                                                    <th>Withdraw amount</th>
                                                    <th>Withdraw address</th>
                                                    <th>Withdraw TXID</th>
                                                    <th>Withdraw status</th>
                                                    <th>Withdraw date</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($withdraw as $top_w)
                                                    <tr>
                                                        <td>{{ sprintf('%.8f',$top_w->amount) }}<i class="fa fa-btc"></i></td>
                                                        <td>{{ $top_w->bit_user_address }}</td>
                                                        <td>{{ $top_w->with_trans_id }}</td>
                                                        <td><span class="label label-danger">{{ ucfirst($top_w->status) }}</span></td>
                                                        <td>{{ $top_w->updated_at }}</td>
                                                    </tr>
                                                @endforeach
                                            </table>

                                            <div class="box-footer">
                                                {{ $withdraw->links() }}
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.tab-pane -->
                                </div>
                                <!-- /.tab-content -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.box-footer -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
        </section>
    </div>
@endsection