@extends('layouts.template')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                WITHDRAW DEPOSIT
                <small>CANCEL YOUR DEPOSIT</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Cancel deposit</li>
            </ol>
        </section>
        @include('partials.errorbag')
        @if(session()->has('info'))
            @include('partials.error',['type'=>session('info')['type'],'message'=> session('info')['message']])
        @endif

        <section class="content">
            <div class="box">
                <div class="panel-body">
                    Here, you can use  Bitcoins from your account (current balance) to make new investments - with higher profit (minimum amount: {{ env('MIN_REINVEST') }} <i class="fa fa-btc"></i>)<br>
                    Reinvesting is a very good option when you want to earn more Bitcoins in a shorter period (you don't have to send Bitcoin to your wallet then resend them back)
                    <hr>
                    <form action="{{ route('reinvest_post') }}" method="post" accept-charset="utf-8">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="reinvest_amount">Amount</label> <small class="form-text text-muted">Your current balance: {{ sprintf('%.8f',$balance) }} <i class="fa fa-btc"></i></small>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-btc"></i></span>
                                <input class="form-control" name="reinvest_amount" placeholder="How much you want reinvest (eg. 0.54522)" type="text">
                            </div>
                        </div>
                        <div class="col-md-offset-4 col-md-4 col-sm-4 col-xs-4">
                            <button type="submit" class="btn btn-success btn-block">Reinvest Bitcoin</button>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
@endsection