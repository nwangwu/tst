@extends('layouts.template')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                WITHDRAW ACTIVE DEPOSIT
                <small>WITHDRAW YOUR ACTIVE DEPOSIT</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">withdraw deposit</li>
            </ol>
        </section>
        @include('partials.errorbag')
        @if(session()->has('info'))
            @include('partials.error',['type'=>session('info')['type'],'message'=> session('info')['message']])
        @endif
        <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Withdraw active deposit</h3>
            </div>
            <div class="box-body">
                <form action="{{ route('cancel_deposit') }}" method="post" accept-charset="utf-8">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="withdraw_amount">Amount</label> <small class="form-text text-muted">Your active deposit: {{ sprintf('%.8f',auth()->user()->account->active_deposit) }} <i class="fa fa-btc"></i></small>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-btc"></i></span>
                            <input class="form-control" id="withdraw_amount" value="{{ old('withdraw_amount') }}" name="withdraw_amount" placeholder="How much you want to withdraw (eg. 0.1524234)" type="text">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="withdraw_address">Enter your BTC address</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-paper-plane-o"></i></span>
                            <input class="form-control" value="{{old('withdraw_address')}}" id="withdraw_address" name="withdraw_address" placeholder="Where Bitcoins should be sent (eg. 1Hcbac1NaKGwutScm34texZUuqFz59HAq9)" type="text">
                        </div>
                       <!-- <small id="bitcoinaddressHelp" class="form-text text-muted">We are completely transparent, and you can track all deposits on our site <a href="https://blockchain.info/address/">here</a></small>-->
                    </div>
                    <div class="col-md-offset-4 col-md-4 col-sm-4">
                        <button type="submit" class="btn btn-primary btn-block">Withdraw Deposit</button>
                    </div>
                </form>

            </div>
            <!-- /.box-body -->
            <div class="box-footer">

            </div>
            <!-- /.box-footer-->
            </div>
        </section>
    </div>
@endsection