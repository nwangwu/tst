@extends('layouts.template')


@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                TERMS OF SERVICE
                <small>READ OUR TERMS OF SERVICE</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">terms of service</li>
            </ol>
        </section>

        <section class="content">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Our terms of service</h3>
                </div>
                <div class="box-body">
                    <div class="col-md-offset-2 col-md-8">
                        Please take the time to read and understand our Terms and Conditions.
                        The following is the legal contract between You (Client) and Btcroyals.com.
                        By making an investment with us, you automatically state that you have read,
                        fully understood, accept and agree to abide by our Terms and Conditions when
                        using our services. Terms and Conditions define eBitInvest services, including
                        Client`s and eBitInvest obligations, with regard to any transactions through
                        Btcroyals.com website.
                        <ol>
                            <li>General</li>
                            <ul>
                                <li>Residents of any country can invest through Btcroyals.</li>
                                <li>Client invest funds into investment plans offered by Btcroyals using Bitcoin cryptocurrency.</li>
                                <li>Btcroyals requires you to act as an individual and not on behalf of any other entity or authority.</li>
                            </ul>
                            <li>Depositing funds</li>
                            <ul>
                                <li>Depositing funds to Btcroyals is executed with Bitcoin cryptocurrency in accordance with the Client`s Agreement.</li>
                                <li>You must deposit funds to the Bitcoin address as stated in the fourth step of the main form only. You can make as many deposits as you want to this address.</li>
                                <li>The deposit will be added to our list of investments after the transaction is confirmed by the network.</li>
                            </ul>
                            <li>Payouts</li>
                                <ul>
                                    <li>Btcroyals investment plan earnings are paid every day, seven days a week.</li>
                                    <li>The unavailability of Btcroyals website due to technical reasons or downtime will not affect the payouts.</li>
                                    <li>The payout usually takes around 10 minutes. In some cases, the payout may be delayed up to 12 hours because we need to refill the hot wallet of the cryptocurrency.</li>
                                    <li>Any commissions for the transfer of Bitcoin cryptocurrency are paid by Btcroyals.</li>
                                </ul>
                            <li>Warranties</li>
                                <ul>
                                    <li>Btcroyals warrants the safety of the invested amounts and interest stability.</li>
                                    <li>Btcroyals guarantees all liayments will be made on time and under the terms stiliulated in this Agreement.</li>
                                    <li>Btcroyals is not associated with any cryptocurrency payment system and, therefore, is not liable for any problems in their performance.</li>
                                </ul>
                            <li>Accepting Terms and Conditions</li>
                            <ul>
                                <li>By accepting these Terms, you are entering into a contract with Btcroyals and accept the conditions related to making an investment. You agree that your age is legal in your country to participate in this program, and in all the cases your minimal age must be 18 year.</li>
                                <li>You agree to invest money only which you can afford to lose. You understand that in all investment arrangements, past performance is no guarantee of the same future performance. Future earnings can be different with past earnings. We have the right to configure our existing investment plans anytime.</li>
                                <li>You agree that all information, communications and materials you will find on this website are intended to be regarded as informational and educational matter and not an investment advice.</li>
                                <li>You acknowledge that you are acting as an individual and not for any other entity. By investing, you
                                    agree, acknowledge, and accept that all information you receive is unsolicited, private communications
                                    of privileged, proprietary, and confidential information for you only and you agree to keep it
                                    private, confidential, and protected from any disclosure unless the information is obviously of a
                                    public nature.</li>
                            </ul>
                            <li>Copyright</li>
                                <ul>
                                    <li>We respect the intellectual property rights of others and require that the people who use Btcroyals do the same.</li>
                                    <li>The contents of this site are protected by copyright and trademark laws and are the property of eBitInvest. Unless we say otherwise, you may access the materials located within Btcroyals only for your personal use.</li>
                                    <li>You may not modify, copy, publish, display, transmit, adapt, or in any way exploit the content of Btcroyals website. Only having obtained prior written consent from us, and from all other entities with an interest in the relevant intellectual property, you may publish, display, or commercially exploit
                                        any material from Btcroyals. To seek our permission, you must contact us directly.</li>
                                </ul>
                            <li>Liability and Force Majeure.</li>
                            <ul>
                                <li>Btcroyals is not responsible for delays in the transmission of any order due to reasons beyond its control. eBitInvest does not control signal power, data transmission and receipt via Internet, or configuration of the Client's computer equipment or the reliability of its connection.</li>
                                <li>Btcroyals is not accountable for any damage or loss of data caused by any events, actions, or omissions, which may lead to delay or distortions in the transmission of information, requests, and orders due to a breakdown in any transmission facilities that are beyond the control of Btcroyals.</li>
                                <li>Btcroyals cannot be held responsible for any data communication failure, distortion, or delay if problems originate from the Client's side.</li>
                                <li>Btcroyals is not liable for any default under this Agreement that may result from force majeure circumstances, including but not limited to military actions, wars, strikes, rebellions, natural disasters, governmental bans, or any breakdown in normal communications or the infrastructure services of utilities.</li>
                            </ul>
                            <li>Terms and Conditions Amendments</li>
                            <ul>
                                <li>Btcroyals reserves the right to modify Terms and Conditions at any time. You should check the
                                    Terms and Conditions periodically for any changes. By using this site after we have posted any
                                    changes to these Terms and Conditions, you agree to accept those changes. If at any time you choose
                                    not to accept these Terms and Conditions of use, you must cease to use this site</li>
                            </ul>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection