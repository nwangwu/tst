@extends('layouts.template')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                DEPOSIT
                <small>START EARNING BITCOIN</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Deposit</li>
            </ol>
        </section>
        @include('partials.errorbag')
        @if(session()->has('info'))
            @include('partials.error',['type'=>session('info')['type'],'message'=> session('info')['message']])
        @endif

        <section class="content">
            <div class="box">
                <div class="panel-body">
                    To start earning Bitcoins, pay in any number of bitcoins above {{ env('MIN_DEPOSIT') }} <i class="fa fa-btc"></i> to your personal Bitcoin address below.<br>
                    Your payment has to go through a validation process to be credited. This process usually takes up to 60 minutes, depending on the speed of Bitcoin service. Sometimes, you can face delays for up to 24 hours, contact us if you experience any difficulties.<br>
                    <br>
                    After successful validation of your payment, click on the button below and your deposit balance will be automatically credited, and you will get your hourly income <strong>{{ env('HOURLY_GROWTH_RATE')*100 }}% (about {{ env('DAILY_GROWTH_RATE')*100 }}% daily) lifetime!</strong> <br>
                    <!--For instance, if you deposit 1 <i class="fa fa-btc"></i>, you will get 3 <i class="fa fa-btc"></i> after 30 days! We are completely transparent, you can check site income whenever you want just by checking our main wallet <a href="https://blockchain.info/address/">here</a>.<br>-->
                    <br>
                    <strong>Notice:</strong> For earnings calculations, we round up and assume that {{ env('DAILY_GROWTH_RATE')*100 }}% divided by 24 hours is <strong>{{ env('HOURLY_GROWTH_RATE')*100 }}% per 1 hour</strong>, because of round-off error your earnings can be slightly different from {{ env('DAILY_GROWTH_RATE')*100 }}%/daily <strong>(</strong> in most cases you will earn a little bit more<strong>)</strong><br>
                    <hr>
                    <strong style="color: lightgreen;">Notice 2:</strong> Because of high interest of our site the system checks for new deposits ONLY if you click at
                    <b>'CONFIRM DEPOSIT'</b> after your bitcoin transaction has gotten 3 confirmations<br>
                    <hr>
                    <br>
                    You can request withdraw whenever you want, even just after first valorisation (every hour)<br>
                    <hr>
                    <div class="form-group">
                        <label for="bitcoinaddress">Your personal Bitcoin address</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-money"></i></span>
                            <input class="form-control" id="bitcoinaddress" value="{{ $deposit_address }}" readonly type="text">
                        </div>
                        <small id="bitcoinaddressHelp" class="form-text text-muted">After 3 confirmations click on the button below and your deposit will be automatically credited to your account. <br>
                            <!--We are completely transparent, and you can track all deposits on our site <a href="https://blockchain.info/address/">here</a>--></small>
                    </div>
                    <form action="{{ route('deposit_post') }}" method="post" accept-charset="utf-8">
                        {{ csrf_field() }}
                        <div class="col-md-4 col-md-offset-4">
                            <button name="check_deposit" type="submit" class="btn btn-primary btn-block">Confirm Deposit</button>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
@endsection