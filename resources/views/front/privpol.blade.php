@extends('layouts.template')


@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                PRIVACY POLICY
                <small>READ OUR PRIVACY POLICY</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">privacy policy</li>
            </ol>
        </section>

        <section class="content">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Our privacy policy</h3>
                </div>
                <div class="box-body">
                    <div class="col-md-offset-2 col-md-8">
                    <p>
                        You agree that all information, communications, materials coming from Btcroyals
                        are unsolicited and must be kept private, confidential and protected from any
                        disclosure. Moreover, the information, communications, and materials contained
                        herein are not to be regarded as an offer, or a solicitation for investments in
                        any jurisdiction which deems non-public offers or solicitations unlawful, nor to
                        any person to whom it will be unlawful to make such offer or solicitation.
                    </p>
                        <p>
                        Like most commercial website owners, we may also use what is known as "cookie"
                        technology. A "cookie" is an element of data that a website can send to your browser
                        when you link to that website. It is not a computer program and has no ability to
                        read data residing on your computer or instruct it to perform any step or function.
                        By assigning a unique data element to each visitor, the website is able to recognize
                        repeat users, track usage patterns and better serve you when you return to that site.
                        For example, a cookie can be used to store your preferences for certain kinds of
                        information or to store a password so that you do not have to input it every time you
                        visit our Site. The cookie does not extract other personal information about you,
                        such as your name and address. Most browsers provide a simple procedure that will
                        enable you to control whether you want to receive cookies by notifying you when a
                        website is about to deposit a cookie file.
                        </p>
                        <p>
                        All the data giving by a member to
                        Cryptoflare will be only privately used and not disclosed to any third parties.
                        Cryptoflare is not responsible or liable for any loss of data.
                        </p>

                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection