@extends('layouts.template')

@section('content')
<div class="content-wrapper">
    <section class="content-header">
    <h1>
        LEADERBOARD
        <small>TOP TRANSACTIONS</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Leaderboard</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Make it to our leaderboard</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <!--<li class=""><a href="#tab_1" data-toggle="tab" aria-expanded="false">TOP EARNERS</a></li>-->
                            <li class="active"><a href="#tab_2" data-toggle="tab" aria-expanded="true">TOP DEPOSIT</a></li>
                            <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">TOP WITHDRAWALS</a></li>
                            <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
                        </ul>

                        <div class="tab-content">
                        <!--    <div class="tab-pane" id="tab_1">
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>Member Username</th>
                                            <th>Deposited</th>
                                            <th>Earned</th>
                                            <th>Withdrawed</th>
                                            <th>Affiliates</th>
                                            <th>Registered</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($top_member as $top)
                                            <tr>
                                                <td>{{ $top->user->name }}</td>
                                                <td  class="text-green">{{ sprintf('%.8f',$top->total_deposit) }}<i class="fa fa-btc"></i></td>
                                                <td>{{ sprintf('%.8f',$top->total_earned) }}<i class="fa fa-btc"></i></td>
                                                <td class="text-info">{{ sprintf('%.8f',$top->total_deposit - $top->active_deposit) }}<i class="fa fa-btc"></i></td>
                                                <td>{{ is_null($top->user->affiliate) ? 0 : $top->user->affiliate->count() }}</td>
                                                <td>{{ $top->created_at }}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>-->
                            <!-- /.tab-pane -->
                            <div class="tab-pane active" id="tab_2">
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>Member Username</th>
                                            <th>Amount</th>
                                            <th>Source</th>
                                            <th>Status</th>
                                            <th>Date</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($top_deposit as $top_d)
                                            <tr>
                                                <td>{{ $top_d->account->user->name }}</td>
                                                <td>{{ sprintf('%.8f',$top_d->amount) }}<i class="fa fa-btc"></i></td>
                                                <td class="{{ $top_d->status =='first' || $top_d->status == 'closed' ? 'text-warning': 'text-green'}}" >
                                                    {{ $top_d->status =='first' || $top_d->status == 'closed'?'Bitcoin': ucfirst($top_d->status)}}</td>
                                                <td><span class="label label-success">Accepted</span></td>
                                                <td>{{ $top_d->updated_at }}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="tab_3">
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>Member Username</th>
                                            <th>Amount</th>
                                            <th>Status</th>
                                            <th>Date</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($top_withdraw as $top_w)
                                            <tr>
                                                <td>{{ $top_w->account->user->name }}</td>
                                                <td>{{ sprintf('%.8f',$top_w->amount) }}<i class="fa fa-btc"></i></td>
                                                <td><span class="label label-danger">Withdrawed</span></td>
                                                <td>{{ $top_w->updated_at }}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-footer -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
</section>
    </div>
@endsection