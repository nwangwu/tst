@extends('layouts.template')
@push('styles')
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
@endpush
@push('scripts')
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
@endpush


@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                AUTOMATION
                <small>AUTOMATE YOUR EARNINGS</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Automation</li>
            </ol>
        </section>
        @include('partials.errorbag')
        @if(session()->has('info'))
            @include('partials.error',['type'=>session('info')['type'],'message'=> session('info')['message']])
        @endif
        <section class="content">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Automate to step up your earning</h3>
                </div>
                <div class="box-body">
                    <form method="post" action="{{ route('automation_post') }}">
                        {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="callout callout-success text-center">
                                <strong>Automate Withdrawal</strong>
                            </div>
                            <div class="row text-center"><strong>Enable Auto withdrawal</strong><small>
                                    (once every 24 hours)
                                </small></div>
                            <div class="row text-center">
                                <input data-toggle="toggle" {{ $auto_with ? 'checked':''}}  data-on="<i class='fa fa-check'></i> Enabled"
                                       data-off="<i class='fa fa-close'></i> Disabled" name="auto_with_check" type="checkbox"/>
                            </div>

                                <div class="form-group">
                                    <label for="member_auto_withdraw_address">Enter your BTC address</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-paper-plane-o"></i></span>
                                        <input class="form-control" name="auto_withdraw_address"
                                               placeholder="Where Bitcoins should be sent (eg. 1Feabc1QaKQutScm39hexYUuqFz89IAy9)"
                                               value="{{ old('auto_withdraw_address') }}" type="text">
                                    </div>
                                    <small id="bitcoinaddressHelp" class="form-text text-muted">
                                        We are completely transparent, and you can track all deposits on our site
                                        <a href="https://blockchain.info/address/">here</a></small>
                                </div>

                        </div>
                        <div class="col-md-6">
                            <div class="callout callout-success text-center">
                                <strong>Automate Reinvest</strong>
                            </div>
                            <div class="row text-center"><strong>Enable Automatic reinvestment</strong><small>
                                    (instantly)
                                </small></div>
                            <div class="row text-center">
                                <input data-toggle="toggle" {{ $auto_invest ? 'checked':''  }} name="auto_reinvest_check"
                                       data-on="<i class='fa fa-check'></i> Enabled" data-off="<i class='fa fa-close'></i> Disabled" type="checkbox"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-offset-4 col-md-4 col-sm-offset-4 col-sm-4">
                            <br/>
                            <button type="submit" class="btn btn-primary btn-block">Save Changes</button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
@endsection