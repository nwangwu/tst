<?php

namespace App\Listeners;

use App\Services\BitcoinService;
use Illuminate\Auth\Events\Registered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegisterSuccess
{
    protected $bitcoin;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(BitcoinService $bitcoinService)
    {
        $this->bitcoin = $bitcoinService;
    }

    /**
     * Handle the event.
     *
     * @param  Registered  $event
     * @return void
     */
    public function handle(Registered $event)
    {

        //$this->bitcoin->createAddress($event->user);
    }
}
