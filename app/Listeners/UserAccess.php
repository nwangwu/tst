<?php

namespace App\Listeners;

use App\Events\UserAccess as UserAccessEvent;
use App\Services\Locale;
use App\Services\Status;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserAccess
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserAccess  $event
     * @return void
     */
    public function handle(UserAccessEvent $event)
    {
        Status::setStatus();

        Locale::setLocale();

    }
}
