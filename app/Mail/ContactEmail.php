<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactEmail extends Mailable
{
    use Queueable, SerializesModels;


    protected $message, $user, $email;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email,$user,$msg)
    {
        //
        $this->message = $msg;
        $this->user = $user;
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.contact')
            ->from($this->email,$this->user)
            ->subject('ContactUs Message')
            ->with(['messages' => $this->message]);
    }
}
