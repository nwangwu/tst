<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AdvertEmail extends Mailable
{
    use Queueable, SerializesModels;


    protected $user;
    protected $message;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $message)
    {
        //
        $this->user = $user;
        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.advert')
            ->from('no-reply@btcroyals.com',env('APP_NAME'))
            ->subject('IMPORTANT!!')
            ->with(['messages' => $this->message,'user' => $this->user]);
    }
}
