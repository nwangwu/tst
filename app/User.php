<?php

namespace App;

use App\Models\Account;
use App\Models\Affiliate;
use App\Models\Transaction;
use App\Notifications\ResetPassword;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Role;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    protected $dates =  ['deleted_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','role_id', 'email', 'password','facebook_id','parent','active',
        'confirmation_code','confirmed','bit_owner_address','bit_deposit_address'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * One to Many relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    /**
     * One to One Relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function account()
    {
        return $this->hasOne(Account::class);
    }

    public function affiliate()
    {
        return $this->hasMany(Affiliate::class);
    }

    /**
     *
     * @return mixed
     */

    public function getRoleStatus()
    {
       // return "guest";
        return $this->role->slug;//->role->slug;
    }


    public function hasReferralLink()
    {
        return $this->referral !== null;
    }

    public function isReferred()
    {
        return $this->parent !== 'admin';
    }

    public function transactions()
    {
        return $this->hasManyThrough(Transaction::class,Account::class);
    }
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }
}
