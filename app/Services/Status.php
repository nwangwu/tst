<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 5/1/2017
 * Time: 11:29 AM
 */

namespace App\Services;


class Status {

    /**
     * @param Illuminate\Auth\Events\Login $login
     * @return void
     */
    public static function setLoginStatus($login)
    {
        session(['login_status' => $login->user->getRoleStatus()]);
    }

    /**
     * Set the visitor user status
     */
    public static function setVisitorStatus()
    {
        session(['login_status' => 'visitor']);
    }


    public static function setStatus()
    {
        if(!session()->has('login_status')){
            session(['login_status' => auth()->check() ? auth()->user()->getRoleStatus() : 'visitor']);
        }
    }


    public static function isAdmin()
    {
        return session('login_status') === 'super_admin' || session('login_status') === 'admin';
    }

}