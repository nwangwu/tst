<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 5/1/2017
 * Time: 11:34 AM
 */

namespace App\Services;


use Symfony\Component\HttpFoundation\Request;

class Locale {

    /**
     * Set the locale
     *
     * @return void
     */

    public static function setLocale()
    {
        if(!session()->has('locale')){
            session()->put('locale',
                Request::getPreferredLanguage(config('app.languages')));
        }
        app()->setLocale(session('locale'));
    }
}