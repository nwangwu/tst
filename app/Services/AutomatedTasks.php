<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 5/9/2017
 * Time: 9:21 PM
 */

namespace App\Services;


use App\Repositories\TransactionRepository;

class AutomatedTasks {


    protected $trans_repo;

    function __construct(TransactionRepository $repository)
    {
        $this->trans_repo = $repository;
    }

    public function run_auto_withdraw()
    {
        $this->trans_repo->auto_withdraw();
    }

    public function run_auto_reinvest()
    {
        $this->trans_repo->auto_reinvest();
    }

    public function run_close_deposit()
    {
        $this->trans_repo->get_open_deposit_and_close();
    }

    public function run_earning_update()
    {
        $this->trans_repo->update_earning();
    }




}
