<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 5/2/2017
 * Time: 9:40 PM
 */

namespace App\Services;


use App\User;
use Blocktrail\SDK\BlocktrailSDK;
use Blocktrail\SDK\Wallet;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use Mockery\Exception;
use Psr\Http\Message\ResponseInterface;
use Blocktrail\SDK\Blocktrail;

class BitcoinService {


    protected  $client;
    protected $passcode;
    protected $wallet;

    function __construct()
    {
        $this->client = new BlocktrailSDK("6fc2685001ea62f942edd20c407f7e2f62aa8000", "6facfb56711146349787288a790b3539536c79fd", "BTC", true); //new Client(['base_uri' => 'http://localhost:3000/merchant/'.env('WALLET_GUID').'/']);
        $this->wallet = $this->client->initWallet("livialink","upjesus");
        //$this->client->
        //$this->passcode = env('WALLET_PASSCODE');
    }


    public function createAddress(User $user)
    {
        $address = $this->wallet->getNewAddress();
        $this->update_user_data($user,$address);
        /*
        $promise = $this->client->getAsync('new_address',
            ['query'=>['password' => $this->passcode,'label' => $user->name]]);

        $promise->then(function(ResponseInterface $res) use ($user){
            $result = json_decode($res->getBody()->getContents());
            $this->update_user_data($user,$result->address);
        })->wait();*/
    }

    private function update_user_data(User $user,$address)
    {
        if(!is_null($address)) {
            $user->bit_deposit_address = $address;
            $user->save();
        }else{
            $this->createAddress($user);
        }
    }

    public function get_address_total_received($address)
    {

        $result = $this->client->address($address);
        return  $result["received"];
        /*
        $promise = $this->client->getAsync('address_balance',
            ['query'=>['password'=>$this->passcode,'address'=>$address]]);

        return $promise->then(function(ResponseInterface $res) use($address){
            $result = json_decode($res->getBody()->getContents());
            return $result->total_received;
        })->wait();*/
    }

    public function get_wallet_balance($wallet = null)
    {
        list($confirmedWallet,$unconfirmedWallet) = $this->wallet->getBalance();
        return $confirmedWallet;
        /*
        $promise = $this->client->getAsync('balance',
            ['query'=>['password'=>$this->passcode]]);

        return $promise->then(function(ResponseInterface $res){
            $result = json_decode($res->getBody()->getContents());
            return $result->balance;
        })->wait();
        */
    }

    public function send_one_payment($to_address,$amount_satoshi,$from_address = null)
    {
        $txid = $this->wallet->pay(array($to_address=>$amount_satoshi),null,false,true,Wallet::FEE_STRATEGY_BASE_FEE);

        return ["message"=>"Successful","tx_hash" =>$txid,"response_code"=>200];

        /*
        $from='';

        if(!is_null($from_address)){
            $from = "'from' => '".$from_address."'";

            $promise = $this->client->getAsync('payment',
                ['query'=>['password'=>$this->passcode,'to'=>$to_address,'amount'=>$amount_satoshi,'from'=>$from_address]]);
        }else{
            $promise = $this->client->getAsync('payment',
                ['query'=>['password'=>$this->passcode,'to'=>$to_address,'amount'=>$amount_satoshi]]);

        }

        return $promise->then(function(ResponseInterface $res){
            $result = json_decode($res->getBody()->getContents());
            return  ['message'=>$result->message,'tx_hash'=>$result->tx_hash,'response_code'=>$res->getStatusCode()];
        })->wait();
        */
    }

    public function send_many_payment($receipients =[],$from_address = null)
    {

        $txid = $this->wallet->pay($receipients,null,false,true,Wallet::FEE_STRATEGY_BASE_FEE);
        return ["message"=>"Successful","tx_hash" =>$txid,"response_code"=>200];
        /*
        $to_addresses = json_encode($receipients);

        $from='';

        if(!is_null($from_address)){
            $from = "'from' => '".$from_address."'";
            $promise = $this->client->getAsync('sendmany',
                ['query'=>['password'=>$this->passcode,'recipients' => $to_addresses,'from'=>$from_address]]);
        }else{
            $promise = $this->client->getAsync('sendmany',
                ['query'=>['password'=>$this->passcode,'recipients' => $to_addresses]]);
        }

        return $promise->then(function(ResponseInterface $res){
            $result = json_decode($res->getBody()->getContents());
            return ['message'=> $result->message,'tx_hash'=> $result->tx_hash,'response_code'=>$res->getStatusCode()];
        })->wait();
        */
    }

    public function get_address_list()
    {
        return $this->wallet->transactions();

        /*
        $promise = $this->client->getAsync('list',['query'=>['password'=>$this->passcode]]);
        return $promise->then(function(ResponseInterface $res){
            $result = json_decode($res->getBody()->getContents(),true);
            return $result;
        })->wait();*/
    }

    public function get_confirmation($address)
    {

        try {
            $client = new Client(['base_uri' => 'https://blockchain.info/']);
            $promise = $client->getAsync('/multiaddr', ['query' => ['cors' => true, 'active' => $address]]);
            return $promise->then(function (ResponseInterface $res) {
                $result = json_decode($res->getBody()->getContents(), true);
                if(isset($result['txs'][0]['block_height'])){
                $confirmation = $result['info']['latest_block']['height'] - $result['txs'][0]['block_height'] + 1;
                return ['confirmation' => $confirmation, 'total_received' => $result['addresses'][0]['total_received'],
                    'txs_hash' => $result['txs'][0]['hash']];
                }else{
                    $result['confirmation'] = 0;
                    return $result;
                }
            })->wait();
        }catch (Exception $ex){
            error_log("Error from BitcoinService Confirmation",0);
            $result['confirmation'] = 0;
            return $result;
        }
    }
}