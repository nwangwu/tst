<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\AutomatedTasks;

class AutomatedtaskCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'automatedtask:hourly';

    protected $automate;
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Automates hourly task';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(AutomatedTasks $automata)
    {
        parent::__construct();

        $this->automate = $automata;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->automate->run_earning_update();
	    $this->automate->run_auto_reinvest();
	    $this->automate->run_close_deposit();
    }
}
