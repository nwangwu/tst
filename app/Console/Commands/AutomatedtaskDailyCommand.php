<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\AutomatedTasks;
class AutomatedtaskDailyCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'automatedtask:daily';
    
    protected $automate;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Daily running autmated tasks. auto_withdraw';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(AutomatedTasks $automata)
    {
        parent::__construct();
	    $this->automate  = $automata;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
	    $this->automate->run_auto_withdraw();
    }
}
