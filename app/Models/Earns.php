<?php

namespace App\Models;

use app\Traits\DatePresenter;
use Illuminate\Database\Eloquent\Model;

class Earns extends Model
{
    use DatePresenter;


    public function account()
    {
        return $this->belongsTo(Account::class);
    }
}
