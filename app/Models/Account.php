<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\DatePresenter;
use Illuminate\Foundation\Auth\User;

class Account extends Model
{
    use DatePresenter;
    //


    protected $guarded = [];
    /**
     * One to One Relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
       return $this->belongsTo(User::class);
    }

    /**
     * One to Many Relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transaction()
    {
        return $this->hasMany(Transaction::class);
    }

    /**
     * One to  Many Relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function earns()
    {
        return $this->hasMany(Earns::class);
    }
}
