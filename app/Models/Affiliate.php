<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Affiliate extends Model
{


    public function user()
    {
        return $this->belongsTo(User::class,'parent_id');
    }

}
