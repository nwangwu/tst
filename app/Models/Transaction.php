<?php

namespace App\Models;

use App\Traits\DatePresenter;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use DatePresenter;

    //


    public function account()
    {
        return $this->belongsTo(Account::class);
    }

}
