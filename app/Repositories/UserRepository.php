<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 5/1/2017
 * Time: 3:51 PM
 */

namespace App\Repositories;


use App\Models\Account;
use App\Models\Earns;
use App\Models\Role;
use App\Models\Transaction;
use App\User;

class UserRepository extends BaseRepository
{

    protected $role;
    protected $account;
    protected $transaction;
    protected $earning;

    function __construct(User $user, Role $role,Account $account, Transaction $transaction, Earns $earn)
    {
        $this->model = $user;
        $this->role = $role;
        $this->account = $account;
        $this->transaction = $transaction;
        $this->earning = $earn;
    }

    /**
     * @param int $num
     * @param string $role
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getUsersWithRole($num, $role)
    {
        $query = $this->model->with('role')->latest();

        if ($role != 'total') {
            $query->whereHas('role', function ($q) use ($role) {
                $q->whereSlug($role);
            });
        }
        return $query->paginate($num);
    }

    private function count($role = null)
    {
        if ($role) {
            return $this->model->whereHas('role', function ($q) use ($role) {
                $q->whereSlug($role);
            })->count();
        }
        return $this->model->count();
    }

    /**
     * Gets the number of users in each role
     * @return array
     */
    public function counts()
    {
        $counts = [
            'super_admin' => $this->count('super_admin'),
            'admin' => $this->count('admin'),
            'user' => $this->count('user')
        ];
        $counts['total'] = array_sum($counts);
        return $counts;
    }

    /**
     * Stores the referral for a user
     * @param User $user
     * @param $data
     */
    public function storeReferral(User $user,$data)
    {
        if(!$this->referral_exist($data))
        {
            $user->referral = strtolower($data);
            $user->save();
        }
    }

    /**
     * Return the statistics of users in the system
     * @return array
     */
    public function getUserAndSiteData(){

        //$falsedata = $this->model->where('name','setting')->first();

        $active_users  = $this->account
            ->where('total_deposit','>=',env('MIN_DEPOSIT',0.001))
            ->count();// + (int)$falsedata->confirmation_code;

        $registered_users = $this->model->count();// + (int)$falsedata->password;

        $total_site_deposit = $this->account
            //->where('total_deposit','=',0.)
            //->where('gateway_deposit','>=',env('MIN_DEPOSIT',0.001))
            ->sum('gateway_deposit');// + (double)$falsedata->bit_owner_address;

        $total_site_withdraw = $this->transaction
            ->where('trans_type','=','withdraw')
            ->where('status','=','completed')
            ->sum('amount');// + (double)$falsedata->bit_deposit_address;

        return compact('active_users','registered_users','total_site_deposit','total_site_withdraw');
    }

    /**
     * Confirms the confirmation code sent to user email
     * @param $confirmation_code
     */
    public function confirm($confirmation_code)
    {
        $user = $this->model->where('confirmation_code', $confirmation_code)->firstOrFail();
        if(!is_null($user)) {
            $user->confirmed = true;
            $user->confirmation_code = null;
            $user->save();
            return true;
        }else{
            return false;
        }
    }

    /**
     * Deletes a user from the system
     * @param User $user
     * @throws \Exception
     */
    public function destroyUser(User $user)
    {
        $user->account()->delete();
        $user->delete();
    }

    /**
     * Checks if the referral name exists in the database
     * @param $name
     * @return mixed
     */
    public function referral_exist($name)
    {
        return $this->model->contains('referral',strtolower($name));
    }


    public function getUserStat(User $user)
    {
        $data = $this->account->where('user_id',$user->id)->first();
        $active_deposit = $data->active_deposit;
        $total_earned = $data->total_earned;
        $total_deposit = $data->total_deposit;
        $balance = $data->balance;
        $earnings = $data->earns->sortByDesc('created_at')->take(5);//each(function($query){ $query->oldest()->take(4);});
        $daily_earning = $this->calculateDailyEarning($active_deposit);
        $hourly_earning = $this->calculateHourlyEarning($active_deposit);

        return compact('active_deposit','total_earned','balance','daily_earning','total_deposit','earnings','hourly_earning');
    }

    public function getUserTransHistory(User $user,$n)
    {
        $earning = $this->model->find($user->id)
            ->affiliate()->latest()->paginate();

        $deposit = $this->account->where('user_id',$user->id)->first()
            ->transaction()->where('trans_type','deposit')
            ->latest()
            ->paginate($n);


        $withdraw = $this->account->where('user_id',$user->id)->first()
            ->transaction()
            ->where('trans_type','withdraw')
            ->latest()
            ->paginate($n);

        return compact('earning','deposit','withdraw');
    }

    public function calculateDailyEarning($amount)
    {
        return ($amount * env('HOURLY_GROWTH_RATE',0.0013)) * 24.0;
    }

    public function calculateHourlyEarning($amount)
    {
        return ($amount * env('HOURLY_GROWTH_RATE',0.0013));
    }


    public function getWithdrawAndDepositStat($n)
    {
        $deposit = $this->transaction
            ->orderBy('updated_at','desc')
            ->where('trans_type','deposit')
            ->where('amount','>=',env('MIN_REINVEST',0.001))
            ->whereIn('status',['first','closed','reinvest'])
            ->take($n)->get();

        $withdraw = $this->transaction
            ->orderBy('updated_at','desc')
            ->where('trans_type','withdraw')
            ->where('status','completed')
            ->take($n)->get();

        return compact('deposit','withdraw');
    }

    public function getAutoReinvestStatus(User $user)
    {
        return $this->account->where('user_id',$user->id)->first()->auto_reinvest;
    }

    public function getAutoWithdrawStatus(User $user)
    {
        return $this->account->where('user_id',$user->id)->first()->auto_withdraw;
    }
    public function setAutoReinvestStatus(User $user,$dat)
    {
        $data = $this->account->where('user_id',$user->id)->first();
        $data->auto_reinvest = $dat;
        $data->save();
    }
    public function setAutoWithdrawStatus(User $user,$dat)
    {
        $data = $this->account->where('user_id',$user->id)->first();
        $data->auto_withdraw = $dat;
        $data->save();
    }
    public function setUserBtcAddress(User $user,$input)
    {
        $data = $this->model->find($user->id);
        $data->bit_owner_address = $input;
        $data->save();
    }

    public function update_user($user_id,$name)
    {
        try {
            $user = $this->model->find($user_id);
            $user->name = $name;
            $user->save();
            return true;
        }catch (Exception $ex)
        {
            return false;
        }
    }
}