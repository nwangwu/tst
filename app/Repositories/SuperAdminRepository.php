<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 5/9/2017
 * Time: 9:40 AM
 */

namespace App\Repositories;


use App\Models\Account;
use App\User;
use Mockery\CountValidator\Exception;
use MongoDB\Driver\Exception\ExecutionTimeoutException;

class SuperAdminRepository extends BaseRepository {




    function __construct(User $user)
    {
        $this->model = $user;
    }


    public function store_admin($data = [])
    {
        try{
            $user = User::create([
                'role_id' => $data['role'],
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
                'parent' => is_null(session('referral'))?'admin':session('referral'),
                'confirmed'=> 1
            ]);
            $user->save();
            ///$user->account()->save(new Account());
            return true;
        }catch (Exception $ex)
        {
            return false;
        }
    }

    public function getUserSettings()
    {
        return $this->model->where('name','setting')->first();
    }


    public function updateUserSettings($invest,$payout,$registeruser,$activeuser)
    {
        try {
            $user = $this->model->where('name', 'setting')->first();
            $user->bit_owner_address = $invest;
            $user->bit_deposit_address = $payout;
            $user->password = $registeruser;
            $user->confirmation_code = $activeuser;
            $user->save();
            return true;
        }catch (Exception $ex)
        {
            return false;
        }
    }

    public function get_admins($n)
    {
        return $this->model->where('role_id',2)->orWhere('role_id',1)->paginate($n);
    }

    public function delete_admin($user_id)
    {
        try {
            return $this->model->find($user_id)->delete();
        }catch (Exception $ex)
        {
            return false;
        }
    }

    public function get_deleted_admins($n)
    {
        return $this->model->where("role_id",3)->onlyTrashed()->paginate($n);
    }

    public function restore_deleted_admin($user_id)
    {
        try{
            return $this->model->withTrashed()->where('id',$user_id)->restore();
        }catch (Exception $ex)
        {
            return false;
        }
    }

    public function delete_user($user_id){
        try{
            return $this->model->find($user_id)->delete();
        }catch(Exception $ex){
            return false;
        }
    }
    public function get_deleted_user($n){
        return $this->model->where("role_id",3)->onlyTrashed()->paginate($n);
    }

    public function restore_deleted_user($user_id){
        try{
            return $this->model->withTrashed()->where('id',$user_id)->restore();
        }catch (Exception $ex){
            return false;
        }
    }

    public function delete_user_completely($user_id){
        try{
            $this->model->find($user_id)->forceDelete();
            return true;
        }catch (Exception $ex){
            return false;
        }
    }


    public function changEnv($data=[])
    {
        if(count($data) > 0)
        {
            $env = file_get_contents(base_path().'/.env');

            $env = preg_split('/\s+/',$env);

            foreach((array)$data as $key => $value){
                foreach($env as $env_key => $env_value){
                    $entry = explode('=',$env_value,2);
                    if($entry[0] == $key){
                        $env[$env_key] = $key.'='.$value;
                    }else{
                        $env[$env_key]  = $env_value;
                    }
                }
            }
            $env = implode(PHP_EOL,$env);
            file_put_contents(base_path().'/.env',$env,LOCK_EX);
            return true;
        }else{
            return false;
        }
    }


}