<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 5/1/2017
 * Time: 3:51 PM
 */

namespace App\Repositories;


abstract class BaseRepository {


    protected $model;

    protected function destroy($id)
    {
        $this->getById($id)->delete();
    }

    public function getById($id)
    {
        return $this->model->findOrFail($id);
    }

}