<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 5/8/2017
 * Time: 8:24 PM
 */

namespace App\Repositories;


use App\Models\Account;
use App\Models\Transaction;
use App\Services\BitcoinService;
use App\User;
use Illuminate\Support\Facades\DB;

class AdminRepository extends BaseRepository{

    protected $trans_repo;
    protected $acc_repo;
    protected $with_repo;
    protected $btcservice;

    function __construct(User $user,Transaction $transaction,Account $account, WithdrawRepository $repository,BitcoinService $service)
    {

        $this->model  = $user;
        $this->trans_repo =$transaction;
        $this->acc_repo = $account;
        $this->with_repo = $repository;
        $this->btcservice = $service;
    }

    public function get_users($n)
    {
        return $this->model->account()->paginate($n);
    }

    public function get_users_data($n)
    {
        return $this->model->where("role_id",3)->paginate($n);
    }


    public function cancel_withdrawal($account_id)
    {
       return DB::transaction(function() use ($account_id){
            $trans = $this->acc_repo->
            where('id',$account_id)->first()
                ->transaction()
                ->where(['trans_type' => 'withdraw','status' =>'pending'])
                ->firstOrFail();
            $amount = $trans->amount;
            $trans->status = 'cancelled';
            $trans->save();

            $account = $this->acc_repo->where('id',$account_id)->first();
            $new_active_deposit = $account->active_deposit + $amount;
            $new_balance = $account->balance + $amount;
            $account->active_deposit = $new_active_deposit;
            $account->balance = $new_balance;
            $account->save();
            return true;
        });
       return false;
    }


    public function get_withdrawal_request($n)
    {
        return $this->trans_repo->where('trans_type','withdraw')
            ->where('status','pending')
            ->paginate($n);
    }

    public function send_payments($new_array,$to_pay_users)
    {

        $response = $this->btcservice->send_many_payment($new_array);
        if($response['response_code']== 200){
            foreach($to_pay_users as $user){
                $this->with_repo->update_withdraw_complete($user);
            }
          return $response;
        }
        return false;
    }

    public function send_single_payment($to,$amount,$user_id)
    {
        $response = $this->btcservice->send_one_payment($to,$amount);
        if($response['response_code'] == 200){
            $this->with_repo->update_withdraw_complete($user_id);
            return $response;
        }
        return false;
    }

    public function get_wallet_balance()
    {
        return $this->btcservice->get_wallet_balance();
    }

    public function get_address_list()
    {
        return $this->btcservice->get_address_list()['addresses'];
    }

    public function get_address_balance($address)
    {
        return $this->btcservice->get_address_total_received($address);
    }

    public function store_user($data = [])
    {
        try{
            $user = User::create([
                'role_id' => $data['role'],
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
                'parent' => is_null(session('referral'))?'admin':session('referral'),
                'confirmed'=> 1
            ]);
            $user->account()->save(new Account());
            $user->save();
            return true;
        }catch (\Exception $ex)
        {
            return false;
        }
    }

    public function get_false_users($n)
    {
        return  $this->model->where('role_id',4)->paginate($n);
    }

}