<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 5/3/2017
 * Time: 11:10 AM
 */

namespace App\Repositories;


use App\Models\Account;
use App\Models\Transaction;
use App\Services\BitcoinService;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class WithdrawRepository extends BaseRepository
{


    protected $transaction;
    protected $btcservice;

    function __construct(Account $account, Transaction $transaction, BitcoinService $btcServ)
    {
        //$this->admin_repo = $adminrepo;
        $this->model = $account;
        $this->transaction = $transaction;
        $this->btcservice = $btcServ;
    }

    public function withdraw(User $user, $data = [])
    {
        $amount = $data['amount'];
        $account = $this->model->where('user_id',$user->id)->first();
        return DB::transaction(function () use ($user, $amount, $data, $account) {

            if ($amount >= env('MIN_WITHDRAW', 0.001) && $account->balance >= $amount
                && !$this->anyPendingWithdrawal($user)
            ) {

                //$new_active_deposit = $account->active_deposit - $amount;
                $new_balance = $account->balance - $amount;
                //$account->active_deposit = $new_active_deposit;
                $account->balance = $new_balance;
                $account->save();

                $this->transaction->trans_type = 'withdraw';
                $this->transaction->amount = $amount;
                $this->transaction->status = 'pending';
                $this->transaction->bit_user_address = $data['bit_address'];
                $account->transaction()->save($this->transaction);
                return true;
            } else {
                return false;
            }
        });
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     * Withdrawal from the gateway
     */
    public function withdraw_from_gateway($request)
    {
        $arr = explode('|', $request);
        $user_id = $arr[0];
        $user_bit_address = $arr[1];
        $amount = (int)($arr[2] * env('SATOSHI', 100000000));

        $result = $this->send_single_payment($user_bit_address, $amount, $user_id);
        if ($result != false) {
            return true;
            /*
            $request->session()->flash('info', ['type' => 'success',
                'message' => $result['message'] . '| Txs_hash:' . $result['tx_hash'] . ' | Notice: ' . $result['notice']]);
            return back();*/
        }
        return false;
        //return back()->with('info', ['type' => 'danger', 'message' => 'Transaction failed to complete']);
    }

    private function send_single_payment($to,$amount,$user_id)
    {
        $response = $this->btcservice->send_one_payment($to,$amount);
        if($response['response_code'] == 200){
            $this->update_withdraw_complete($user_id);
            return $response;
        }
        return false;
    }


    public function withdraw_active_deposit(User $user, $data = [])
    {
        $amount = $data['amount'];
        $account = $this->model->where('user_id',$user->id)->first();
        return DB::transaction(function() use ($user,$amount,$data,$account){
            if($amount >= env('MIN_WITHDRAW',0.001) && $account->active_deposit >= $amount
                && !$this->anyPendingWithdrawal($user)){

                $new_active_deposit = $account->active_deposit - $amount;

                $account->active_deposit = $new_active_deposit;
                $account->save();

                $this->transaction->trans_type = 'withdraw';
                $this->transaction->amount = $amount;
                $this->transaction->status = 'pending';
                $this->transaction->bit_user_address = $data['bit_address'];
                $account->transaction()->save($this->transaction);
                return true;
            }else{
                return false;
            }
        });
    }

    public function isTodayWithdrawalAllowed(User $user)
    {
        return   $this->model->where('user_id',$user->id)
                ->first()
                    ->transaction()
                    ->where(['trans_type' => 'withdraw'])
                    ->whereDay('updated_at',Carbon::now()->day)
                    ->count() === 0;
    }

    public function isMonthWithdrawalAllowed(User $user)
    {
        $created_at = $this->model->where('user_id',$user->id)
            ->first()->created_at;
        $created = new Carbon($created_at);
        $now = Carbon::now();
        return $created->diff($now)->days >= 28 ? true : false;
    }
    public function monthWithdrawalTimeRemaining(User $user)
    {
        $created_at = $this->model->where('user_id',$user->id)
            ->first()->created_at;
        $created = new Carbon($created_at);
        $now = Carbon::now();
        return (28 - $now->diffInDays($created)).' days remaining';

    }

    public function isInitialWithdrawalAllowed(User $user)
    {
        $create_at = $this->model->where('user_id',$user->id)
            ->first()->created_at;
        $created = new Carbon($create_at);
        $now = Carbon::now();
       // return $created->diff($now)->days >= 2 ? true : false;
	return true;
    }

    public function getUserBitAddress($id){
        return $this->model->where('user_id',$id)->first()->user()->bit_owner_address;
    }


    public function anyPendingWithdrawal(User $user){
        $account = $this->getPendingWithdrawal($user);
        if($account->count() > 0)
            return true;
        else
            return false;
    }

    public function getPendingWithdrawal(User $user){
        $account = $this->model->where('user_id',$user->id)->first();
        return $account->transaction()->where(['trans_type'=>'withdraw','status'=>'pending'])->get();
           /* ->whereHas('transaction', function($query){
            $query->where('status','=','pending')->where('trans_type','=','withdraw');
            })->get();*/

    }

    public function cancel_withdrawal(User $user)
    {
        DB::transaction(function() use ($user){
            $trans = $this->model->
            where('user_id',$user->id)->first()
                ->transaction()
                ->where(['trans_type' => 'withdraw','status' =>'pending'])
                ->firstOrFail();
            $amount = $trans->amount;
            $trans->status = 'cancelled';
            $trans->save();

            $account = $this->model->where('user_id',$user->id)->first();
            $new_active_deposit = $account->active_deposit + $amount;
            $new_balance = $account->balance + $amount;
            $account->active_deposit = $new_active_deposit;
            $account->balance = $new_balance;
            $account->save();

        });
    }

    public function update_withdraw_complete($user_id)
    {
        $trans = $this->model->where('user_id',$user_id)->first()
            ->transaction()
            ->where(['trans_type' => 'withdraw','status'=>'pending'])
            ->firstOrFail();
        $trans->status = 'completed';
        $trans->save();
    }

    public function withdraw_deposit(DepositRepository $repository,User $user,$data = [])
    {
        $depo = $repository->getLatestDeposit($user);
        $deposit = $depo->find($data['deposit_id']);
        return DB::transaction(function()use ($deposit,$data,$user){
            if($this->withdraw($user,['amount'=>$deposit->amount,'bit_address' => $data['bit_address']])!==false) {

                $deposit_to_withdraw = $this->model->where('user_id',$user->id)->first()
                    ->transaction()
                    ->findOrFail($data['deposit_id']);

                $deposit_to_withdraw->status = 'cancelled';
                $deposit_to_withdraw->save();
                return true;
            }else{
                return  false;
            }
        });
    }
}
