<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 5/4/2017
 * Time: 1:43 AM
 */

namespace App\Repositories;


use App\Models\Account;
use App\Models\Affiliate;
use App\Models\Transaction;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Mockery\CountValidator\Exception;

class AffiliateRepository extends BaseRepository {

    protected $affiliate;

    function __construct(Account $account, Affiliate $affiliate)
    {
        $this->model = $account;
        $this->affiliate = $affiliate;
    }

    public function getParent($parent)
    {
        return User::where('name',$parent)->firstOrFail();
        //return User::find($user->id);
    }

    public function isUserCreditedBefore($parent_id,$child_id)
    {
        $data = $this->affiliate->where('user_id',$parent_id)
            ->where('child_id',$child_id)
            ->first();
        return is_null($data) ? false : ($data->count() > 0 ? true : false);
    }

    public function update_affiliate(User $user)
    {
        if($user->isReferred())
        {
            $data = $this->model->where('user_id',$user->id)->first()->transaction()
                ->where('trans_type', '=', 'deposit')
                ->where('status', '=','first')
                ->firstOrFail();

            $parent = $this->getParent($user->parent);
            if(!$this->isUserCreditedBefore($parent->id,$user->id)) {
                if (!is_null($data) && $data->count() > 0) {
                    if (Affiliate::where('child_id', $user->id)->count() == 0) {

                        $this->affiliate->user_id = $parent->id;
                        $this->affiliate->child_id = $user->id;
                        $this->affiliate->real_deposit = $data->amount;
                        $this->affiliate->commission = ($data->amount * env('REF_COMMISSION', 0.01));
                        $this->affiliate->member_btc_address = $data->bit_address;
                        $this->affiliate->member_registered = $user->created_at;
                        $this->affiliate->member_last_login = $user->last_login;

                        $parent->affiliate()->save($this->affiliate);
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        }
    }

    public function get_children (User $user,$n)
    {
        return $user->where('parent','admin')->whereHas('transactions',function($query){
            $query->where('trans_type','deposit')
                ->where('status','first');
        })->paginate($n);
    }

    public  function  reinvest_deposit(User $user, $child_id,$amount)
    {
        DB::beginTransaction();
        try {
        //DB::transaction(function() use ($user,$child_id,$amount){

                $child = $user->affiliate()->where('child_id', $child_id)->firstOrFail();
                $child->reinvested = true;
                $child->save();

                $account = $this->model->where('user_id',$user->id)->first();

                $total = $amount + $account->total_deposit;
                $active = $amount + $account->active_deposit;

                $account->total_deposit = $total;
                $account->active_deposit = $active;
                $account->save();

                $transaction = new Transaction();
                $transaction->trans_type = 'deposit';
                $transaction->status = 'affiliate';
                $transaction->amount = $amount;

                $account->transaction()->save($transaction);
                DB::commit();

                return true;
            }catch (Exception $ex)
            {
                DB::rollback();
                return false;
            }
        //});
    }

    public function get_children_active(User $user,$n)
    {
        return $user->affiliate()->paginate($n);
    }


}