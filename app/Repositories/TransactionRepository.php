<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 5/2/2017
 * Time: 9:56 PM
 */

namespace App\Repositories;

use Blocktrail\SDK\BlocktrailSDK;
use App\Models\Account;
use App\Models\Earns;
use App\Models\Transaction;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Mockery\CountValidator\Exception;
use PhpParser\Node\Expr\Cast\Double;

class TransactionRepository extends BaseRepository {

    protected $transaction;


    function __construct(Account $account, Transaction $transaction)
    {
        $this->model = $account;
        $this->transaction = $transaction;
    }

    public function auto_reinvest()
    {

        $users = $this->get_automated_reinvest_accounts();
        DB::transaction(function() use ($users){
            $users->each(function($data){
                $balance = $data->balance;
                $active = $data->active_deposit + $data->balance;
                $total = $data->total_deposit + $data->balance;
                $data->total_deposit = $total;
                $data->active_deposit = $active;
                $data->balance = 0;
                $data->save();


                $this->transaction->trans_type = 'deposit';
                $this->transaction->status = 'reinvest';
                $this->transaction->amount = $balance;

                $data->transaction()->save($this->transaction);
            });

        });

    }

    public function auto_withdraw()
    {
        $accounts = $this->get_automate_withdraw_accounts();
        $accounts = $accounts->filter(function($data){
            return !($this->anyPendingWithdrawal($data->id) > 0) ? true : false;
        });

        DB::transaction(function() use ($accounts){
            $accounts->each(function($data){
                $balance = $data->balance;
                $new_active_deposit = $data->active_deposit - $balance;
                $new_balance = $data->balance - $balance;

                $data->active_deposit = $new_active_deposit;
                $data->balance = $new_balance;
                $data->save();

                $this->transaction->trans_type = 'withdraw';
                $this->transaction->amount = $balance;
                $this->transaction->status = 'pending';
                $this->transaction->bit_user_address = $data->user->bit_owner_address;
                $data->transaction()->save($this->transaction);
            });
        });
    }

    public function anyPendingWithdrawal($id)
    {
        return $this->model->find($id)->whereHas('transaction', function($query){
            $query->where('status','pending'); })->get()->count();
    }


    public function get_automated_reinvest_accounts()
    {
        return $this->model->where('auto_reinvest',true)
            ->where('balance','>=',(int)BlocktrailSDK::toSatoshi(env('MIN_REINVEST',0.0005)))->get();
    }

    public function get_automate_withdraw_accounts()
    {
        return $this->model->where('auto_withdraw',true)
            ->where('balance','>=',(int)BlocktrailSDK::toSatoshi(env('MIN_WITHDRAW',0.001)))->get();
    }

    public function getAccountWithTransactions($id)
    {
        return $this->model->with('transactions')->firstOrFail($id);

    }

    public function get_open_deposit_and_close()
    {
        DB::transaction(function() {

            $this->transaction->orderBy('id')->chunk(env('MAX_CHUNK',5000),function($users){
                $users->where('trans_type','deposit')->where('status','open')->each(function($uu){
                    $uu->status ='closed';
                    $uu->account->can_with_deposit = false;
                    $uu->push();
                });
            });
            /*
            $this->transaction->where('trans_type', 'deposit')
                ->where('status', 'open')->orderBy('id')->chunk(1000, function ($users) {
                    foreach ($users as $uu) {
                        $uu->status = 'closed';
                        $uu->account->can_with_deposit = false;
                        $uu->push();
                    }
                });
           */
        });
    }

    private function calculate_hourly_earning($amount){
        return ($amount * env('HOURLY_GROWTH_RATE',0.05));
    }

    public function update_earning()
    {
        DB::transaction(function(){
            $this->model->orderBy('id')->chunk(env('MAX_CHUNK',5000),function($users){
                $users->where('active_deposit','>=',(int)BlocktrailSDK::toSatoshi(env('MIN_DEPOSIT',0.001)))->each(function($uu){
                    $earnings = (int)BlocktrailSDK::toSatoshi($this->calculate_hourly_earning($uu->active_deposit));
                    $new_balance = $uu->balance + $earnings;
                    $new_total_earned = $uu->total_earned + $earnings;
                    $earn = new Earns();
                    $earn->amount = $earnings;

                    $uu->total_earned = $new_total_earned;
                    $uu->balance = $new_balance;
                    $uu->save();
                    $uu->earns()->save($earn);
            });
            });
        });
    }

    public function add_false_transaction($user_id,$data = [])
    {
        try {
            $account = $this->model->where('user_id', $user_id)->first();

            $this->transaction->trans_type = $data['trans_type'];
            $this->transaction->amount = $data['amount'];
            $this->transaction->status = $data['status'];
            $account->transaction()->save($this->transaction);
            return true;
        }catch (Exception $ex)
        {
            return false;
        }
    }

    public function view_false_transactions($user_id)
    {
        $account = $this->model->where('user_id',$user_id)->first();
        return $account->transaction()->get();
        /*$users = User::where('role_id',4)->get();
        $trans_arr = [];
        $users->each(function($acc){
            $acc->each(function($data){

            });
        });*/
    }
    public function update_false_transaction($trans_id,$data=[])
    {
        try{
            $trans = $this->transaction->find($trans_id);
            $trans->trans_type = $data['trans_type'];
            $trans->amount = $data['amount'];
            $trans->updated_at = $data['updated_at'];
            $trans->save();

            return true;
        }catch (Exception $ex)
        {
            return false;
        }
    }

    public function get_transaction($trans_id)
    {
        return $this->transaction->find($trans_id);
    }

}
