<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 5/4/2017
 * Time: 8:55 AM
 */

namespace App\Repositories;


use App\Models\Account;
use App\Models\Transaction;
use App\User;

class LearderbaordRepository extends BaseRepository{


    protected $account;
    protected $transaction;

    function __construct(User $user, Account $account, Transaction $transaction)
    {
        $this->model = $user;
        $this->account = $account;
        $this->transaction = $transaction;
    }


    public function getTopWithdrawals($n)
    {
        return $this->transaction
            ->orderBy('amount','desc')
            ->where('trans_type','withdraw')
            ->where('status','completed')
            ->paginate($n);
    }

    public function getTopDeposits($n)
    {
        return $this->transaction
            ->orderBy('amount','desc')
            ->where('trans_type','deposit')
            ->whereIn('status',['first','closed','reinvest','affiliate'])
            ->paginate($n);

    }

    public function getTopMembers($n)
    {
        return $this->account
            ->where('total_deposit','>=',env('MIN_DEPOSIT',0.001))
            ->orderBy('total_deposit','desc')
            ->paginate($n);
    }
}
