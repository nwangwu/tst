<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 5/3/2017
 * Time: 2:32 PM
 */

namespace App\Repositories;


use App\Models\Account;
use App\Models\Transaction;
use App\User;
use Illuminate\Support\Facades\DB;

class DepositRepository extends BaseRepository{

    protected $transaction;

    function __construct(Account $account, Transaction $transaction)
    {
        $this->model = $account;
        $this->transaction = $transaction;
    }

    public function deposit(User $user,$amount,$data = [])
    {
        return
            DB::transaction(function() use ($amount,$user,$data){

            $account = $this->model->where('user_id',$user->id)->first();
            if(($amount - $account->gateway_deposit) >= env('MIN_WITHDRAW',0.001)){
                $old_total_deposit = $account->total_deposit;
                $surplus = $amount - $account->total_deposit;
                $active = $surplus + $account->active_deposit;
                $total = $surplus + $account->total_deposit;
                $gateway = $surplus + $account->gateway_deposit;
                $account->total_deposit = $total;
                $account->active_deposit = $active;// + $account->active_deposit);
                $account->gateway_deposit = $gateway;
                $account->save();

                $this->transaction->trans_type = 'deposit';
                if($old_total_deposit == 0.0){
                    $this->transaction->status = 'first';
                }else{
                    $this->transaction->status = 'open';
                }
                $this->transaction->amount = $surplus;
                $this->transaction->bit_address = $data['deposit_address'];
                $this->transaction->with_trans_id = $data['txs_hash'];

                $account->transaction()->save($this->transaction);
                return true;
            }else{
                return false;
            }
        });
    }

    public function getLatestDeposit(User $user)
    {
        return $this->model
                    ->where('user_id',$user->id)->first()
                    ->transaction()
                    ->where(['trans_type' => 'deposit','status' => 'open'])
                    ->latest()->get();
    }

    public  function reinvent(User $user,$amount)
    {
        $account = $this->model->where('user_id',$user->id)->first();
        DB::transaction(function() use ($account,$amount){
            if($account->balance >= $amount && $amount >= env('MIN_REINVEST',0.0005)){
                //$surplus = $amount - $account->total_deposit;
                $active = $amount + $account->active_deposit;
                $total = $amount + $account->total_deposit;
                $new_balance = $account->balance - $amount;

                $account->total_deposit = $total;
                $account->active_deposit = $active;// + $account->active_deposit);
                $account->balance = $new_balance;
                $account->save();

                $this->transaction->trans_type = 'deposit';
                $this->transaction->status = 'reinvest';
                $this->transaction->amount = $amount;

                $account->transaction()->save($this->transaction);
            }
        });
    }

    public function getBalance(User $user)
    {
        return $this->model->where('user_id',$user->id)->first()->balance;
    }

}