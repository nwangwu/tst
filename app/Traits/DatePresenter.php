<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 5/1/2017
 * Time: 10:34 AM
 */

namespace app\Traits;

use Carbon\Carbon;

trait DatePresenter {


    public function getCreatedAtAttribute($date)
    {
        return $this->getDateTimeFormatted($date);
    }

    public  function getUpdatedAtAttribute($date)
    {
        return $this->getDateTimeFormatted($date);
    }


    public function getDateTimeFormatted($data)
    {
        return Carbon::parse($data)->format(config('app.locale') != 'en' ? 'd/m/Y H:i:s' : 'm/d/Y H:i:s');
    }
}