<?php

namespace App\Http\Controllers;

use App\Http\Requests\AutomationRequest;
use App\Repositories\UserRepository;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $user_repo;
    //
    public function __construct(UserRepository $userRepository)
    {
        $this->middleware(['auth','username']);
        // TODO: Implement __construct() method.
        $this->user_repo = $userRepository;
    }

    public function finance_history()
    {
        return view('front.financehistory',$this->user_repo->getUserTransHistory(auth()->user(),25));
    }



    public function automation()
    {
        $auto_invest = $this->user_repo->getAutoReinvestStatus(auth()->user());
        $auto_with = $this->user_repo->getAutoWithdrawStatus(auth()->user());

        return view('front.automation',compact('auto_invest','auto_with'));
    }
    public function automation_post(AutomationRequest $request)
    {
        $with = $request->input('auto_with_check');
        $with = isset($with)?true:false;
        $reinvest = $request->auto_reinvest_check;
        $reinvest = isset($reinvest)?true:false;

        $db_with = $this->user_repo->getAutoWithdrawStatus(auth()->user());
        $db_reinvest = $this->user_repo->getAutoReinvestStatus(auth()->user());

        if($with !== $db_with){
            $this->user_repo->setAutoWithdrawStatus(auth()->user(),$with);
        }
        if($reinvest !== $db_reinvest){
            $this->user_repo->setAutoReinvestStatus(auth()->user(),$reinvest);
        }
        if(!is_null($request->auto_withdraw_address)){
            $this->user_repo->setUserBtcAddress(auth()->user(),$request->auto_withdraw_address);
        }
        return back()->with('success','Information updated successfully');
    }

}
