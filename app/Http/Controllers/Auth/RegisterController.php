<?php

namespace App\Http\Controllers\Auth;

use App\Mail\EmailVerification;
use App\Models\Account;
use App\Notifications\ConfirmEmail;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Repositories\UserRepository;
use App\Notifications\ConfirmEmail as ConfirmEmailNotification;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|alpha_dash|max:255|unique:users|not_in:admin',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',
            'agree' => 'required'
            //'g-recaptcha-response'=>'required|captcha',
        ]);
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        //$this->guard()->login($user);

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath())->with('info',['type'=>'success',
                'message'=>'Account successfully created. You can now login']);
    }


    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
       // $user = null;
        $token = str_random(30);
        return DB::transaction(function() use ($data,$token){
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'parent' => is_null(session('referral'))?'admin':session('referral'),
            'confirmation_code' => null,
            'confirmed' => true
        ]);
        $user->account()->save(new Account());
        return $user;
        });
        //Mail::to($data['email'])->queue( new EmailVerification($token));
//        $user->notify(new ConfirmEmailNotification());
    }

    /**
     * Handle a confirmation request
     *
     * @param  \App\Repositories\UserRepository $userRepository
     * @param  string  $confirmation_code
     * @return \Illuminate\Http\Response
     */
    public function confirm(UserRepository $userRepository, $confirmation_code)
    {
        $status = $userRepository->confirm($confirmation_code);
        if($status) {
            return redirect('/login')->with('info',['type'=>'success', 'message'=>'Account Activated! You can now login']);//trans('front/verify.success'));
        }else{
            return redirect('/login')->with('info',['type'=>'Account has been activated']);
        }
    }

    /**
     * Handle a resend request
     *
     * @param  \App\Repositories\UserRepository $userRepository
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function resend(UserRepository $userRepository, Request $request)
    {
        if ($request->session()->has('user_id')) {
            $user = $userRepository->getById($request->session()->get('user_id'));

           // $user->notify(new ConfirmEmail());
            Mail::to($user->email)->queue( new EmailVerification($user->confirmation_code));
            return redirect()->route('login')->with('info',['type'=>'success',
                'message'=>'Activation email resent successfully. Pls check your email']);// trans('front/verify.resend'));
        }

        return redirect('/');
    }


    public function referral(Request $request, $name = 'admin')
    {

        //if(!$request->session()->has('referral')){
            $user = User::where('name',$name)->first();
            if(!is_null($user)) {
                session(['referral'=> $name]);
                return redirect()->route('homepage');
            }
        session(['referral' => null]);

        return redirect()->route('homepage');
    }

}
