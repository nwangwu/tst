<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Account;
use app\Services\Status;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;


    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout','create_username','create_username_post');
    }


    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function login(Request $request)
    {

        if ($lockedOut = $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return back()
                ->with('error', 'Login failed: You have been locked out because of too many attempts. Try again in the next minute')//trans('front/login.maxattempt'))
                ->withInput($request->only('log'));
        }

        $logValue = $request->input('log');

        $logAccess = filter_var($logValue, FILTER_VALIDATE_EMAIL) ? 'email' : 'name';

        $credentials = [
            $logAccess  => $logValue,
            'password'  => $request->input('password'),
        ];

        if (!auth()->validate($credentials)) {
            if (! $lockedOut) {
                $this->incrementLoginAttempts($request);
            }

            return back()->with('info',['type'=>'danger','message'=>'Login failed: Incorrect username or password'])// trans('front/login.credentials'))
                ->withInput($request->only($logAccess));
        }

        $user = auth()->getLastAttempted();

        if ($user->confirmed) {
            if (! $lockedOut) {
                $this->incrementLoginAttempts($request);
            }

            auth()->login($user, $request->has('remember'));

            if ($request->session()->has('user_id')) {
                $request->session()->forget('user_id');
            }

            if(Status::isAdmin()){
                return redirect()->route('admin_index');
            }
            return redirect()->route('home');
        }

        $request->session()->put('user_id', $user->id);

        return back()->with('info',
            ['type'=>'danger','message'=>trans('front/verify.again')]);
    }

    /**
     * @return mixed
     */
    public  function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function handleProviderCallback()
    {
        $data = Socialite::driver('facebook')->user();
        if(!is_null($data->email)) {
            $user = User::where('email', $data->email)->first();
            if (!is_null($user)) {
                if ($user->facebook_id === $data->user['id']) {
                    Auth::login($user);

                    return redirect()->route('home');
                    /*  $user->name = $data['name']; $user->facebook_id = $data['id']; $user->save();*/
                } else {
                    return redirect()->route('login')->with('info', ['type' => 'danger',
                        'message' => 'Login failed: a user with the same facebook email(' . $data->email . ') has been registered']);
                }
            } else {
                $user = User::where('facebook_id', $data->user['id'])->first();
                if (is_null($user)) {
                    $users = DB::transaction(function () use ($user, $data) {
                        $user = User::create([
                            'email' => $data->email,
                            'facebook_id' => $data->user['id'],
                            'parent' => is_null(session('referral')) ? 'admin' : session('referral'),
                            'confirmed' => true,
                            'confirmation_code' => null,
                        ]);
                        $user->account()->save(new Account());
                        //return $user;
                    });
                    Auth::login($users);
                }
                $this->create_username();
            }
        }else{
            return redirect()->route('login')->with('info',['type'=>'warning',
                'message'=>"You can't register with facebook. No email found in your facebook profile"]);
        }

    }

    public function create_username()
    {
        if(is_null(auth()->user()->name)){
            session(['info'=> ['type'=>'warning',
                'message'=>'You have to create a username to continue using the system']]);
            return view('auth.createusername');
        }else {
            return redirect()->route('home');
        }
    }

    public function create_username_post(Request $request)
    {
        $this->validate($request,['name'=>'required|alpha_dash|max:255|unique:users|not_in:admin']);

        DB::transaction(function()use ($request){

            $user = $request->user();
                $user->name = $request->name;
            $user->save();
            event(new Registered($user));
        });
        return redirect()->route('home')->with('info',['type'=>'success',
            'message'=>'Success: You have successfully created a username']);
    }
}
