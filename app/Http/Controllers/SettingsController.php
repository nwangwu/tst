<?php

namespace App\Http\Controllers;

use App\Repositories\UserRepository;
use Illuminate\Http\Request;

class SettingsController extends Controller
{

    protected $user_repo;

    function __construct(UserRepository $userRepository)
    {
        $this->user_repo = $userRepository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('front.setting');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'btc_address'=>'nullable|min:33|max:34|alpha_num',
            'password' =>'required|string|confirmed',
            'password_confirmation'=> 'required|string'
        ]);
        if(is_null($request->btc_addres))
        {
            $user = $request->user();
            $user->password = bcrypt($request->input('password'));
            $user->save();
        }else{
            $user = $request->user();
            $user->password = bcrypt($request->input('password'));
            $user->bit_owner_address = $request->btc_address;
            $user->save();
        }
        return back()->with('info',['type'=>'success','message'=>'User details successfully updated']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
