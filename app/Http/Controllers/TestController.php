<?php

namespace App\Http\Controllers;

use App\Models\Affiliate;
use App\Repositories\AffiliateRepository;
use App\Repositories\DepositRepository;
use App\Repositories\LearderbaordRepository;
use App\Repositories\TransactionRepository;
use App\Repositories\UserRepository;
use App\Repositories\WithdrawRepository;
use App\Services\BitcoinService;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

class TestController extends Controller
{
    protected $trans_repo;
    protected $with_repo;
    protected $deposit_repo;
    protected $leader_repo;
    protected $user;
    protected $affiliate;

    function __construct(TransactionRepository $repository,LearderbaordRepository $learderbaordRepository,AffiliateRepository $affiliate,
                         WithdrawRepository $with_repo, DepositRepository $depositRepository,UserRepository $userRepository)
    {
        $this->middleware('auth');
        $this->deposit_repo = $depositRepository;
        $this->trans_repo = $repository;
        $this->with_repo  = $with_repo;
        $this->leader_repo = $learderbaordRepository;
        $this->user = $userRepository;
        $this->affiliate = $affiliate;
        // TODO: Implement __construct() method.
    }

    public function get_confirmation($tx_hash)
    {
        //$raw_tx = json_decode(file_get_contents('https://blockchain.info/rawtx/'.$tx_hash),true);
        //https://blockchain.info/multiaddr?cors=true&active=1GyAVLrArGgLCiHMDj6DBhGU3BH6HEqCSs
        $tx_block_height = $tx_hash;//$raw_tx['block_height'];
        $latest_block = json_decode(file_get_contents('https://blockchain.info/latestblock'),true);
        $latest_block_height = $latest_block['height'];
        $confirmation = $latest_block_height - $tx_block_height + 1;
        return $confirmation;
    }

    public function test(){

        $client = new Client(['base_uri' => 'https://blockchain.info/']);
        $promise = $client->getAsync('/multiaddr',['query'=>['cors'=>'true','active'=>'1GyAVLrArGgLCiHMDj6DBhGU3BH6HEqCSs']]);
        //var_dump($promise->getBody()->getContents());
        return $promise->then(function(ResponseInterface $res){
            $result = json_decode($res->getBody()->getContents(),true);
            return $result;
        })->wait();
    }

    public function index(){

  //      echo $this->with_repo->monthWithdrawalTimeRemaining(auth()->user());

        //phpinfo();
  //      $dd = new BitcoinService();
        //echo $dfd;
//        var_dump($dd->get_confirmation("1CHqyMiPEG3Gw4z5ir7ivRXJVeyGG55N1e"));

        //$from_address = "sjdofejwlfjsfks";
        //echo $from = "'from' => '".$from_address."'";
/*
        $bitcoin_address = "1GyAVLrArGgLCiHMDj6DBhGU3BH6HEqCSs";
        $file = json_decode(file_get_contents('https://blockchain.info/address/'.$bitcoin_address.'?format=json&limit=2'));
        print_r($file->txs[0]->block_height);
        echo "<br/>";
        print $this->get_confirmation($file->txs[0]->block_height);
  */      //echo $this->get_confirmation("5535a38c26f2ca97efd168e2145a91c7fc536fc721e90f92073de61672c81c9e");


        $guid = "6634ec8e-8813-4fae-bfe4-6154ac61c6e6";
        $client = new Client(['base_uri'=>'http://127.0.0.1:3000/merchant/'.$guid.'/']);
        $password = "@mocking@jay@";
        $label = "gynesis";
        $address ="152msHajDrvwqYjDZx9M2U44JPyzW5tQUC";
        $promise =
            $client->getAsync('balance',['query'=>['password'=>$password]]);//."&label=".$label);
        $promise->then(function(ResponseInterface $res){
            var_dump($res->getBody()->getContents());
        })->wait();

/*
        $gg = new BitcoinService();
        var_dump($gg->get_wallet_balance());
*/
        //$arry = json_decode($json);

/*
          echo $arry->balance."<br/>";
          echo $arry->total_received."<br/>";
          echo $arry->address."<br/>";
*/
        //$array = $response->withBody();
       // print_r($array);
    }

    public function deposit(Request $request,$amount)
    {

        //$request->get()
        $user = $this->deposit_repo->deposit($request->user(),floatval($amount),['deposit_address'=>'3872863429302984903ioei0r9390','txs_hash'=>'3234232432432']);
        $this->affiliate->update_affiliate($request->user());
        echo  $user?'Success':'livia';

    }

    public function withdraw(Request $request,$amount)
    {
        $this->with_repo->withdraw($request->user(),
            ['amount' => $amount,'bit_address' => 'bit8892ejsj8ru3woirwe']);
    }

    public function cancel(Request $request)
    {
        $depo = $this->deposit_repo->getLatestDeposit($request->user());
        echo $depo->count();
        foreach($depo as $d){
            echo $d->amount."<br>";
        }
       // $this->with_repo->cancel_withdrawal($request->user());
    }
    public function reinvest()
    {
        $this->trans_repo->auto_reinvest();
    }

    public function ajax(Request $request){

        return response()->json(['deposit_address'=>$request->input('deposit_address'),'gateway_deposit'=>$request->input('gateway_deposit')]);
    }
    public function auto_withdraw(Request $request)
    {


        //$this->trans_repo->update_earning();
        //$this->trans_repo->get_open_deposit_and_close();

      //  $this->affiliate->update_affiliate(auth()->user());
        /*
        $user = $this->affiliate->get_affiliate(auth()->user(),2);
        $user->each(function($data){
            echo $data->name."<br/>";
        });*/
        /*
        $user = $this->leader_repo->getTopDeposits(4);//$this->leader_repo->getTopWithdrawals(2);
        foreach($user as $u)
            echo $u->amount." : " .$u->status." : ". $u->account->user->email."<br/>";

        //$this->trans_repo->auto_withdraw();
        */
        /*$user = $this->leader_repo->getTopMembers(5);
        foreach($user as $u)
        {
            echo $u->total_deposit . " : ". $u->balance . " : ". $u->user->email. "<br/>";
        }

        print_r($this->user->getUserCategory());
        */
    }


}
