<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Repositories\AffiliateRepository;
use Illuminate\Http\Request;

class AffiliateController extends Controller
{
    //

    protected $affiliate_repo;


    function __construct(AffiliateRepository $affiliateRepository)
    {
        $this->middleware('auth');
        $this->affiliate_repo = $affiliateRepository;
    }


    public function index()
    {
        //$this->affiliate_repo->update_affiliate($request->user());
        $affiliate = $this->affiliate_repo->get_children_active(auth()->user(),env('TOP_PAGINATE',25));
        return view('front.affiliateprogram',compact('affiliate'));
    }

    public function affiliate_post(Request $request){
        $this->validate($request,[
            'commission'=>'required|numeric',
            'child_id' => 'required|integer'
        ]);
        $result = $this->affiliate_repo->reinvest_deposit($request->user(),$request->child_id,$request->commission);
        if($result === true)
        {
            return back()
                ->with('info',['type'=>'success',
                    'message'=>'Success: You have successfully invested '.sprintf('%.8f',$request->commission).' BTC']);
        }else {
            return back()
                ->with('info', ['type' => 'danger',
                    'message' => 'Failed: Action failed. Try again later'.$result]);
        }

    }

}
