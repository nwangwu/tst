<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BitcoinController extends Controller
{
    function __construct()
    {
        $this->middleware('ajax')->only('ajax_check');
    }


    public function ajax_check(Request $request)
    {
        return response()->json(['deposit_address'=>$request->input('deposit_address'),'gateway_deposit'=>$request->input('gateway_deposit'),'txs_hash' => $request->input('txs_hash')]);
    }
}
