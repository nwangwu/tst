<?php

namespace App\Http\Controllers;

use App\Http\Requests\WithdrawRequest;
use App\Models\Affiliate;
use App\Repositories\AffiliateRepository;
use App\Repositories\DepositRepository;
use App\Repositories\TransactionRepository;
use App\Repositories\WithdrawRepository;
use App\Services\BitcoinService;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TransactionController extends Controller
{


    protected $transactionRepository;
    protected $deposit_repo;
    protected $withdraw_repo;
    protected $bitcoin_service;
    protected $affiliate;
    //
    function __construct(TransactionRepository $repository,DepositRepository $depositRepository,
                         WithdrawRepository $withdrawRepository,BitcoinService $bitcoinService,AffiliateRepository $affiliate)
    {
        $this->middleware(['auth','username']);
        $this->transactionRepository = $repository;
        $this->deposit_repo = $depositRepository;
        $this->withdraw_repo = $withdrawRepository;
        $this->bitcoin_service = $bitcoinService;
        $this->affiliate=$affiliate;
    }

    public function deposit()
    {
        $deposit_address = auth()->user()->bit_deposit_address;
        return view('front.deposit',compact('deposit_address'));
    }

    public function deposit_post(Request $request)
    {
        $result = $this->bitcoin_service->get_confirmation($request->user()->bit_deposit_address);
        if($result['confirmation'] > env('NUM_CONFIRMATION',3)) {
            $total_received = $result['total_received'] / env('SATOSHI', 100000000);
            $frn = $this->deposit_repo->deposit($request->user(), $total_received,
                ['deposit_address' => $request->user()->bit_deposit_address, 'txs_hash' => $result['txs_hash']]);
            if($frn) {
                $this->affiliate->update_affiliate($request->user());
                $request->session()->flash('info', ['type' => 'success', 'message' => 'Success: Transaction confirmed and updated']);
            }else {
                $request->session()->flash('info', ['type' => 'info', 'message' => 'Checking transaction']);
            }
        }else{
            $request->session()->flash('info',['type'=>'warning','message' => 'Failed: Transaction not yet confirmed']);
        }

        return redirect()->action('TransactionController@deposit');
    }
    /**
     * used to withdraw earned balance
     */
    public function withdraw()
    {

        $balance = $this->deposit_repo->getBalance(auth()->user());

        return view('front.withdraw',compact('balance'));
    }

    public function withdraw_post(WithdrawRequest $request)
    {
        $user = auth()->user();
        $addr = $request->input('withdraw_address');
        if(is_null($addr) && !is_null($user->bit_owner_address))
        {
            $addr = $user->bit_owner_address;
        }
        if(is_null($addr) && is_null(auth()->user()->bit_owner_address)){
            return back()->with('info',['type'=>'warning',
                'message'=>'No recipient BTC address in our database and none supplied.
                 Enter the BTC address where your Bitcoin should be sent']);
        }
        if(!$this->withdraw_repo->isInitialWithdrawalAllowed($user)){
            return back()->with('info',['type'=>'warning',
                'message'=>'You cannot withdraw now! Your initial deposit has not exceeded 48 hours']);
        }
        if(!$this->withdraw_repo->isTodayWithdrawalAllowed($user)){
            return back()->with('info',['type'=>'warning',
                'message'=>'You have exceeded the number of daily withdrawals. Try again in 24 hours']);
        }
        if($this->withdraw_repo->anyPendingWithdrawal($user)){
            return back()->with('info',['type' =>'warning',
                'message'=>'You have a pending withdrawal request. You cant withdraw until it is completed']);
        }
        $result = $this->withdraw_repo->withdraw($user,['bit_address'=>$addr,'amount'=>$request->withdraw_amount]);

        if($result == false)
        {
            return back()->with('info',['type'=>'danger',
                'message'=>'Failed: make sure the amount entered is greater than your current balance and 0.001 BTC']);
        }
        $gateway = $this->withdraw_repo->withdraw_from_gateway($user->id."|".$addr."|".$request->withdraw_amount);
        if($gateway == false){
            return back()->with('info', ['type' => 'danger', 'message' =>
                'Transaction failed to complete but withdrawal request received successfully.']);
        }
        return redirect()->route('home')->with('info', ['type' => 'success',
            'message' => /*$result['message'] . '| Txs_hash:' . $result['tx_hash'] .
                ' | Notice: ' . $result['notice'].*/ '  Withdraw was successful. Your BTC address will be credited soon']);
        //return back();
        /*return redirect()->route('home')->with('info',['type'=>'success',
            'message'=>'Withdraw request received successfully. The system will update your BTC address soon']);
        */
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function reinvest()
    {
        $balance = $this->deposit_repo->getBalance(auth()->user());
        return view('front.reinvest',compact('balance'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function reinvest_post(Request $request)
    {
        $this->validate($request,['reinvest_amount' => 'required|numeric']);

        if($this->deposit_repo->getBalance($request->user()) < $request->reinvest_amount)
        {
            return back()->with('info',['type'=> 'danger','message'=>'Reinvest amount cannot be greater than your current balance']);
        }
        if($request->reinvest_amount < env('MIN_REINVEST',0.0005))
        {
            return back()->with('info',['type'=> 'danger','message'=>'Reinvest amount is less than the minimum reinvest amount']);
        }

        $this->deposit_repo->reinvent($request->user(),$request->reinvest_amount);

        return redirect()->route('home')
            ->with('info',['type' => 'success','message'=>'Your have successfully reinvested '.sprintf('%.8f',$request->reinvest_amount).' BTC']);
    }

    /**
     * Used to withdraw deposited BTC that is still open before first valorization
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function withdraw_deposit()
    {
        return view('front.withdrawdeposit');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function withdraw_deposit_post(WithdrawRequest $request)
    {
        $addr = $request->input('withdraw_address');
        $amount = $request->input('withdraw_amount');
        if(is_null($addr) && !is_null(auth()->user()->bit_owner_address))
        {
            $addr = auth()->user()->bit_owner_address;
        }
        if(is_null($addr && is_null(auth()->user()->bit_owner_address))){
            return back()->with('info',['type'=>'warning',
                'message'=>'No recipient BTC address in database and none supplied.
                 Enter the BTC address where Bitcoin should be sent']);
        }

        if(!$this->withdraw_repo->isMonthWithdrawalAllowed($request->user())){
            return back()->with('info',['type'=>'warning','message'=>'Failed: You have '.
                $this->withdraw_repo->monthWithdrawalTimeRemaining($request->user()). ' to withdraw active deposit']);
        }
        if($this->withdraw_repo->withdraw_active_deposit($request->user(),['amount' => $amount,'bit_address' => $addr]) == false)
        {
            return back()->with('info',['type'=>'danger','message'=>'Failed: you might have a pending withdrawal request']);
        }
        return back()
            ->with('info',['type'=>'success','message'=> 'Withdraw request sent successfully. You will receive your BTC soon']);

    }
    /*
    public function cancel_deposit(Request $request)
    {
        $this->validate($request,[
            'item_id' => 'required|numeric',
            'with_address'=>'nullable|alpha_num|min:33|max:34'
        ],[
            'min' =>'The BTC address must be at least :min characters',
            'max' =>'The BTC address must not exceed :max characters',
            'alpha_num' =>'The BTC address must be alphanumeric characters'
        ]);
        $addr = $request->input('with_address');
        if(is_null($addr) && !is_null(auth()->user()->bit_owner_address))
        {
            $addr = auth()->user()->bit_owner_address;
        }
        if(is_null($addr && is_null(auth()->user()->bit_owner_address))){
            return back()->with('info',['type'=>'warning',
                'message'=>'No receive BTC address in database and none supplied.
                 Enter the BTC address where Bitcoin should be sent']);
        }
        if($this->withdraw_repo->withdraw_deposit($this->deposit_repo,$request->user(),
            ['deposit_id'=>$request->input('item_id'),'bit_address'=>$addr])==false){
            return back()->with('info',['type'=>'danger','message'=>'Failed: you might have pending withdraw']);
        }

        return back()
            ->with('info',['type'=>'success','message'=> 'Deposit successfully cancelled. You will receive your BTC soon']);
    }
    */


}
