<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactRequest;
use App\Mail\ContactEmail;
use App\Repositories\LearderbaordRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{

    protected $userRepository;

    protected $leaderboardrepo;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserRepository $repository, LearderbaordRepository $learderbaordRepository)
    {
        /*
        $this->middleware('auth')->only('index','leaderboard');

        $this->middleware('username')->only('index','leaderboard');
        */
        $this->userRepository = $repository;
        $this->leaderboardrepo = $learderbaordRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->user()->getRoleStatus() == 'super_admin'
            || auth()->user()->getRoleStatus() == 'admin'){
            return redirect()->route('admin_index');
        }
        //Auth::logout();
        return view('front.home',array_merge($this->userRepository->getUserAndSiteData(),
            $this->userRepository->getWithdrawAndDepositStat(env('TOP_PAGINATE',25)),
            $this->userRepository->getUserStat(auth()->user())));
    }

    public function leaderboard()
    {
        $top_withdraw = $this->leaderboardrepo->getTopWithdrawals(env('TOP_PAGINATE',25));
        $top_deposit = $this->leaderboardrepo->getTopDeposits(env('TOP_PAGINATE',25));
        $top_member = $this->leaderboardrepo->getTopMembers(env('TOP_PAGINATE',25));
        return view('front.leaderboard',compact('top_withdraw','top_deposit','top_member'));
    }

    public function homepage()
    {
        return view('pages.home',array_merge($this->userRepository->getUserAndSiteData(),
            $this->userRepository->getWithdrawAndDepositStat(env('TOP_PAGINATE',25))));
    }


    public function faq()
    {
        return view('front.questions');
    }


    public function contact_us()
    {
        return view('front.contact');
    }

    public function contact_process(ContactRequest $request)
    {

        Mail::to(env('ADMIN_EMAIL'))
            ->queue( new ContactEmail($request->email,$request->username,$request->message));

        return back()->with('info',['type'=>'success',
            'message'=>'Email sent successfully. We will get back to you soon through the provided email']);
    }

    public function terms_of_service()
    {
        return view('front.tos');
    }
    public function privacy_policy()
    {
        return view('pages.privacy_policy');
    }

}
