<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\AdminRepository;
use App\Repositories\SuperAdminRepository;
use Illuminate\Http\Request;

class SuperAdminController extends Controller
{
    protected $admin_repo;

    function __construct(SuperAdminRepository $adminRepository)
    {
        $this->admin_repo = $adminRepository;
    }


    public  function add_admin()
    {
        return view('back.addadmin');
    }

    public function add_admin_post(Request $request)
    {
        $this->validate($request,
            [
                'name' => '|bail|required|alpha_dash|unique:users',
                'email' =>'bail|required|email|unique:users',
                'password'=>'required|confirmed|string',
                'password_confirmation' =>'required',
            ]);

        $user = $this->admin_repo->store_admin([
                    'role'=> 2,'name'=>$request->name,
                    'email' =>$request->email,
                    'password' => $request->password]);
        if($user){
            return back()->with('info',['type'=>'success','message'=>'User registered successfully']);
        }
        return back()->with('info',['type'=>'danger','message' => 'User registration failed']);
    }

    public function view_admin()
    {
        $admins = $this->admin_repo->get_admins(env('TOP_PAGINATE',25));
        return view('back.viewadmin',compact('admins'));
    }

    public function remove_admin(Request $request)
    {
        $this->validate($request,['adminform' => 'required|integer']);

        if($request->adminform == $request->user()->id)
        {
            return back()->with('info',['type'=>'danger',
                'message'=>'Failed: User '.$request->user()->name.
                    ' currently owns the session and cannot be deleted from this account']);
        }
        $result = $this->admin_repo->delete_admin($request->adminform);
        if($result)
        {
            return back()->with('info',['type'=>'success','message'=>'Success: admin successfully deleted']);
        }
        return back()->with('info',['type'=>'danger','message'=>'Failed: deletion failed']);
    }

    public function deleted_admin()
    {
        $admins = $this->admin_repo->get_deleted_admins(env('TOP_PAGINATE',25));
        return view('back.deletedadmin',compact('admins'));
    }
    public function restore_admin(Request $request)
    {
        $this->validate($request,['adminform' =>'required|integer']);

        $result = $this->admin_repo->restore_deleted_admin($request->adminform);
        if($result)
        {
            return back()->with('info',['type'=>'success','message'=>'Success: admin successfully restored']);
        }
        return back()->with('info',['type'=>'danger','message'=>'Failed: retoration failed']);

    }

    public function remove_user(Request $request){

        $this->validate($request,['userform' => 'required|integer']);

        $result = $this->admin_repo->delete_user($request->userform);
        if($result)
        {
            return back()->with('info',['type'=>'success','message'=>'Success: user successfully deleted']);
        }
        return back()->with('info',['type'=>'danger','message'=>'Failed: deletion failed']);
    }
    public function deleted_user()
    {
        $users = $this->admin_repo->get_deleted_user(env('TOP_PAGINATE',25));
        return view('back.deleteduser',compact('users'));
    }


    public function restore_user(Request $request)
    {
        $this->validate($request,['userform' =>'required|integer']);

        $result = $this->admin_repo->restore_deleted_user($request->userform);
        if($result)
        {
            return back()->with('info',['type'=>'success','message'=>'Success: user successfully restored']);
        }
        return back()->with('info',['type'=>'danger','message'=>'Failed: retoration failed']);
    }

    public function wallet_setting()
    {


        /*
        $env = file_get_contents(base_path().'/.env');
        $envv = [];
        $env = preg_split('/\s+/',$env);
        foreach($env as $key => $env_value) {
            $entry = explode('=', $env_value, 2);
            $envv[$entry[0]] = $entry[1];
        }*/
        $envv = $this->admin_repo->getUserSettings();
        return view('back.walletsetting',compact('envv'));

    }

    public function wallet_setting_post(Request $request)
    {
        $this->validate($request,[
            'invest'=>'required|numeric',
            'payouts' => 'required|numeric',
            'registerusers'=> 'required|integer',
            'activeusers' => 'required|integer'

        ]);
        if($this->admin_repo->updateUserSettings($request->invest,
            $request->payouts,$request->registerusers, $request->activeusers)){
            return back()->with('info',['type'=>'success','message'=>'You have successfully changed the settings']);
        }
        return back()->with('info',['type'=>'danger','message'=>'Changes failed to save']);


        /*
        $this->validate($request,[
            'key' => 'required|alpha_dash',
            'keyvalue'=>'required'
        ]);
        $key = strtoupper($request->key);
        $data[$key] = $request->keyvalue;

        if($this->admin_repo->changEnv($data))
        {
            return back()->with('info',['type'=>'success','message'=>'You have successfully changed the env content']);
        }
        return back()->with('info',['type'=>'warning','message'=>'Change of environment variable failed']);
        */
    }

}
