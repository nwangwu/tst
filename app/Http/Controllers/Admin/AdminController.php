<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Mail\AdvertEmail;
use App\Repositories\AdminRepository;
use App\Repositories\LearderbaordRepository;
use App\Repositories\TransactionRepository;
use App\Repositories\UserRepository;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class AdminController extends Controller
{

    protected $admin_repo;
    protected $learder_repo;
    protected $user_repo;
    protected  $trans_repo;

    function __construct(AdminRepository $adminRepository, LearderbaordRepository $learderbaordRepository, UserRepository $repository, TransactionRepository $tran_repo)
    {
        $this->admin_repo = $adminRepository;
        $this->leader_repo = $learderbaordRepository;
        $this->user_repo = $repository;
        $this->trans_repo = $tran_repo;
    }

    public function index()
    {

        return view('back.index',$this->user_repo->getUserAndSiteData());
    }

    public function withdraw_request()
    {
        $users = $this->admin_repo->get_withdrawal_request(env('TOP_PAGINATE',25));
        return view('back.viewwithdraw',compact('users'));
    }

    public function make_payment()
    {
        $users = $this->admin_repo->get_withdrawal_request(env('TOP_PAGINATE',25));
        return view('back.makepayment',compact('users'));
    }

    private function explode_post($request_arr)
    {
        $new_array = [];
        $user_id = [];

        foreach($request_arr as $rr){
            $new_arra = explode('|',$rr);
            $new_array[$new_arra[1]] = (int)($new_arra[2] * env('SATOSHI',100000000));
            $user_id[] = (int)$new_arra[0];
        }
        return [$new_array,$user_id];
    }

    public function make_payment_post(Request $request)
    {
        $pro_data = $this->explode_post($request->input('btcaddress'));
        $new_array = $pro_data[0];
        $user_id  = $pro_data[1];

        $result = $this->admin_repo->send_payments($new_array,$user_id);
        if($result != false){
            $request->session()->flash('info',['type'=>'success','message'=>$result['message'].'| Txs_hash: '.$result['tx_hash']]);
            return back();
        }
        return back()->with('info',['type'=>'danger','message'=>'Transaction failed to complete']);
    }

    public function make_single_payment_post(Request $request)
    {
        $this->validate($request,['user'=>'required']);

        $arr = explode('|',$request->input('user'));
        $user_id = $arr[0];
        $user_bit_address = $arr[1];
        $amount = (int)($arr[2] * env('SATOSHI',100000000));
        $result = $this->admin_repo->send_single_payment($user_bit_address,$amount,$user_id);
        if($result != false){
            $request->session()->flash('info',['type'=>'success',
                'message'=>$result['message'].'| Txs_hash:'.$result['tx_hash']]);
            return back();
        }
        return back()->with('info',['type'=>'danger','message'=>'Transaction failed to complete']);
    }

    public function cancel_payment_post(Request $request)
    {
        $this->validate($request,['user'=>'required']);
        $arr = explode('|',$request->input('user'));
        $user_id = $arr[0];
        $result = $this->admin_repo->cancel_withdrawal($user_id);
        if($result == true){
            return back()->with('info',['type'=>'success','message'=>'Cancelled withdrawal request successfully']);
        }
        return back()->with('info',['type'=>'danger','message'=>'Cancellation failed']);
    }

    public function view_leaderboard()
    {
        $top_withdraw = $this->leader_repo->getTopWithdrawals(env('TOP_PAGINATE',25));
        $top_deposit = $this->leader_repo->getTopDeposits(env('TOP_PAGINATE',25));
        $top_member = $this->leader_repo->getTopMembers(env('TOP_PAGINATE',25));
        return view('back.leaderboard',compact('top_withdraw','top_deposit','top_member'));
    }

    public function view_users()
    {
        $users  = $this->admin_repo->get_users_data(env('TOP_PAGINATE',25));
        return view('back.viewuser',compact('users'));
    }


    public function wallet_info()
    {
        $balance = $this->admin_repo->get_wallet_balance();
        $address_list = $this->admin_repo->get_address_list();

        return view('back.walletinfo',compact('balance','address_list'));
    }

    public function address_balance()
    {
        return view('back.addressbalance');
    }
    public function address_balance_post(Request $request)
    {
        $this->validate($request,[
            'btcaddress'=>'required|alpha_num|min:33|max:34'
        ]);
        $address = $request->btcaddress;
        $balance = $this->admin_repo->get_address_balance($address);
        return view('back.addressbalance',compact('balance','address'));
    }

    public function change_admin_passcode()
    {
        return view("back.settings");
    }

    public function change_admin_passcode_post(Request $request)
    {
        $this->validate($request,[
            'password' =>'required|string|confirmed',
            'password_confirmation'=> 'required|string'
        ]);

        $user = $request->user();
        $user->password = bcrypt($request->input('password'));
        $user->save();
        return back()->with('info',['type'=>'success','message'=>'User password successfully updated']);
    }

    public function create_user()
    {
        return view('back.addfalseuser');
    }

    public function create_user_post(Request $request)
    {
        $this->validate($request,
        [
            'name' => '|bail|required|alpha_dash|unique:users',
            'email' =>'bail|required|email|unique:users',
            'password'=>'required|confirmed|string',
            'password_confirmation' =>'required',
        ]);

        $user = $this->admin_repo->store_user([
            'role'=> 4,'name'=>$request->name,
            'email' =>$request->email,
            'password' => $request->password]);
        if($user){
            return back()->with('info',['type'=>'success','message'=>'User registered successfully']);
        }
        return back()->with('info',['type'=>'danger','message' => 'User registration failed']);
    }

    public function get_users(){
        $users = $this->admin_repo->get_false_users(env('TOP_PAGINATE'));
        return view('back.viewfalseusers',compact('users'));
    }

    public function add_trans(Request $request)
    {
        $this->validate($request,['user_id' => 'required|integer']);
        $user_id = $request->input('user_id');
        return view('back.addfalsetrans',compact('user_id'));
    }

    public function add_trans_post(Request $request)
    {
        $this->validate($request,[
            'user_id' => 'required|integer',
            'trans_type'=>'required',
            'amount'=>'required'
        ]);
        $user_id = $request->user_id;
        $status = null;
        if(strtolower(trim($request->trans_type)) == 'withdraw'){
            $status = 'completed';
        }else{
            $status = 'closed';
        }
        $result = $this->trans_repo->add_false_transaction($request->user_id,
            ['trans_type'=>$request->trans_type,'amount'=>$request->amount,'status'=>$status]);
        if($result){
            $alert = ['type'=>'success','message'=>'User transaction added successfully'];
        }else{
            $alert = ['type'=>'danger','message'=>'User transaction failed'];
        }
        $request->session()->flash('info',$alert);
        return view('back.addfalsetrans',compact('user_id'));
    }


    public function view_trans(Request $request)
    {
        $this->validate($request,['user_id' => 'required|integer']);
        $trans = $this->trans_repo->view_false_transactions($request->user_id);

        return view('back.viewfalsetrans',compact('trans'));
    }

    public function update_trans(Request $request)
    {
        $this->validate($request,['transform'=>'required']);
        $result = $this->trans_repo->get_transaction($request->input('transform'));
        return view('back.updatefalsetrans',compact('result'));
    }

    public function update_trans_post(Request $request)
    {
        $this->validate($request,[
            'trans_id' => 'required|integer',
            'trans_type'=>'required',
            'amount'=>'required',
            'updated_at'=>'required'
        ]);
        $exp  = explode('-',str_replace('/','-',trim($request->updated_at)),3);
        $new_trail = explode(' ',trim(array_pop($exp)));
        $year = $new_trail[0];
        $date = $new_trail[1];
        $new_exp = implode('-',$exp);
        $updated_at = $year.'-'.$new_exp.' '.$date;


        $result = $this->trans_repo->update_false_transaction($request->trans_id,[
            'trans_type'=>$request->trans_type,
            'amount'=>$request->amount,
            'updated_at'=>$updated_at
        ]);
        if($result){
            $alert = ['type'=>'success','message'=>'Transaction updated successfully '.$updated_at];
        }else{
            $alert = ['type'=>'danger','message'=>'Transaction update failed'];
        }

        $result = $this->trans_repo->get_transaction($request->input('trans_id'));
        $request->session()->flash('info',$alert);
        return view('back.updatefalsetrans',compact('result'));

    }

    public function update_user(Request $request)
    {
        $user_id = $request->user_id;
        return view('back.updatefalseuser',compact('user_id'));
    }

    public function update_user_post(Request $request)
    {
        $this->validate($request,
            ['name' => '|bail|required|alpha_dash|unique:users',
                'user_id' =>'required|integer'
            ]);

        $result = $this->user_repo->update_user($request->user_id,$request->name);
        if($result){
            $alert = ['type'=>'success','message'=>'username updated successfully'];
        }else{
            $alert = ['type'=>'danger','message'=>'username update failed'];
        }
        $request->session()->flash('info',$alert);
        $user_id = $request->user_id;
        return view('back.updatefalseuser',compact('user_id'));
    }

    public function send_many_mail()
    {
        return view('back.sendmany');
    }

    public function send_many_post(Request $request)
    {
        $this->validate($request,['message'=>'required']);
        foreach (User::all() as $user){
            Mail::to($user->email)
                ->queue( new AdvertEmail($user->name,$request->message));
        }
        return back()->with('info',['type'=>'success','message'=>'Campaign Email sent successfully']);
    }

    public function send_one_mail()
    {
        return view('back.sendone');
    }

    public  function send_one_mail_post(Request $request)
    {
        $this->validate($request,['email' => 'required|email','message'=>'required']);

        Mail::to($request->email)
            ->queue( new AdvertEmail(User::where('email',$request->email)->first()->name,$request->message));

        return back()->with('info',['type'=>'success',
            'message'=>'Email sent successfully']);

    }
}
