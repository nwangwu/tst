<?php

namespace App\Http\Middleware;

use Closure;

class IsSuperAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(session('login_status') === 'super_admin'){
            return $next($request);
        }
       return redirect()->back();
    }
}
