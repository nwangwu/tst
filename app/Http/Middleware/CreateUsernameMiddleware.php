<?php

namespace App\Http\Middleware;

use Closure;

class CreateUsernameMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!is_null($request->user()->name)){
            return $next($request);
        }

        return redirect()->route('create_username');
    }
}
